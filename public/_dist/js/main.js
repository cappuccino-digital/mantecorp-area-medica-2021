
// efeito parallax por background
function parallaxFX(obj, speed) {
    $(obj).css('background-position-y', $(window).scrollTop() / ($(obj).attr('data-parallax-scroll') * speed));
}
// efeito parallax para objetos flutuantes
function movedToUpbyScroll(obj, speed) {
    $(obj).css('transform', 'translateY(-' + ($(window).scrollTop() / speed) + 'px)');
}
function movedToDownbyScroll(obj, speed) {
    $(obj).css('transform', 'translateY(' + ($(window).scrollTop() / speed) + 'px)');
}

$(document).ready(function(){
    var header = $('header');

    // Compactando o Meny superior no scroll
    $(window).scroll(function () {
        compactHeader();
    });

    $('.nav .dropdown').on('click', function(){
        $(this).find('.submenu').toggleClass('open');
        e.preventDefault();
    });

    $('.items-menu-my-profile p').on('click', function(){
        $(this).parent().find('ul').toggleClass('open');
        e.preventDefault();
    });

    function openMenu(){
        $( '#menu-mobile' ).unbind('click').on('click' , function(e){
            e.preventDefault();

            if($('#menu-mobile').hasClass('navOpen')){
                $('#menu-mobile').removeClass('navOpen');
                $('.content-header').removeClass('open-menu');
                $('body').removeClass('open');
                $('.logo').removeClass('logo-open');
                $('.alignBtnForm').removeClass('open');
                // $('.items-menu-my-profile ul').removeClass('open');
                
            } else{
                $('#menu-mobile').addClass('navOpen');
                $('.content-header').addClass('open-menu');
                $('body').addClass('open');
                $('.logo').addClass('logo-open');
                $('.alignBtnForm').addClass('open');
                // $('.items-menu-my-profile ul').addClass('open');
                
            }
        });
    }

    openMenu();


    function compactHeader() {
        if ($(window).scrollTop() > header.outerHeight()) {
            header.addClass('compact');
            $('body').addClass('compact');
            $('.main-section').addClass('main-section-compact');
            
        } else {
            header.removeClass('compact');
            $('body').removeClass('compact');
            $('.main-section').removeClass('main-section-compact');
        }
    }


    function openModal(){
        $( '[data-modal]' ).unbind('click').on('click' , function(e){
            e.preventDefault();
            var openModal = $(this).attr("data-modal");
            $('body').addClass('open-modal');
            $('.modal-default').removeClass('open-modal');
            $('#'+openModal).addClass('open-modal');
        });
    }

    function closeModal(){
        $( '.close-modal' ).unbind('click').on('click' , function(e){
            e.preventDefault();

            if($('body, .modal-default').hasClass('open-modal')){
                $('body, .modal-default').removeClass('open-modal');
            }
        });
    }

    var $animation_elements = $('.animation-element, .animation-element-left, .animation-element-right');
	var $window = $(window);

	function check_if_in_view(isFirstTime) {
	  var window_height = $window.height();
	  var window_top_position = $window.scrollTop();
	  var window_bottom_position = (window_top_position + window_height);
	 
	  $.each($animation_elements, function() {
	    var $element = $(this);
	    var element_height = $element.outerHeight();
	    var element_top_position = $element.offset().top;
	    var element_bottom_position = (element_top_position + element_height);
	 
	    //check to see if this current container is within viewport
	    if ((element_bottom_position >= window_top_position) &&
	        (element_top_position <= window_bottom_position)) {
	      $element.addClass('in-view');
	    } else {
				if(isFirstTime) $element.removeClass('in-view');
			}
	  });
    }
	check_if_in_view(true);
	
	$window.on('scroll resize', function(){
		check_if_in_view(false);
	});
	$window.trigger('scroll');

    
});