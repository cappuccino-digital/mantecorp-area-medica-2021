var fire = fire || {};

const messaging = firebase.messaging();
messaging.usePublicVapidKey('BDDnKa_mRK7TbmDVqjCR6E3RGInYnIzNalGUJnCwBHqDa3VILQdjWajbxAu9pxZH0D7u3J_88LQ5viePyixS-2k');

messaging.onTokenRefresh(() => {
    messaging.getToken().then((refreshedToken) => {
        fire.sendTokenToServer(refreshedToken);
    }).catch((err) => {
    });
});

fire = {
    init: function() {
        Notification.requestPermission().then((permission) => {
            if (permission === 'granted') {
              fire.getToken();
            } else {
            }
        });
    },
    getToken: function(){

        messaging.getToken().then(function(currentToken) { 
            if (currentToken) { 
                fire.sendTokenToServer(currentToken); 
            } 
            else { 
            } 
        }).catch(function(err) { 
        });

    },
    sendTokenToServer: function(currentToken){
        if(currentToken){
            $.ajax({
                url: 'api/subscribe-notification/' + currentToken,
                type: 'POST',
                dataType: "json",
                success: function (response) {
                }
            });
        }

        messaging.onMessage(function(payload) {
            var notificationTitle = payload.data.title;
            var notificationOptions = {
                body: payload.data.message,
                icon: "https://portalneopharma.com.br/_dist/image/icon.png",
                collapse_key: false
            };
            var notification = new Notification(notificationTitle,notificationOptions);
            notification.onclick = function(event) {
                event.preventDefault();
                notification.close();
            };
        });
    }
}