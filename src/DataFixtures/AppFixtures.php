<?php

namespace App\DataFixtures;

use App\Entity\Menu;
use App\Entity\Profile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Role;
use DateTime;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $roles = new Role();
        $roles->setName('Administrador');
        $roles->setRole('ROLE_ADMIN');

        $manager->persist($roles);
        $manager->flush();

        $roles = new Role();
        $roles->setName('Usuário');
        $roles->setRole('ROLE_USER');

        $manager->persist($roles);
        $manager->flush();

        $profile = new Profile();
        $profile->setName('Administrador');
        $profile->setSlug('admin');
        $profile->setIsActive(true);
        $profile->setCreatedAt(new \DateTime);
        $profile->setUpdatedAt(new \DateTime);

        $manager->persist($profile);
        $manager->flush();

        $role = $manager->getRepository('App:Role')->findOneBy(['role' => 'ROLE_ADMIN']);
        $profile = $manager->getRepository('App:Profile')->findOneBy(['slug' => 'admin']); //perfil admin

        $user = new User();
        $user->setProfile($profile);
        $user->setName('Administrador');
        $user->setEmail('wellington.santos@cappuccinodigital.com.br');
        $user->setUsername('admin');
        // $salt = sha1(uniqid()); //criptografia
        // $user->setSalt($salt);
        $user->setPassword(
            $this->encoder->encodePassword($user, '123')
        );
        $user->setAvatar('upload/user/avatar.png');
        $user->addRole($role);
        $user->setIsActive(true);
        $user->setCreatedAt(new \DateTime);
        $user->setUpdatedAt(new \DateTime);
        $user->setLastLoginAt(new \DateTime);

        $manager->persist($user);
        $manager->flush();

        $menu = new Menu();
        $menu->setName('Usuários');
        $menu->setSlug('usuarios');
        $menu->setUrl('admin_users');
        //$menu->setIcon('nav-icon fas fa-tachometer-al');
        $menu->setIsActive(true);
        // $menu->setIsDev(false);
        $menu->setCreatedAt(new \DateTime);
        $menu->setUpdatedAt(new \DateTime);

        $manager->persist($menu);
        $manager->flush();

        $menu = new Menu();
        $menu->setName('Dashboard');
        $menu->setSlug('dashboard');
        $menu->setUrl('admin_dashboard');
        //$menu->setIcon('nav-icon fas fa-tachometer-al');
        $menu->setIsActive(true);
        //$menu->setIsDev(false);
        $menu->setCreatedAt(new \DateTime);
        $menu->setUpdatedAt(new \DateTime);

        $manager->persist($menu);
        $manager->flush();
    }
}
