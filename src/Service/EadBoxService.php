<?php

namespace App\Service;

use App\Entity\UserEad;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class EadBoxService {

    private $em;
    private $params;
    private $mailer;
    private $logger;


    public function __construct(ContainerBagInterface $params, EntityManagerInterface $em, MailerInterface $mailer, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->params = $params;
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    public function accessEadbox($user)
    { 
        $response = $this->login($user); 
       
        if (!$response['status']) { 
            $response = $this->signUp($user);
        }
        
        return $response;
    }

    public function login($user)
    {
        $data = [
            'email' => $user->getEmailEadbox(),
            'password' => $user->getPassword(),
            'document_id' => 0
        ];
    
        $header = [
            'Content-Type:application/json'
        ];

        try {

            $ch = curl_init();
            if (!$ch) {
                return ['status' => false, 'message' => "Couldn't initialize a cURL handle"];
            }
            curl_setopt( $ch, CURLOPT_URL, 'https://mantecorp.eadbox.com/api/login');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = curl_exec($ch); // execute
            curl_close($ch);
            
        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...

            return ['status' => false, 'message' => 'Ops! error ' . $e->getCode() . ' - ' . $e->getMessage(), 'error' => 'GJ002'];
        } finally {

            $response = json_decode($result, true);
            
            if (isset($response['valid'])) {
                if ($response['valid']) {
                    $url = $response['login_url'] . '?' . $response['token_authentication_key'] . '=' . $response['authentication_token'];
                    $this->insertUserEad($user, $response['user']['user_id']);
                    return ['status' => true, 'message' => 'success', 'url' => $url, 'authentication_token' => $response['authentication_token']];
                }
            }

            return ['status' => false, 'message' => 'MT-ERROR-LOGIN-01'];
        }
    }

    public function signUp($user)
    {
        $data = [
            'name' => $user->getName(),
            'email' => $user->getEmailEadbox(),
            'password' => $user->getPassword(),
            'password_confirmation' => $user->getPassword(),
            //'profession' => 'CRM 123',
            'document_id' => 0
        ];

        $header = [
            'Content-Type:application/json'
        ];

        try {

            $ch = curl_init();
            if (!$ch) {
                return ['status' => false, 'message' => "Couldn't initialize a cURL handle"];
            }
            curl_setopt( $ch, CURLOPT_URL, 'https://mantecorp.eadbox.com/api/signup');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = curl_exec($ch); // execute
            curl_close($ch);

        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...

            return ['status' => false, 'message' => 'Ops! error ' . $e->getCode() . ' - ' . $e->getMessage(), 'error' => 'GJ002'];
        } finally {

            $response = json_decode($result, true);
            
            if (isset($response['valid'])) {
                if ($response['valid']) {
                    $url = $response['login_url'] . '?' . $response['token_authentication_key'] . '=' . $response['authentication_token'];
                    
                    //gravando os usuários do EAD na base
                    if (isset($response['user']['user_id'])) {
                        $this->insertUserEad($user, $response['user']['user_id']);
                    }
                
                    return ['status' => true, 'message' => 'success', 'url' => $url];
                }
            }

            return ['status' => false, 'message' => 'MT-ERROR-REGISTER-01'];
        }
    }

    public function updateUser($user)
    {        
        $search = $this->searchUser($user);
        if (!$search['status']) {
            return ['status' => false, 'message' => 'MT-ERROR-UPDATE-01'];
        }
        
        if (!$search['user_id']) {
            return ['status' => false, 'message' => 'MT-ERROR-UPDATE-02'];
        }
    
        $user_admin = $this->em->getRepository('App:UserEad')->findOneBy(['email' => 'renan@agenciafreela.com.br', 'is_admin' => true]);
        $loginAdmin = $this->login($user_admin);
        
        if (!$loginAdmin['status']) {
            return ['status' => false, 'message' => 'MT-ERROR-UPDATE-03'];
        }

        $user_id = $search['user_id'];

        $data = [
            'user_id' => $user_id,
            'auth_token' => $loginAdmin['authentication_token'],
            'name' => $user->getName(),
            'password' => $user->getPassword(),
            'password_confirmation' => $user->getPassword(),  
        ];

        $header = [
            'Content-Type:application/json'
        ];

        try {

            $ch = curl_init();
            if (!$ch) {
                return ['status' => false, 'message' => "Couldn't initialize a cURL handle"];
            }
            curl_setopt( $ch, CURLOPT_URL, "https://mantecorp.eadbox.com/api/admin/users/$user_id");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = curl_exec($ch); // execute
            curl_close($ch);

        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...

            return ['status' => false, 'message' => 'Ops! error ' . $e->getCode() . ' - ' . $e->getMessage(), 'error' => 'GJ002'];
        } finally {

            $response = json_decode($result, true);
            
            if (isset($response['valid'])) {
                if ($response['valid']) {
                    //gravando os usuários do EAD na base
                    $this->insertUserEad($user, $user_id);

                    return ['status' => true, 'message' => 'success'];
                }
            }

            return ['status' => false, 'message' => 'MT-ERROR-UPDATE-04'];
        }
    }

    public function searchUser($user)
    {        
        //buscando o id do usuário na base
        $search_base = $this->findByUserEad($user);
        
        if ($search_base['status']) { 
            return ['status' => true, 'message' => 'success', 'user_id' => $search_base['user_id']];
        }
        /* END */

        $user_admin = $this->em->getRepository('App:UserEad')->findOneBy(['email' => 'renan@agenciafreela.com.br', 'is_admin' => true]);
        $loginAdmin = $this->login($user_admin);
        
        if (!$loginAdmin['status']) {
            return ['status' => false, 'message' => 'MT-ERROR-SEARCH-01'];
        }
        
        $data = [
            'auth_token' => $loginAdmin['authentication_token'],
            'email' => $user->getEmailEadbox(),
            'updated_after' => date('Y-m-d', strtotime("-58 days")), //Parâmetro obrigatório e intervalo máximo do parametro updated_after de até 60 dias anteriores a data atual.
        ];
        
        $header = [
            'Content-Type:application/json'
        ];

        try {

            $ch = curl_init();
            if (!$ch) {
                return ['status' => false, 'message' => "Couldn't initialize a cURL handle"];
            }
            curl_setopt( $ch, CURLOPT_URL, 'https://mantecorp.eadbox.com/api/admin/users');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = curl_exec($ch); // execute
            curl_close($ch);
           
        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...

            return ['status' => false, 'message' => 'Ops! error ' . $e->getCode() . ' - ' . $e->getMessage(), 'error' => 'GJ002'];
        } finally {

            $response = json_decode($result, true);
            
            if (isset($response[0]['user_id'])) {
                return ['status' => true, 'message' => 'success', 'user_id' => $response[0]['user_id']];
            }
                    
            return ['status' => false, 'message' => 'MT-ERROR-SEARCH-02'];
        }
    }

    public function insertUserEad($user, $ead_id)
    {
        $name = $user->getName();
        $password = $user->getPassword();
        $email = $user->getEmailEadbox();
        $created_at = date('Y-m-d');

        //Verificando se já existe na base 
        //$search_base = $this->findByUserEad($user);
        $user_ead = $this->em->getRepository('App:UserEad')->findOneBy(['email' => $email]);
        
        if (!$user_ead) { 
            $user_ead = new UserEad();
            $user_ead->setEmail($email);
            $user_ead->setEmailEadBox($email);
            $user_ead->setEadId($ead_id);
            $user_ead->setIsAdmin(false);
            $user_ead->setCreatedAt($created_at);
        }
        /* END */

        $user_ead->setName($name);
        $user_ead->setPassword($password);
        
        $this->em->persist($user_ead);
        $this->em->flush();

        return true;
    }

    public function findByUserEad($user)
    {
        $email = $user->getEmailEadbox();
        $user_ead = $this->em->getRepository('App:UserEad')->findOneBy(['email' => $email]);

        if ($user_ead) {
            return ['status' => true, 'message' => 'success', 'user_id' => $user_ead->getEadId()];
        }

        return ['status' => false, 'message' => 'Usuário não encontrado.'];
    }
}