<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AuthUserOldService {

    private $em;
    private $params;
    private $mailer;
    private $logger;


    public function __construct(ContainerBagInterface $params, EntityManagerInterface $em, MailerInterface $mailer, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->params = $params;
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    public function login($username, $password)
    {
        // $data = [
        //     'username' => $username,
        //     'password' => $password
        // ];
       // dump($username, $password); 
       //$username = 'pediatra@agenciafreela.com.br';
       //$password = '123';
       $header = [
           "Authorization: Basic ".base64_encode($username.":".$password)
        ];

        
        try {
            $ch = curl_init();
            if (!$ch) {
                return ['status' => false, 'message' => "Couldn't initialize a cURL handle"];
            }
           
            curl_setopt($ch, CURLOPT_URL, 'https://areamedica.mantecorp.webcenter.net.br/wp-json/');

            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            // curl_setopt($ch, CURLOPT_USERPWD, $credentials);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
           
            $result=curl_exec ($ch);
            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
            curl_close ($ch);
            //dump($status_code, $result); die();
            if ($status_code != 200) {
                return ['status' => false, 'message' => 'Ops! error ' . $status_code];
            }
        
            //...
        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...
        
            return ['status' => false, 'message' => 'Ops! error ' . $e->getCode() . ' - ' . $e->getMessage()];
        } finally {

            $response = json_decode($result, true);
           
            if (isset($response['code'])) {
                if ($response['code'] == 'empty_username') {
                    return ['status' => false, 'message' => 'O campo usuário está vazio.'];
                } else if ($response['code'] == 'empty_password') {
                    return ['status' => false, 'message' => 'O campo da senha está vazio.'];
                } else if ($response['code'] == 'incorrect_password') {
                    return ['status' => false, 'message' => 'O usuário ou senha que você digitou está incorreto.'];
                } else if ($response['code'] == 'awaiting_email_confirmation') {
                    return ['status' => false, 'message' => 'Sua conta está aguardando verificação de e-mail.'];
                } else if ($response['code'] == 'too_many_retries') {
                    return ['status' => false, 'message' => 'Muitas tentativas falhas de login. Tente novamente em 9 minutos.'];
                } else {
                    $message = isset($response['message']) ? $response['message'] : '';
                    return ['status' => false, 'message' => $message];
                }

            } else {
                if (isset($response['name'])) {
                    return ['status' => true, 'message' => 'success'];
                }
            }
            
            return ['status' => false, 'message' => 'Muitas tentativas falhas de login. Tente novamente em 9 minutos.'];
        }
    }

}