<?php

namespace App\Service;

use \GuzzleHttp\Client;

class CorreiosService {

    private $url = 'https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl';    
    private $http;

    public function __construct()
    {
        $this->http  = new Client();
    }

    public function consultaCEP($cep)
    {
        if (empty($cep)) {
            return array('status' => false, 'message' => 'CEP não identificado.');
        }
        
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cli="http://cliente.bean.master.sigep.bsb.correios.com.br/">
                    <soapenv:Header />
                    <soapenv:Body>
                        <cli:consultaCEP>
                            <cep>'.$cep.'</cep>
                        </cli:consultaCEP>
                    </soapenv:Body>
                </soapenv:Envelope>';
        
        $options = [
            'headers' => [
                'Content-Type' => 'text/xml; charset=UTF8',
            ],
            'body' => $xml,
        ];

        try { //tratamento de erro de requisição
            
            $ch = curl_init();
            if (!$ch) {
                $errors['init'] = "Couldn't initialize a cURL handle";
            }
            curl_setopt( $ch, CURLOPT_URL, $this->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $response = curl_exec($ch); // execute
            curl_close($ch);
        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...
            return ['status' => false, 'message' => 'Ops! error '. $e->getCode(). ' - ' . $e->getMessage(), 'error' => 'GJ002'];
        }

        $content  = iconv('ISO-8859-1' ,'UTF-8', $response);

        $doc = new \DOMDocument();
        $doc->formatOutput = true;
        $doc->preserveWhiteSpace = false;
        
        $doc->loadXML($content);

        $bairro = $doc->getElementsByTagName('bairro')->item(0)->nodeValue;
        $cep = $doc->getElementsByTagName('cep')->item(0)->nodeValue;
        $cidade = $doc->getElementsByTagName('cidade')->item(0)->nodeValue;
        $complemento2 = $doc->getElementsByTagName('complemento2')->item(0)->nodeValue;
        $endereco = $doc->getElementsByTagName('end')->item(0)->nodeValue;
        $uf = $doc->getElementsByTagName('uf')->item(0)->nodeValue;
       
        $info = array(
            'status'=> true, 
            'bairro' => $bairro, 
            'cep' => $cep, 
            'cidade' => $cidade, 
            'endereco' => $endereco, 
            'complemento2' => $complemento2,
            'uf'=> $uf
        );
        
        return $info;
    }

}

