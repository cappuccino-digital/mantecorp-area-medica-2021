<?php

namespace App\Service;

use \GuzzleHttp\Client;

class ConsultaMedicosService {

    private $url = 'https://ws.cfm.org.br:8080/WebServiceConsultaMedicos/';    
    private $http;

    public function __construct()
    {
        $this->http  = new Client();
    }
    
    public function consultaMedicos($crm, $uf)
    {
        if (!$crm) {
            return array('status' => false, 'message' => 'CRM não identificado.');
        }

        if (!$uf) {
            return array('status' => false, 'message' => 'Estado não identificado.');
        }

        $errors = [];
        
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://servico.cfm.org.br/">
                    <soapenv:Header />
                    <soapenv:Body>
                        <ser:Consultar>
                            <crm>'.$crm.'</crm>
                            <uf>'.$uf.'</uf>
                            <chave>PKVZP299</chave>
                        </ser:Consultar>
                    </soapenv:Body>
                </soapenv:Envelope>';

        try {
            $ch = curl_init();
            if (!$ch) {
                $errors['init'] = "Couldn't initialize a cURL handle";
            }
            curl_setopt( $ch, CURLOPT_URL, $this->url . 'ServicoConsultaMedicos');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = curl_exec($ch); // execute
            curl_close($ch);
           
        } catch (\Exception $e) { //Em caso de erros críticos na API, erro 500, 502, 504 ...
            return ['status' => false, 'message' => 'Ops! error '. $e->getCode(). ' - ' . $e->getMessage(), 'error' => 'GJ002'];

        } finally {
            
            $content  = iconv('ISO-8859-1' ,'UTF-8', $result);
                   
            $doc = new \DOMDocument();
            $doc->formatOutput = true;
            $doc->preserveWhiteSpace = false;
            
            $doc->loadXML($content);
            
            if (!$doc->getElementsByTagName('crm')->item(0)) {
                $errors['crm'] = 'CRM não encontrado';
            }

            // if (!$doc->getElementsByTagName('dataAtualizacao')->item(0)) {
            //     $errors['dataAtualizacao'] = 'Data atualização não encontrado';
            // }

            if (!$doc->getElementsByTagName('nome')->item(0)) {
                $errors['nome'] = 'Nome não encontrado';
            }

            if (!$doc->getElementsByTagName('situacao')->item(0)) {
                $errors['situacao'] = 'CRM não encontrado';
            } else {
                $situacao = $doc->getElementsByTagName('situacao')->item(0)->nodeValue;
                if ($situacao != 'A') {
                    $errors['situacao'] = 'Situação do CRM não permitida. Favor digitar o CRM ativo.';
                }
            }

            // if (!$doc->getElementsByTagName('tipoInscricao')->item(0)) {
            //     $errors['tipoInscricao'] = 'Tipo inscrição não encontrado';
            // }

            if (!$doc->getElementsByTagName('uf')->item(0)) {
                $errors['uf'] = 'Estado não encontrado';
            }

            if (count($errors) > 0) {
                return ['status' => false, 'message' => 'Campos obrigatórios', 'error' => $errors];
            }
            
            $crm             = $doc->getElementsByTagName('crm')->item(0)->nodeValue;
            //$dataAtualizacao = $doc->getElementsByTagName('dataAtualizacao')->item(0)->nodeValue;
            $nome            = $doc->getElementsByTagName('nome')->item(0)->nodeValue;
            $situacao        = $doc->getElementsByTagName('situacao')->item(0)->nodeValue;
            //$tipoInscricao   = $doc->getElementsByTagName('tipoInscricao')->item(0)->nodeValue;
            $uf              = $doc->getElementsByTagName('uf')->item(0)->nodeValue;
                       
            return ['status' => true, 'message' => 'success', 'data' => ['crm' => $crm, 'name' => $nome, 'uf' => $uf, 'situacao' => $situacao]];
        }
    }
    /*
    public function consultaMedicos($crm, $uf)
    {               
        if (!$crm) {
            return array('status' => false, 'message' => 'CRM não identificado.');
        }

        if (!$uf) {
            return array('status' => false, 'message' => 'Estado não identificado.');
        }
        
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://servico.cfm.org.br/">
            <soapenv:Header />
            <soapenv:Body>
                <ser:Consultar>
                    <crm>155</crm>
                    <uf>CE</uf>
                    <chave>PKVZP299</chave>
                </ser:Consultar>
            </soapenv:Body>
        </soapenv:Envelope>';
        
        $options = [
            'headers' => [
                'Content-Type' => 'application/xml',
            ],
            'body' => $xml,
        ];

        try { //tratamento de erro de requisição
            $response = $this->http->post($this->url, $options);
            dump('01',$response); die();
        } catch (\GuzzleHttp\Exception\RequestException $e) { 
            $response = $e->getResponse();
            dump('02',$response); die();
        }

             
        $doc = new \DOMDocument();
        $doc->loadXML($response->getBody()->getContents());
        dump('teste', $response->getStatusCode()); die();
        if ($response->getStatusCode() == 500) {
            $faultstring = $doc->getElementsByTagName('faultstring')->item(0)->nodeValue;
            return ['status' => false, 'message' => $faultstring];
        }

        if ($response->getStatusCode() != 200) {
            return ['status' => false, 'message' => 'Ops! error na requisição'];
        }

        $bairro = $doc->getElementsByTagName('bairro')->item(0)->nodeValue;
        $cep = $doc->getElementsByTagName('cep')->item(0)->nodeValue;
        $cidade = $doc->getElementsByTagName('cidade')->item(0)->nodeValue;
        $complemento2 = $doc->getElementsByTagName('complemento2')->item(0)->nodeValue;
        $endereco = $doc->getElementsByTagName('end')->item(0)->nodeValue;
        $uf = $doc->getElementsByTagName('uf')->item(0)->nodeValue;
       
        $info = array(
            'status'=> true, 
            'bairro' => $bairro, 
            'cep' => $cep, 
            'cidade' => $cidade, 
            'endereco' => $endereco, 
            'complemento2' => $complemento2,
            'uf'=> $uf
        );
        
        return $info;
    }
    */

}

