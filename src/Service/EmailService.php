<?php

namespace App\Service;

use App\Entity\UserEad;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;

class EmailService {

    private $em;
    private $params;
    private $mailer;
    private $logger;

    public function __construct(ContainerBagInterface $params, EntityManagerInterface $em, MailerInterface $mailer, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->params = $params;
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    public function emailTrigger($user, $subject, $view)
    {
        $email = (new Email())
                ->from(new Address('portalmantecorpfarmasa@gmail.com', 'Mantecorp Farmasa - Área médica'))
                ->to('wellington.santos@cappuccinodigital.com.br')
                //->to($user->getEmail())
                //->addBcc('ana.morales@cappuccinodigital.com.br')
                //->replyTo($contact->getEmail())
                ->priority(Email::PRIORITY_HIGH)
                ->subject($subject)
                ->html($view);

        $response = $this->mailer->send($email);

        if ($response == 1) { 
            return ['status' => true, 'message' => 'E-mail enviado.'];
        }
        
        return ['status' => false, 'message' => 'E-mail não foi enviado.']; 
    }
}