<?php

namespace App\Controller\Admin;

use App\Entity\CadastroEstudos;
use App\Form\CadastroEstudosType;
use App\Form\ScientificStudiesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin")
 */
class CadastroEstudosController extends AbstractController
{
    /**
     * @Route("/cadastro-tipos", name="admin_cadastro_estudos")
     */
    public function index()
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'estudos-tipo', 'view');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */


        $types = $em->getRepository('App:CadastroEstudos')->findAll();

        return $this->render('admin/cadastroEstudos/index.html.twig', [
            "user_acess" => $user_acess['user_acess'],
            "types" => $types
        ]);
    }

    /**
     * @Route("/cadastro-estudos/novo", name="admin_cadastro_estudos_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'estudos-cientificos', 'create');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $studies_type = new CadastroEstudos();
        $form = $this->createForm(CadastroEstudosType::class, $studies_type);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $studies_type->setCreatedAt(new \DateTime());
            $studies_type->setUpdatedAt(new \DateTime());
            $new_type = $form->getData();

            //$news_categories->setUpdatedAt(new \DateTime());
            $em->persist($studies_type);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Tipo!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_cadastro_estudos');
        }
        return $this->render('admin/cadastroEstudos/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/cadastro_tipos/editar/{id}", name="admin_cadastro_estudos_edit")
     * @ParamConverter("id", class="App\Entity\CadastroEstudos", options={"id": "id"})
     */
    public function edit(Request $request, CadastroEstudos $studies_type, FileUploader $fileUploader)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'estudos-cientificos', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */
        /* dump($studies_type);
        die(); */
        $deleteForm = $this->createDeleteForm($studies_type);
        $form = $this->createForm(CadastroEstudosType::class, $studies_type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $news_specialty = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($news_specialty);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Tipo!', 'message' => 'atualizado com sucesso.']);
            return $this->redirectToRoute('admin_cadastro_estudos');
        }


        return $this->render('admin/cadastroEstudos/edit.html.twig', [
            "user_acess" => $user_acess['user_acess'],
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * @Route("/cadastro-estudos/{id}/deletar", name="admin_cadastro_estudos_delete")
     * @ParamConverter("id", class="App\Entity\CadastroEstudos", options={"id": "id"})
     */
    public function deleteAction(Request $request, CadastroEstudos $studies_type)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'estudos-cientificos', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */

        if ($studies_type) {
            $em->remove($studies_type);
            $em->flush();

            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Tipo!', 'message' => 'excluído com sucesso.']);
        }

        return $this->redirectToRoute('admin_cadastro_estudos');
    }


    private function createDeleteForm(CadastroEstudos $studies_type)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'estudos-cientificos', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_cadastro_estudos_delete', array('id' => $studies_type->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}