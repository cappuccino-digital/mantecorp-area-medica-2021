<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\UserAcess;
use App\Entity\UserNewPassword;
use App\Form\UserType;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
 * @Route("/admin/usuarios")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="admin_users")
     * @IsGranted("ROLE_ADMIN")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function index(Request $request)
    {   
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('App:User')->findBy([], ['created_at' => 'DESC']);

        return $this->render('admin/user/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/novo", name="admin_user_new")
     * @IsGranted("ROLE_ADMIN")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function new(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $em = $this->getDoctrine()->getManager();
       
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();     
            //roles
            if ($user->getProfile()->getSlug() == 'admin') {
                $role = $em->getRepository('App:Role')->findOneBy(['role' => 'ROLE_ADMIN']);
                $user->addRole($role);
            } else if ($user->getProfile()->getSlug() == 'dev') {
                $role = $em->getRepository('App:Role')->findOneBy(['role' => 'ROLE_DEV']);
                $user->addRole($role);

            } else if ($user->getProfile()->getSlug() == 'medico') {
                    $role = $em->getRepository('App:Role')->findOneBy(['role' => 'ROLE_MEDICAL']);
                    $user->addRole($role);
            } 
            // else {
            //     $role = $em->getRepository('App:Role')->findOneBy(['role' => 'ROLE_USER']);
            //     $user->addRole($role);
            // }

            $user->setPassword($encoder->encodePassword($user, $user->getPassword())); //password
            $user->setCreatedAt(new \DateTime());
            $user->setUpdatedAt(new \DateTime());
            $user->setLastLoginAt(new \DateTime());
                        
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            //liberando acesso para o perfil de dev e admin 
            if (($user->getProfile()->getSlug() == 'admin') || ($user->getProfile()->getSlug() == 'dev')) {
                
                $menus = $em->getRepository('App:Menu')->findBy(['is_active' => true]);
                    
                foreach ($menus as $key => $menu) {
                    $user_menu = new UserAcess();
                    $user_menu->setUsers($user);
                    $user_menu->setMenus($menu);
                    $user_menu->setIsCreate(true);
                    $user_menu->setIsUpdate(true);
                    $user_menu->setIsDelete(true);
                    $user_menu->setIsViewed(true);
                    $user_menu->setCreatedAt(new \DateTime());
                    $user_menu->setUpdatedAt(new \DateTime());

                    $em->persist($user_menu);
                    $em->flush();
                }
            }
            //end permissão 
           
            $this->addFlash('success', ['type'=>'success', 'title'=>'Usuário!', 'message'=>'cadastrado com sucesso.']);
            return $this->redirectToRoute('admin_user_acess', ['user_id' => $user->getId()]);
        }
      
        return $this->render('admin/user/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/editar/{id}", name="admin_user_edit")
     * @ParamConverter("id", class="App\Entity\User", options={"id": "id"})
     */
    public function edit(Request $request, User $user, UserPasswordEncoderInterface $encoder, FileUploader $fileUploader)
    {
        //checando segurança
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            if ($this->getUser()->getId() != $user->getId())
                throw $this->createNotFoundException('[01] - Acesso negado.');
        }

        $em = $this->getDoctrine()->getManager();
        $old_password = $user->getPassword();
        $old_roles = $user->getRoles();
        $avatarOld = $user->getAvatar();
     
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            //removendo o role de cadastro 
            $roles = isset($old_roles[0]) ? $old_roles[0] : null;

            if ($roles) {
                $roleUser = $em->getRepository('App:Role')->findOneByRole($roles);
                $user->removeRole($roleUser);
            }
            
            //password
            if (trim($user->getPassword())) {
                $checkPass = $encoder->isPasswordValid($user, $old_password);

                if (!$checkPass) {
                    $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
                }
            } else {
                $user->setPassword($old_password);
            }
                      
            //add new role
            if ($user->getProfile()->getSlug() == 'admin') {
                $role = $em->getRepository('App:Role')->findOneBy(['role' => 'ROLE_ADMIN']);
                $user->addRole($role);
            } else if ($user->getProfile()->getSlug() == 'dev') {
                $role = $em->getRepository('App:Role')->findOneBy(['role' => 'ROLE_DEV']);
                $user->addRole($role);

            } else if ($user->getProfile()->getSlug() == 'medico') {
                $role = $em->getRepository('App:Role')->findOneBy(['role' => 'ROLE_MEDICAL']);
                $user->addRole($role);
            }
            // else {
            //     $role = $em->getRepository('App:Role')->findOneBy(['role' => 'ROLE_USER']);
            //     $user->addRole($role);
            // }

            //upload image
            $avatarFile = $form->get('avatar')->getData();
           
            if ($avatarFile) {
                $fileName = $fileUploader->upload($avatarFile, 'user');
                $user->setAvatar($fileName);
            } else {
                if (!$avatarOld) {
                    $user->setAvatar('');
                } else {
                    $user->setAvatar($avatarOld);
                }
            }
            
            $user->setUpdatedAt(new \DateTime());
            $user->setLastLoginAt(new \DateTime());
            $em->persist($user);
            $em->flush();
            
            $this->addFlash('success',['type'=>'success', 'title'=>'Usuário!', 'message'=>'atualizado com sucesso.']);

            if ($this->getUser()->getId() == $user->getId()) {
                return $this->redirectToRoute('admin_user_edit', ['id' =>  $user->getId()]);
            }

            return $this->redirectToRoute('admin_users');
        }
      
        return $this->render('admin/user/edit.html.twig', [
            'form' => $form->createView(),
            'user' => $user
        ]);
    }

    /**
     * @Route("/{id}/deletar", name="admin_user_delete")
     * @IsGranted("ROLE_ADMIN")
     * @Security("is_granted('ROLE_ADMIN')")
     * @ParamConverter("id", class="App\Entity\User", options={"id": "id"})
     */
    public function delete(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($user) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        $this->addFlash('success', ['type'=>'success', 'title'=>'Usuário!', 'message'=>'deletado com sucesso.']);

        return $this->redirectToRoute('admin_users');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $post The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @Route("/{id}/mais-opcoes", name="admin_user_change_password")
     * @IsGranted("ROLE_ADMIN")
     * @Security("is_granted('ROLE_ADMIN')")
     * @ParamConverter("id", class="App\Entity\User", options={"id": "id"})
     */
    public function userChangePassword(Request $request, User $user)
    {
        $em = $this->getDoctrine()->getManager();

        $token = sha1($user->getId().$user->getCreatedAt()->getTimestamp().$user->getSalt().$user->getPassword()).dechex(time()).dechex($user->getId());
        
        $expiresAt = new \DateTime();
        $expiresAt->add(new \DateInterval('PT8H'));
        $newPassword = $em->getRepository('App:UserNewPassword')->findOneByUser($user);

        if (!$newPassword) {
            $newPassword = new UserNewPassword();
        }
        
        $newPassword->setExpiresAt($expiresAt);
        $newPassword->setToken($token);
        $newPassword->setIsValid(true);
        $newPassword->setUser($user);

        $em->persist($newPassword);
        $em->flush();

        $url = $this->generateUrl('change-password', array('token' => $newPassword->getToken()), UrlGenerator::ABSOLUTE_URL);
                   
        $this->addFlash('success', ['type' => 'success', 'title' => $user->getName() . ", ", 'message'=> "URL de recuperação de senha: <strong>$url</strong>"]);

        return $this->redirectToRoute('admin_users');
    }
}
