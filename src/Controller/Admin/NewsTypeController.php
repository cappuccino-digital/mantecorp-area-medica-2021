<?php

namespace App\Controller\Admin;

use App\Entity\NewsType;
use App\Form\TypeNews;
use App\Form\TypeNewsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/noticias/tipo")
 */
class NewsTypeController extends AbstractController
{
    /**
     * @Route("/", name="admin_news_type")
     */
    public function index()
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'tipo-de-noticia', 'view');


        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $news_types = $em->getRepository('App:NewsType')->findAll();

        return $this->render('admin/news_type/index.html.twig', [
            'news_types' => $news_types,
            'user_acess' => $user_acess['user_acess']
        ]);
    }

    /**
     * @Route("/novo", name="admin_news_type_new")
     */
    public function new(Request $request)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'tipo-de-noticia', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $news_categories = new NewsType();
        $form = $this->createForm(TypeNewsType::class, $news_categories);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $news_categories = $form->getData();

            $news_categories->setCreatedAt(new \DateTime());
            //$news_categories->setUpdatedAt(new \DateTime());
            $em->persist($news_categories);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Tipo de Notícias!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_news_type');
        }

        return $this->render('admin/news_type/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/editar/{id}", name="admin_news_type_edit")
     * @ParamConverter("id", class="App\Entity\NewsType", options={"id": "id"})
     */
    public function edit(Request $request, NewsType $news_type)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'tipo-de-noticia', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $deleteForm = $this->createDeleteForm($news_type);
        $form = $this->createForm(TypeNewsType::class, $news_type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($news_type);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Tipo de Notícias!', 'message' => 'atualizado com sucesso.']);
            return $this->redirectToRoute('admin_news_type_edit', ['id' => $news_type->getId()]);
        }

        return $this->render('admin/news_type/edit.html.twig', [
            'news_categories' => $news_type,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}/deletar", name="admin_news_type_delete")
     * @ParamConverter("id", class="App\Entity\NewsType", options={"id": "id"})
     */
    public function deleteAction(Request $request, NewsType $news_type)
    {


        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'tipo-de-noticia', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */


        $news = $em->getRepository('App:NewsType')->getNoticeByTypeId($news_type->getId());


        if (count($news) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Tipo de Notícias!', 'message' => 'não foi possível excluir, esse tipo de notícia está relacionado com outras notícias.']);
            return $this->redirectToRoute('admin_news_type');
        }

        if ($news_type) {
            $em->remove($news_type);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Tipo de Notícias!', 'message' => 'excluído com sucesso.']);
        }

        return $this->redirectToRoute('admin_news_type');
    }

    /**
     * Creates a form to delete a news_categories entity.
     *
     * @param NewsCategories $news_categories The news_categories entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(NewsType $news_type)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'tipo-de-noticia', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_news_type_delete', array('id' => $news_type->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}