<?php

namespace App\Controller\Admin;

use App\Service\AuthUserOldService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;

class UserAuthenticationController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, UserPasswordEncoderInterface $encoder, AuthUserOldService $auth_user_old_service): Response
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('_username') || $request->query->get('_password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                return $this->redirectToRoute('admin_dashboard');
            }
            return $this->redirectToRoute('home');
        }
      
        if ($request->isMethod('GET')) {
            if ($request->query->has('form')) {
                throw $this->createNotFoundException('A página não foi encontrada');
            }
         
            return $this->render('front/user/login.html.twig', ['error'=> null]);
        }
     
        $username = $request->request->get('_username');
        $password = $request->request->get('_password');
      
        if (!$username || !$password) {
            return $this->render('front/user/login.html.twig', [
                'error' => 'Todos os campos devem ser preenchidos.'    
            ]);
        }

        $em = $this->getDoctrine()->getManager();		
        $user = $em->getRepository('App:User')->findOneBy(['username' => $username, 'is_active' => true]);

        if (!$user) {
            $user_temporary = $em->getRepository('App:UserTemporary')->findOneBy(['username' => $username]);

            if ($user_temporary) {
                if ($user_temporary->getIsUserOld()) { //usuário do site antigo
                    $login_service = $auth_user_old_service->login($username, $password); //autenticação na base do site antigo por conta criptografia da senha 
                    // dump($login_service);
                    // die();
                    if ($login_service['status']) {
                        
                        $user_temporary->setPassword($encoder->encodePassword($user_temporary, $password));
            
                        $em->persist($user_temporary);
                        $em->flush();

                        $response = $this->redirectToRoute('register-doctor-consultation');
                        $response->headers->setCookie(new Cookie('_u', $user_temporary->getId()));
                        return $response;
                        
                    } else {
                        $error = $login_service['message'];
                    }

                } else { 
                    if ($user_temporary->getIsFinished()) { //usuário que ainda não confirmou o cadastro no e-mail
                        
                        $bool = ($encoder->isPasswordValid($user_temporary, $password)) ? true : false;

                        if ($bool) {
                            $url = $this->generateUrl('user-remember-confirm-register-token', array('token' => $user_temporary->getToken()), UrlGenerator::ABSOLUTE_URL);

                            $error = '<p>Sua conta está aguardando verificação de e-mail.</p>' .
                                     '<p><a href="' . $url . '" style="text-decoration: underline;"><strong>Clique aqui</strong></a> para reenviar caso não tenha recebido o e-mail.</p>';
                        } else {
                            $error = 'Usuário não encontrado';
                        }
                       
                    } else { //usuário que ainda não finalizou o cadastro
                        $error = 'Por favor, finalize seu cadastro';
                    }
                    
                }
                
            } else {
                $error = 'Usuário não encontrado';
            }
           
            return $this->render('front/user/login.html.twig', [
                'error' => $error  
            ]);
        }
        
        $bool = ($encoder->isPasswordValid($user, $password)) ? true : false;

        if (!$bool) {
                        
            return $this->render('front/user/login.html.twig', [
                'error' => 'Usuário ou senha incorretos'    
            ]);
        }

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));

        // Dispara o evento de login manualmente
        $event = new InteractiveLoginEvent($request, $token);
        $dispatcher = new EventDispatcher(); //dispara
        $dispatcher->dispatch('security.interactive_login', $event);
        //$this->get('event_dispatcher')->dispatch('security.interactive_login', $event);
        //dump($user); die();

        $user->setLastLoginAt(new \DateTime());
            
        $em->persist($user);
        $em->flush();

        //Redirecionamento para a referência após o login 
        $url_reference = $request->cookies->get('_referenceURL') ? $request->cookies->get('_referenceURL') : null;
        if ($url_reference) {
            $response = $this->redirect($url_reference);
            $response->headers->setCookie(new Cookie('_referenceURL', false));
            return $response;
        }   
        /** END */
        
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('admin_dashboard');
        } else {
            return $this->redirectToRoute('home');
        }
    }

    
    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request)
    {

    }

    /**
     * @Route("/reenviar-confimacao-cadastro/{token}", name="user-remember-confirm-register-token")
     */
    public function userSendEmailConfirmRegister(Request $request, $token, MailerInterface $mailer)
    {
        $em = $this->getDoctrine()->getManager();
        
        if ($token) {
            $user_temporary = $em->getRepository('App:UserTemporary')->findOneBy(['token' => $token]);

            if (!$user_temporary) {
                return $this->redirectToRoute('home');
            }
            
            $url = $this->generateUrl('user-confirm-register-token', array('token' => $user_temporary->getToken()), UrlGenerator::ABSOLUTE_URL);
            //send e-mail
            $email = (new Email())
                ->from(new Address('portalmantecorpfarmasa@gmail.com', 'Mantecorp Farmasa - Área médica'))
                ->to($user_temporary->getEmail())
                //->addBcc('ana.morales@cappuccinodigital.com.br')
                //->replyTo($contact->getEmail())
                ->priority(Email::PRIORITY_HIGH)
                ->subject('Mantecorp Farmasa - Confirmação de cadastro')
                ->html($this->renderView(
                    'front/home/mkt-user-confirm-register.html.twig',
                    [
                        'user_temporary' => $user_temporary,
                        'url' => $url
                    ]
                ));

            $mailer->send($email);

            $url = $this->generateUrl('home', array('p' => 'cf'), UrlGenerator::ABSOLUTE_URL);

            return $this->redirect($url);
        } 
                     
        return $this->redirectToRoute('home');
    }

}