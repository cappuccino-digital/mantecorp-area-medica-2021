<?php

namespace App\Controller\Admin;

use App\Entity\Menu;
use App\Entity\UserAcess;
use App\Form\MenuType;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin/menu")
 */
class MenuController extends AbstractController
{
    /**
     * @Route("/", name="admin_menu")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $menus = $em->getRepository('App:Menu')->findBy(['is_caterer' => false], ['menu_category'=>'ASC']);
        //dump($menus); die();
        return $this->render('admin/menu/index.html.twig', [
            'menus' => $menus,
        ]);
    }

    /**
     * @Route("/novo", name="admin_menu_new")
    */
    public function new(Request $request)
    {
        $menu = new Menu();
        $form = $this->createForm(MenuType::class, $menu);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $menu = $form->getData();
           
            $em = $this->getDoctrine()->getManager();
            $menu->setIsCaterer(false); //não é um perfil de fornecedor
            $menu->setCreatedAt(new DateTime());
            $menu->setUpdatedAt(new DateTime());
            $em->persist($menu);
            $em->flush();

            //permissão para dev e admin 
            $users = $em->getRepository('App:User')->findByUserRoles();
            
            foreach ($users as $key => $u) {
                $user = $em->getRepository('App:User')->findOneById($u['id']);
                $user_menu = $em->getRepository('App:UserAcess')->findOneBy(['users' => $user, 'menus' => $menu]);
              
                if (!$user_menu) {

                    $user_menu = new UserAcess();
                    $user_menu->setUsers($user);
                    $user_menu->setMenus($menu);
                    $user_menu->setIsCreate(true);
                    $user_menu->setIsUpdate(true);
                    $user_menu->setIsDelete(true);
                    $user_menu->setIsViewed(true);
                    $user_menu->setCreatedAt(new DateTime());
                    $user_menu->setUpdatedAt(new DateTime());

                    $em->persist($user_menu);
                    $em->flush();
                }
            }
            //end

            $this->addFlash('success', ['type'=>'success', 'title'=>'Menu!', 'message'=>'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_menu');
        }

        return $this->render('admin/menu/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    
    /**
     * @Route("/editar/{id}", name="admin_menu_edit")
     * @ParamConverter("id", class="App\Entity\Menu", options={"id": "id"})
    */
    public function edit(Request $request, Menu $menu)
    {
        $deleteForm = $this->createDeleteForm($menu);
        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $menu->setIsCaterer(false); //não é um perfil de fornecedor
            $menu->setUpdatedAt(new DateTime());
            $em->persist($menu);
            $em->flush();

            $this->addFlash('success', ['type'=>'success', 'title'=>'Menu!', 'message'=>'atualizado com sucesso.']);
            return $this->redirectToRoute('admin_menu_edit', ['id'=> $menu->getId()]);
        }

        return $this->render('admin/menu/edit.html.twig', [
            'menu' => $menu,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}/deletar", name="admin_menu_delete")
     * @ParamConverter("id", class="App\Entity\Menu", options={"id": "id"})
    */
    public function deleteAction(Request $request, Menu $menu)
    {
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->findByMenus($menu);
      
        if ($user_acess) {
            $this->addFlash('danger', ['type'=>'danger', 'title'=>'Menu!', 'message'=>'não foi possível excluir, esse menu está relacionado com outros usuários.']);
            return $this->redirectToRoute('admin_menu');
        }

        if ($menu) {
            $em->remove($menu);
            $em->flush();

            $this->addFlash('danger', ['type'=>'danger', 'title'=>'Menu!', 'message'=>'excluído com sucesso.']);
        }

        return $this->redirectToRoute('admin_menu');
    }

    /**
     * Creates a form to delete a product entity.
     *
     * @param Product $product The product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Menu $menu)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_menu_delete', array('id' => $menu->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


}
