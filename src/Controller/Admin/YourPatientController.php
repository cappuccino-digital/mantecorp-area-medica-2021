<?php

namespace App\Controller\Admin;

use App\Entity\ScientificStudies;
use App\Entity\YourPatient;
use App\Form\ScientificStudiesType;
use App\Form\YourPatientType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin")
 */
class YourPatientController extends AbstractController
{
    /**
     * @Route("/seu-paciente", name="admin_your_patient")
     */
    public function index()
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'seu-paciente', 'view');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */


        $yourpatient = $em->getRepository('App:YourPatient')->getAllTitles();

        return $this->render('admin/your_patient/index.html.twig', [
            "user_acess" => $user_acess['user_acess'],
            "yourpatients" => $yourpatient
        ]);
    }

    /**
     * @Route("/seu-paciente/novo", name="admin_your_patient_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'seu-paciente', 'create');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $new_material = new YourPatient();
        $form = $this->createForm(YourPatientType::class, $new_material);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $news_study = $form->getData();

            $validaTipo = $request->request->get("your_patient");
            if ($validaTipo["materialType"] == "File") {

                $news_study->setTitleV("");
                $news_study->setResumeV("");
                $news_study->setVideo("");
            } else {

                $news_study->setTitleM("");
                $news_study->setResumeM("");
                $news_study->setFile("");
            }

            $file = $form->get('file')->getData();
            if ($file) {
                $fileName = $fileUploader->upload($file, 'your-patient');
                $news_study->setFile($fileName);
            }

            //$news_categories->setUpdatedAt(new \DateTime());
            $em->persist($news_study);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Novo Material!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_your_patient');
        }
        return $this->render('admin/your_patient/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("seu-paciente/editar/{id}", name="admin_your_patient_edit")
     * @ParamConverter("id", class="App\Entity\YourPatient", options={"id": "id"})
     */
    public function edit(Request $request, YourPatient $edit_material, FileUploader $fileUploader)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'seu-paciente', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */


        $fileOld = $edit_material->getFile();

        $deleteForm = $this->createDeleteForm($edit_material);
        $form = $this->createForm(YourPatientType::class, $edit_material);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $news_study = $form->getData();
            $validaTipo = $request->request->get("your_patient");
            if ($validaTipo["materialType"] == "File") {
                $file = $form->get('file')->getData();
                if ($file) {
                    $fileName = $fileUploader->upload($file, 'your-patient');
                    $edit_material->setFile($fileName);
                } else {

                    $edit_material->setFile($fileOld);
                }

                $news_study->setTitleV("");
                $news_study->setResumeV("");
                $news_study->setVideo("");
            } else {
                $news_study->setTitleM("");
                $news_study->setResumeM("");
                $news_study->setFile("");
            }




            $em = $this->getDoctrine()->getManager();
            $em->persist($news_study);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Materiais do Paciênte!', 'message' => 'atualizado com sucesso.']);
            return $this->redirectToRoute('admin_your_patient');
        }


        return $this->render('admin/your_patient/edit.html.twig', [
            "user_acess" => $user_acess['user_acess'],
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * @Route("seu-paciente/{id}/deletar", name="admin_your_patient_delete")
     * @ParamConverter("id", class="App\Entity\YourPatient", options={"id": "id"})
     */
    public function deleteAction(Request $request, YourPatient $yourPatient)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'seu-paciente', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */

        if ($yourPatient) {
            $em->remove($yourPatient);
            $em->flush();

            $this->addFlash('danger', ['type' => 'success', 'title' => 'Estudos!', 'message' => 'excluído com sucesso.']);
        }

        return $this->redirectToRoute('admin_your_patient');
    }


    private function createDeleteForm(YourPatient $new_material)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'seu-paciente', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_your_patient_delete', array('id' => $new_material->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}