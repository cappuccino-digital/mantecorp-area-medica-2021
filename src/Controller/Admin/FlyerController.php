<?php

namespace App\Controller\Admin;

use App\Entity\Flyer;
use App\Form\FlyerType;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class FlyerController extends AbstractController
{
    /**
     * @Route("/admin/flyer", name="admin_flyer")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'flyer-produtos', 'view');
        $flyers = $em->getRepository('App:Flyer')->findAllFlyers();


        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */


        return $this->render('admin/flyer/index.html.twig', [
            "user_acess" => $user_acess['user_acess'],
            "flyers" => $flyers
        ]);
    }

    /**
     * @Route("/admin/flyer/new", name="admin_flyer_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'flyer-produtos', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $new_flyer = new Flyer();
        $form = $this->createForm(FlyerType::class, $new_flyer);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $new_flyer->setCreatedAt(new \DateTime());
            $new_flyer->setUpdatedAt(new \DateTime());
            $new_flyer = $form->getData();

            $file = $form->get('file')->getData();

            if ($file) {
                $fileNameMobile = $fileUploader->upload($file, 'flyers');
                $new_flyer->setFile($fileNameMobile);
            }


            $em->persist($new_flyer);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Folheto!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_flyer');
        }

        return $this->render('admin/flyer/new.html.twig', [
            'user_acess' => $user_acess['user_acess'],
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/flyer/{id}", name="admin_flyer_edit")
     */
    public function edit(Request $request, FileUploader $fileUploader, Flyer $flyer)
    {
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'flyer-produtos', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $form = $this->createForm(FlyerType::class, $flyer);
        $fileOld = $flyer->getFile();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $flyer->setUpdatedAt(new \DateTime());
            $new_flyer = $form->getData();

            $file = $form->get('file')->getData();

            if ($file) {
                $fileNameMobile = $fileUploader->upload($file, 'flyers');
                $new_flyer->setFile($fileNameMobile);
            } else {
                $new_flyer->setFile($fileOld);
            }


            $em->persist($new_flyer);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Folheto!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_flyer');
        }

        return $this->render('admin/flyer/edit.html.twig', [
            'user_acess' => $user_acess['user_acess'],
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/admin/flyer/delete/{id}", name="admin_flyer_delete")
     */
    public function delete(Request $request, Flyer $flyer)
    {
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'flyer-produtos', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */

        $form = $this->createDeleteForm($flyer);
        $form->handleRequest($request);


        if ($flyer) {
            $em->remove($flyer);
            $em->flush();

            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Folheto!', 'message' => 'excluído com sucesso.']);
        }

        return $this->redirectToRoute('admin_flyer');
    }

    /**
     * Creates a form to delete a news_categories entity.
     *
     * @param NewsCategories $news_categories The news_categories entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Flyer $flyer)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'flyer-produtos', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_flyer_delete', array('id' => $flyer->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}