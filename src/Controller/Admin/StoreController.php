<?php

namespace App\Controller\Admin;


use App\Entity\Store;
use App\Form\StoreType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class StoreController extends AbstractController
{
    /**
     * @Route("/admin/lojas", name="admin_lojas")
     */
    public function index()
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'cadastro-de-lojas', 'view');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);


        $stores = $em->getRepository('App:Store')->findAll([], ['created_at' => 'DESC']);
        return $this->render('admin/stores/index.html.twig', [
            'user_acess' => $user_acess['user_acess'],
            'stores' => $stores
        ]);
    }

    /**
     * @Route("/admin/lojas/new", name="admin_lojas_new")
     */
    public function new(Request $request)
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'produto-Categorias', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $stores = new Store();
        $form = $this->createForm(StoreType::class, $stores);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $stores = $form->getData();
            $em->persist($stores);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Loja!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_lojas');
        }

        return $this->render('admin/stores/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/lojas/{id}", name="admin_lojas_edit")
     */
    public function edit(Request $request, Store $store)
    {
        dump("teste");
        die();
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'produto-Categorias', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $form = $this->createForm(StoreType::class, $store);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $store = $form->getData();
            $em->persist($store);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Loja!', 'message' => 'Atualizada com sucesso.']);

            return $this->redirectToRoute('admin_lojas');
        }

        return $this->render('admin/stores/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/lojas/{id} ", name="admin_lojas_delete")
     */
    public function delete()
    {

        $em = $this->getDoctrine()->getManager();

        return $this->render('admin/dashboard/index.html.twig', []);
    }
}
