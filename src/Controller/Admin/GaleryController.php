<?php

namespace App\Controller\Admin;

use App\Entity\Galery;
use App\Form\GaleryType;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class GaleryController extends AbstractController
{
    /**
     * @Route("/galeria", name="admin_galery")
     */
    public function index()
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'galery', 'view');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $images = $em->getRepository('App:Galery')->findAll([], ['created_at' => 'DESC']);

        return $this->render('admin/galery/index.html.twig', [
            'images' => $images,
            'user_acess' => $user_acess['user_acess']
        ]);
    }


    /**
     * @Route("/admin/galeria/new", name="admin_galery_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'galery', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $galery = new Galery();
        $galery->setCreatedAt(new \DateTime());
        $galery->setUpdatedAt(new \DateTime());


        $form = $this->createForm(GaleryType::class, $galery);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $galery = $form->getData();
            $imageFile = $form->get('image')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'galery');
                $galery->setImage($fileName);
            }

            $em->persist($galery);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Imagem!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_galery');
        }

        return $this->render('admin/galery/new.html.twig', [
            'user_acess' => $user_acess['user_acess'],
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/admin/galeria/edit/{id}", name="admin_galery_edit")
     */
    public function edit(Request $request, Galery $galery, FileUploader $fileUploader)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'galery', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $imageOld = $galery->getImage();
        $galery->setUpdatedAt(new \DateTime());


        $form = $this->createForm(GaleryType::class, $galery);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $galery = $form->getData();

            $imageFile = $form->get('image')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'galery');

                $galery->setImage($fileName);
            } else {
                $galery->setImage($imageOld);
            }
            $em->persist($galery);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Imagem!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_galery');
        }

        return $this->render('admin/galery/new.html.twig', [
            'user_acess' => $user_acess['user_acess'],
            'form' => $form->createView()
        ]);
    }



    /**
     * @Route("galeria/{id}/deletar", name="admin_galery_delete")
     */
    public function delete(Request $request, Galery $galery)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'galery', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */

        $form = $this->createDeleteForm($galery);
        $form->handleRequest($request);

        if ($galery) {
            $em->remove($galery);
            $em->flush();
        }

        $this->addFlash('success', ['type' => 'success', 'title' => 'galery!', 'message' => 'deletado com sucesso.']);

        return $this->redirectToRoute('admin_galery');
    }



    /**
     * Creates a form to delete a user entity.
     *
     * @param Products $products The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Galery $galery)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'galery', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_galery_delete', array('id' => $galery->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}