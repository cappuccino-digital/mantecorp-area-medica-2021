<?php

namespace App\Controller\Admin;

use App\Entity\Profile;
use App\Form\ProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin/perfil")
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/", name="admin_profile")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $profiles = $em->getRepository('App:Profile')->findBy([], ['created_at' => 'DESC']);

        return $this->render('admin/profile/index.html.twig', [
            'profiles' => $profiles,
        ]);
    }

    /**
     * @Route("/novo", name="admin_profile_new")
     */
    public function new(Request $request)
    {
        $profile = new Profile();
        $form = $this->createForm(ProfileType::class, $profile);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $profile = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $profile->setIsCaterer(false); //não é um perfil de fornecedor
            $profile->setCreatedAt(new \DateTime());
            $profile->setUpdatedAt(new \DateTime());
            $em->persist($profile);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Perfil!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_profile');
        }

        return $this->render('admin/profile/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/editar/{id}", name="admin_profile_edit")
     * @ParamConverter("id", class="App\Entity\Profile", options={"id": "id"})
     */
    public function edit(Request $request, Profile $profile)
    {
        $deleteForm = $this->createDeleteForm($profile);
        $form = $this->createForm(ProfileType::class, $profile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $profile->setIsCaterer(false); //não é um perfil de fornecedor
            $profile->setUpdatedAt(new \DateTime());
            $em->persist($profile);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Perfil!', 'message' => 'atualizado com sucesso.']);
            return $this->redirectToRoute('admin_profile_edit', ['id' => $profile->getId()]);
        }

        return $this->render('admin/profile/edit.html.twig', [
            'profile' => $profile,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}/deletar", name="admin_profile_delete")
     * @ParamConverter("id", class="App\Entity\Profile", options={"id": "id"})
     */
    public function deleteAction(Request $request, Profile $profile)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->findByProfile($profile);

        if ($user) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Perfil!', 'message' => 'não foi possível excluir, esse perfil está relacionado com outros usuários.']);
            return $this->redirectToRoute('admin_profile');
        }

        if ($profile) {
            $em->remove($profile);
            $em->flush();

            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Perfil!', 'message' => 'excluído com sucesso.']);
        }

        return $this->redirectToRoute('admin_profile');
    }

    /**
     * Creates a form to delete a profile entity.
     *
     * @param Profile $profile The profile entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Profile $menu)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_profile_delete', array('id' => $menu->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
