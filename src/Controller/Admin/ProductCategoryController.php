<?php

namespace App\Controller\Admin;

use App\Entity\ProductCategory;
use App\Form\ProductCategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin/produto/categoria")
 */
class ProductCategoryController extends AbstractController
{
    /**
     * @Route("/", name="admin_product_category")
     */
    public function index(Request $request)
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'produto-categorias', 'view');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $product_category = $em->getRepository('App:ProductCategory')->findBy([], ['created_at' => 'DESC']);

        return $this->render('admin/product_category/index.html.twig', [
            'product_categorys' => $product_category,
            'user_acess' => $user_acess['user_acess']
        ]);
    }

    /**
     * @Route("/novo", name="admin_product_category_new")
     */
    public function new(Request $request)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'produto-Categorias', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $product_category = new ProductCategory();
        $form = $this->createForm(ProductCategoryType::class, $product_category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product_category = $form->getData();
            $title_to_slug = str_replace(" ", "-", $product_category->getName());
            $product_category->setSlug($title_to_slug);
            $product_category->setCreatedAt(new \DateTime());
            $product_category->setUpdatedAt(new \DateTime());
            $em->persist($product_category);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Categoria!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_product_category');
        }

        return $this->render('admin/product_category/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/editar/{id}", name="admin_product_category_edit")
     * @ParamConverter("id", class="App\Entity\ProductCategory", options={"id": "id"})
     */
    public function edit(ProductCategory $product_category, Request $request, FileUploader $fileUploader)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'produto-categorias', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */


        $form = $this->createForm(ProductcategoryType::class, $product_category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product_category = $form->getData();
            $title_to_slug = str_replace(" ", "-", $product_category->getName());
            $product_category->setSlug($title_to_slug);
            $product_category->setUpdatedAt(new \DateTime());
            $em->persist($product_category);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Categoria!', 'message' => 'atualizado com sucesso.']);

            return $this->redirectToRoute('admin_product_category');
        }

        return $this->render('admin/product_category/edit.html.twig', [
            'form' => $form->createView(),
            'product_category' => $product_category
        ]);
    }

    /**
     * @Route("/{id}/deletar", name="admin_product_category_delete")
     * @ParamConverter("id", class="App\Entity\ProductCategory", options={"id": "id"})
     */
    public function delete(Request $request, ProductCategory $product_category)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'produto-categorias', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */

        $product = $em->getRepository('App:Products')->createQueryBuilder('p')
                ->join('p.category', 'c')
                ->where('c.id = :category')
                ->setParameter('category', $product_category)
                ->getQuery()
                ->getResult();

        if ($product) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Categoria!', 'message' => 'Não foi possível excluir. A categoria " ' . $product_category->getName() .' " está relacionado com outros produtos.']);
            return $this->redirectToRoute('admin_product_category');
        }

        $form = $this->createDeleteForm($product_category);
        $form->handleRequest($request);

        if ($product_category) {
            $em->remove($product_category);
            $em->flush();
        }

        $this->addFlash('success', ['type' => 'success', 'title' => 'Categoria!', 'message' => 'deletado com sucesso.']);

        return $this->redirectToRoute('admin_product_category');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param ProductCategory $product_category The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ProductCategory $product_category)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'produto-Categorias', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */
      
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_product_category_delete', array('id' => $product_category->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
