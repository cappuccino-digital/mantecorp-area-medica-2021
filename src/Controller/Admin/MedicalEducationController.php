<?php

namespace App\Controller\Admin;

use App\Entity\MedicalEducation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/medical-education")
 */
class MedicalEducationController extends AbstractController
{


    /**
     * @Route("/", name="admin_medical_education")
     */
    public function index()
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'curso', 'view');


        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $medicalEducation = $em->getRepository('App:MedicalEducation')->findAll();

        return $this->render('admin/medicalEducation/index.html.twig', [
            'news_types' => $medicalEducation,
            'user_acess' => $user_acess['user_acess']
        ]);
    }
}