<?php

namespace App\Controller\Admin;

use App\Entity\Menu;
use App\Entity\MenuCategory;
use App\Form\MenuCategoryType;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin/menu/categoria")
 */
class MenuCategoryController extends AbstractController
{
    /**
     * @Route("/", name="admin_menu_category")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $menu_categories = $em->getRepository('App:MenuCategory')->findBy([], ['created_at'=>'DESC']);

        return $this->render('admin/menu_category/index.html.twig', [
            'menu_categories' => $menu_categories,
        ]);
    }

    /**
     * @Route("/novo", name="admin_menu_category_new")
    */
    public function new(Request $request)
    {
        $menu = new MenuCategory();
        $form = $this->createForm(MenuCategoryType::class, $menu);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $menu = $form->getData();
           
            $em = $this->getDoctrine()->getManager();
            $menu->setIsCaterer(false); //não é um perfil de fornecedor
            $menu->setCreatedAt(new DateTime());
            $menu->setUpdatedAt(new DateTime());
            $em->persist($menu);
            $em->flush();

            $this->addFlash('success', ['type'=>'success', 'title'=>'Menu Categoria!', 'message'=>'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_menu');
        }

        return $this->render('admin/menu_category/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/editar/{id}", name="admin_menu_category_edit")
     * @ParamConverter("id", class="App\Entity\MenuCategory", options={"id": "id"})
    */
    public function edit(Request $request, MenuCategory $menu_category)
    {
       // $deleteForm = $this->createDeleteForm($menu_category);
        $form = $this->createForm(MenuCategoryType::class, $menu_category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $menu_category->setIsCaterer(false); //não é um perfil de fornecedor
            $menu_category->setUpdatedAt(new DateTime());
            $em->persist($menu_category);
            $em->flush();

            $this->addFlash('success', ['type'=>'success', 'title'=>'Menu Categoria!', 'message'=>'atualizado com sucesso.']);
            return $this->redirectToRoute('admin_menu_category_edit', ['id'=> $menu_category->getId()]);
        }

        return $this->render('admin/menu_category/edit.html.twig', [
            'menu_category' => $menu_category,
            'form' => $form->createView(),
            //'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}/deletar", name="admin_menu_category_delete")
     * @ParamConverter("id", class="App\Entity\MenuCategory", options={"id": "id"})
    */
    public function deleteAction(Request $request, MenuCategory $menu_category)
    {
        $em = $this->getDoctrine()->getManager();
        $menu = $em->getRepository('App:Menu')->findOneBy(['menu_category' => $menu_category]);
      
        if ($menu) {
            $this->addFlash('danger', ['type'=>'danger', 'title'=>'Menu Categoria!', 'message'=>'não foi possível excluir, esse menu está relacionado com outros usuários.']);
            return $this->redirectToRoute('admin_menu');
        }

        if ($menu_category) {
            $em->remove($menu_category);
            $em->flush();

            $this->addFlash('danger', ['type'=>'danger', 'title'=>'Menu Categoria!', 'message'=>'excluído com sucesso.']);
        }

        return $this->redirectToRoute('admin_menu_category');
    }

    /**
     * Creates a form to delete a menu_category entity.
     *
     * @param MenuCategory $menu_category The menu_category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Menu $menu)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_menu_category_delete', array('id' => $menu->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


}
