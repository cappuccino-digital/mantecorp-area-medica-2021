<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\UserTemporary;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;

/**
 * @Route("/admin/usuarios/pendente")
 */
class UserTemporaryController extends AbstractController
{
    /**
     * @Route("/", name="admin_users_temporary")
     * @IsGranted("ROLE_ADMIN")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function index(Request $request)
    {   
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('App:UserTemporary')->findBy([], ['created_at' => 'DESC']);

        return $this->render('admin/user_temporary/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/{id}/enviar-email-confirmacao-cadastro", name="admin_user_send_email_confirm_register")
     * @IsGranted("ROLE_ADMIN")
     * @Security("is_granted('ROLE_ADMIN')")
     * @ParamConverter("id", class="App\Entity\UserTemporary", options={"id": "id"})
     */
    public function userSendEmailConfirmRegister(Request $request, UserTemporary $user_temporary, MailerInterface $mailer)
    {
        $em = $this->getDoctrine()->getManager();
        
        if ($user_temporary->getIsFinished()) {
            //send e-mail
            $url = $this->generateUrl('user-confirm-register-token', array('token' => $user_temporary->getToken()), UrlGenerator::ABSOLUTE_URL);
            
            $email = (new Email())
                ->from(new Address('portalmantecorpfarmasa@gmail.com', 'Mantecorp Farmasa - Área médica'))
                ->to($user_temporary->getEmail())
                //->addBcc('ana.morales@cappuccinodigital.com.br')
                //->replyTo($contact->getEmail())
                ->priority(Email::PRIORITY_HIGH)
                ->subject('Mantecorp Farmasa - Confirmação de cadastro')
                ->html($this->renderView(
                    'front/home/mkt-user-confirm-register.html.twig',
                    [
                        'user_temporary' => $user_temporary,
                        'url' => $url
                    ]
                ));

            $mailer->send($email);

            $this->addFlash('success', ['type' => 'success', 'title' => $user_temporary->getName() . ", ", 'message' => " e-mail de confirmação de cadastro enviado com sucesso."]);
            //$this->addFlash('success', ['type' => 'success', 'title' => $user_temporary->getName() . ", ", 'message' => " URL de confirmação de cadastro: <strong>$url</strong>"]);

            return $this->redirectToRoute('admin_users_temporary');
        } 
                          
        $this->addFlash('success', ['type' => 'danger', 'title' => $user_temporary->getName() . ", ", 'message' => " o usuário ainda não finalizou o cadastro"]);

        return $this->redirectToRoute('admin_users_temporary');
    }

    /**
     * @Route("/{id}/gerar-link-confirmacao-cadastro", name="admin_user_generate_url_confirm_register")
     * @IsGranted("ROLE_ADMIN")
     * @Security("is_granted('ROLE_ADMIN')")
     * @ParamConverter("id", class="App\Entity\UserTemporary", options={"id": "id"})
     */
    public function userGenerateUrlConfirmRegister(Request $request, UserTemporary $user_temporary, MailerInterface $mailer)
    {
        $em = $this->getDoctrine()->getManager();
        
        if ($user_temporary->getIsFinished()) {
            //send e-mail
            $url = $this->generateUrl('user-confirm-register-token', array('token' => $user_temporary->getToken()), UrlGenerator::ABSOLUTE_URL);
        
            $this->addFlash('success', ['type' => 'success', 'title' => $user_temporary->getName() . ", ", 'message' => " URL de confirmação de cadastro: <strong>$url</strong>"]);

            return $this->redirectToRoute('admin_users_temporary');
        } 
                          
        $this->addFlash('success', ['type' => 'danger', 'title' => $user_temporary->getName() . ", ", 'message' => " o usuário ainda não finalizou o cadastro"]);

        return $this->redirectToRoute('admin_users_temporary');
    }
}
