<?php

namespace App\Controller\Admin;

use App\Entity\Notice;
use App\Form\NoticeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin/noticias")
 */
class NoticeController extends AbstractController
{
    /**
     * @Route("/", name="admin_notice")
     */
    public function index()
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'noticia', 'view');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $notices = $em->getRepository('App:Notice')->findBy([], ['created_at' => 'DESC']);

        return $this->render('admin/notice/index.html.twig', [
            'notices' => $notices,
            'user_acess' => $user_acess['user_acess']
        ]);
    }


    /**
     * @Route("/novo", name="admin_notice_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'noticia', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */


        $notice = new Notice();
        $form = $this->createForm(NoticeType::class, $notice);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $notice = $form->getData();

            /** slug amigável */
            if (trim($notice->getSlug())) {
                $notice->setSlug($this->slugify($notice->getSlug()));
            } else {
                $notice->setSlug($this->slugify($notice->getTitle()));
            }
            /** end */

            $imageFile = $form->get('image')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'notice');
                $notice->setImage($fileName);
            }


            $notice->setYear($notice->getCreatedAt()->format('Y'));
            $notice->setCreatedAt(new \DateTime());
            $notice->setUpdatedAt(new \DateTime());

            $em->persist($notice);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Notícia!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_notice');
        }

        return $this->render('admin/notice/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="admin_notice_edit")
     * @ParamConverter("id", class="App\Entity\Notice", options={"id": "id"})
     */
    public function edit(Notice $notice, Request $request, FileUploader $fileUploader)
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'noticia', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $imageOld = $notice->getImage();

        $form = $this->createForm(NoticeType::class, $notice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $notice = $form->getData();

            /** slug amigável */
            if (trim($notice->getSlug())) {
                $notice->setSlug($this->slugify($notice->getSlug()));
            } else {
                $notice->setSlug($this->slugify($notice->getTitle()));
            }
            /** end */

            $imageFile = $form->get('image')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'notice');
                $notice->setImage($fileName);
            } else {
                $notice->setImage($imageOld);
            }

            $notice->setUpdatedAt(new \DateTime());
            $em->persist($notice);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Notícia!', 'message' => 'atualizado com sucesso.']);

            return $this->redirectToRoute('admin_notice', ['id' => $notice->getId()]);
        }

        return $this->render('admin/notice/edit.html.twig', [
            'form' => $form->createView(),
            'notice' => $notice
        ]);
    }

    /**
     * @Route("/{id}/delete", name="admin_notice_delete")
     * @ParamConverter("id", class="App\Entity\Notice", options={"id": "id"})
     */
    public function delete(Request $request, Notice $notice)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'noticia', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */

        $form = $this->createDeleteForm($notice);
        $form->handleRequest($request);

        if ($notice) {
            $em->remove($notice);
            $em->flush();
        }

        $this->addFlash('success', ['type' => 'success', 'title' => 'Notícia!', 'message' => 'deletado com sucesso.']);

        return $this->redirectToRoute('admin_notice');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param Notice $notice The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Notice $notice)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'noticia', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */


        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_notice_delete', array('id' => $notice->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function slugify($string)
    {
        $string = preg_replace('/[\t\n]/', ' ', $string);
        $string = preg_replace('/\s{2,}/', ' ', $string);
        $list = array(
            'Š' => 'S',
            'š' => 's',
            'Đ' => 'Dj',
            'đ' => 'dj',
            'Ž' => 'Z',
            'ž' => 'z',
            'Č' => 'C',
            'č' => 'c',
            'Ć' => 'C',
            'ć' => 'c',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'A',
            'Ç' => 'C',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ñ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            'Ø' => 'O',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ý' => 'Y',
            'Þ' => 'B',
            'ß' => 'Ss',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            'æ' => 'a',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ð' => 'o',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ý' => 'y',
            'ý' => 'y',
            'þ' => 'b',
            'ÿ' => 'y',
            'Ŕ' => 'R',
            'ŕ' => 'r',
            '/' => '-',
            ' ' => '-',
            '.' => '-',
        );

        $string = strtr($string, $list);
        $string = preg_replace('/-{2,}/', '-', $string);
        $string = strtolower($string);

        return $string;
    }
}