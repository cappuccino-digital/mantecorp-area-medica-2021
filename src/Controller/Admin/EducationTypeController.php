<?php

namespace App\Controller\Admin;

use App\Entity\Education;
use App\Form\EducationType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("admin/education-type")
 */
class EducationTypeController extends AbstractController
{
    /**
     * @Route("/", name="admin_education_type")
     */
    public function index()
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'education-type', 'view');


        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $types = $em->getRepository('App:Education')->findAll();

        return $this->render('admin/educationType/index.html.twig', [
            'types' => $types,
            'user_acess' => $user_acess['user_acess']
        ]);
    }



    /**
     * @Route("/novo", name="admin_education_type_new")
     */
    public function new(Request $request)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'education-type', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $education_type = new Education();
        $form = $this->createForm(EducationType::class, $education_type);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $news_categories = $form->getData();

            $em->persist($news_categories);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Tipo de Curso!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_education_type');
        }

        return $this->render('admin/educationType/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/editar/{id}", name="admin_education_type_edit")
     * @ParamConverter("id", class="App\Entity\Education", options={"id": "id"})
     */
    public function edit(Request $request, Education $education_type)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'education-type', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $deleteForm = $this->createDeleteForm($education_type);
        $form = $this->createForm(EducationType::class, $education_type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($education_type);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Tipo de curso!', 'message' => 'atualizado com sucesso.']);
            return $this->redirectToRoute('admin_education_type', ['id' => $education_type->getId()]);
        }

        return $this->render('admin/educationType/edit.html.twig', [
            'news_categories' => $education_type,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}/deletar", name="admin_education_type_delete")
     * @ParamConverter("id", class="App\Entity\Education", options={"id": "id"})
     */
    public function deleteAction(Request $request, Education $education_type)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'education-type', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */


        $form = $this->createDeleteForm($education_type);
        $form->handleRequest($request);

        if ($education_type) {
            $em->remove($education_type);
            $em->flush();
        }

        $this->addFlash('success', ['type' => 'success', 'title' => 'Cursos!', 'message' => 'deletado com sucesso.']);


        return $this->redirectToRoute('admin_education_type');
    }

    /**
     * Creates a form to delete a news_categories entity.
     *
     * @param NewsCategories $news_categories The news_categories entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Education $education_type)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'education-type', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_education_type_delete', array('id' => $education_type->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}