<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserEventsController extends AbstractController
{
    /**
     * @Route("/admin/user/events", name="admin_user_events")
     */
    public function index(): Response
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'user_eventos', 'view');


        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $user_events = $em->getRepository('App:UserEvents')->findAll();

        return $this->render('admin/user_events/index.html.twig', [
            'user_events' => $user_events,
        ]);
    }
}
