<?php

namespace App\Controller\Admin;

use App\Entity\Products;
use App\Form\ProductsType;
use App\Entity\Recipe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin/produtos")
 */
class ProductsController extends AbstractController
{
    /**
     * @Route("/", name="admin_products")
     */
    public function index(Request $request)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'produtos', 'view');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $products = $em->getRepository('App:Products')->findBy([], ['created_at' => 'DESC']);

        return $this->render('admin/products/index.html.twig', [
            'products' => $products,
            'user_acess' => $user_acess['user_acess']
        ]);
    }

    /**
     * @Route("/novo", name="admin_products_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {
        ini_set('upload_max_filesize', '200M');
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'produtos', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */


        $products = new Products();
        $products->setCreatedAt(new \DateTime());
        $products->setUpdatedAt(new \DateTime());


        $form = $this->createForm(ProductsType::class, $products);
        $form->handleRequest($request);

        //dump($request); die();
        if ($form->isSubmitted() && $form->isValid()) {
            $products = $form->getData();
            $title_to_slug = $this->tratamentoString($products->getName());
            $title_to_slug = str_replace(" ", "-", strtolower($title_to_slug));

            $products->setSlug($title_to_slug);

            $imageFile = $form->get('image')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'products');
                $products->setImage($fileName);
            }

            $em->persist($products);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Produtos!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_products');
        }

        return $this->render('admin/products/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/editar/{id}", name="admin_products_edit")
     * @ParamConverter("id", class="App\Entity\Products", options={"id": "id"})
     */
    public function edit(Products $products, Request $request, FileUploader $fileUploader)
    {
        ini_set('upload_max_filesize', '200M');
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'produtos', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $imageOld = $products->getImage();
        
        $form = $this->createForm(ProductsType::class, $products);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $products = $form->getData();
            $title_to_slug = $this->tratamentoString($products->getName());
            $title_to_slug = str_replace(" ", "-", strtolower($title_to_slug));
            $products->setSlug($title_to_slug);

            $imageFile = $form->get('image')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'products');
                $products->setImage($fileName);
            } else {
                $products->setImage($imageOld);
            }

            $products->setUpdatedAt(new \DateTime());
            $em->persist($products);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Produtos!', 'message' => 'atualizado com sucesso.']);

            return $this->redirectToRoute('admin_products');
        }
        return $this->render('admin/products/edit.html.twig', [
            'form' => $form->createView(),
            'products' => $products
        ]);
    }

    /**
     * @Route("/{id}/deletar", name="admin_products_delete")
     * @ParamConverter("id", class="App\Entity\Products", options={"id": "id"})
     */
    public function delete(Request $request, Products $products)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'produtos', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */

        //Validação de produtos relacionados

        //delete file
        /* Validação se existe um usuário médico para a especialidade a ser excluida especialidade */
        $scientific = $em->getRepository('App:ScientificStudies')->findByProductId($products->getId());
        if (count($scientific) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Produto!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com Estudos Científicos.']);
            return $this->redirectToRoute('admin_products');
        }

        $flyer = $em->getRepository('App:Flyer')->findByProductId($products->getId());
        if (count($flyer) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Produto!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com Folhetos.']);
            return $this->redirectToRoute('admin_products');
        }


        $yourpatient = $em->getRepository('App:YourPatient')->findByProductId($products->getId());
        if (count($yourpatient) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Produto!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com Seu Paciente.']);
            return $this->redirectToRoute('admin_products');
        }


        $notice = $em->getRepository('App:Notice')->findByProductId($products->getId());
        if (count($notice) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Produto!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com Notícia.']);
            return $this->redirectToRoute('admin_products');
        }


        $mementos = $em->getRepository('App:Mementos')->findByProductId($products->getId());
        if (count($mementos) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Produto!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com Residentes.']);
            return $this->redirectToRoute('admin_products');
        }
        
        $form = $this->createDeleteForm($products);
        $form->handleRequest($request);

        if ($products) {
            $em->remove($products);
            $em->flush();
        }

        $this->addFlash('success', ['type' => 'success', 'title' => 'Produtos!', 'message' => 'deletado com sucesso.']);

        return $this->redirectToRoute('admin_products');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param Products $products The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Products $products)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'produtos', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $em->getRepository('App:Notice')->updateProductId($products->getId());

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_products_delete', array('id' => $products->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function tratamentoString($string)
    {
        $what = array(
            'Š' => 'S', 'š' => 's', 'Ð' => 'Dj', '' => 'Z', '' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A',
            'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I',
            'Ï' => 'I', 'Ñ' => 'N', 'Ń' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U',
            'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a',
            'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i',
            'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ń' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u',
            'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'ƒ' => 'f',
            'ă' => 'a', 'î' => 'i', 'â' => 'a', 'ș' => 's', 'ț' => 't', 'Ă' => 'A', 'Î' => 'I', 'Â' => 'A', 'Ș' => 'S', 'Ț' => 'T',
        );

        $res = strtr($string, $what);
        return $res;
    }
}