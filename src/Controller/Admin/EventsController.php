<?php

namespace App\Controller\Admin;

use App\Entity\Events;
use App\Form\EventsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;

/**
 * @Route("/admin/eventos")
 */
class EventsController extends AbstractController
{
    /**
     * @Route("/", name="admin_event")
     */
    public function index()
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'eventos', 'view');


        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $events = $em->getRepository('App:Events')->findAll();

        return $this->render('admin/event/index.html.twig', [
            'events' => $events,
            'user_acess' => $user_acess['user_acess']
        ]);
    }



    /**
     * @Route("/novo", name="admin_events_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'eventos', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */
        
        $new_event = new Events();
        $form = $this->createForm(EventsType::class, $new_event);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //dump($request); die();
            $new_event = $form->getData();

            $imageFile = $form->get('imageweb')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'events');
                $new_event->setImageweb($fileName);
            } 

            /** slug amigável */
            $new_event->setSlug($this->slugify($new_event->getName()));
            /** end */

            $new_event->setDatetimestart(new \DateTimeImmutable());
            $new_event->setCreatedAt(new \DateTimeImmutable());
            $new_event->setUpdateAt(new \DateTimeImmutable());
           
            $em->persist($new_event);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Eventos!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_event');
        }

        return $this->render('admin/event/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/editar/{id}", name="admin_event_edit")
     * @ParamConverter("id", class="App\Entity\Events", options={"id": "id"})
     */
    public function edit(Request $request, Events $events_type, FileUploader $fileUploader)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'eventos', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $imagewebOld = $events_type->getImageweb();

        $deleteForm = $this->createDeleteForm($events_type);
        $form = $this->createForm(EventsType::class, $events_type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imageFile = $form->get('imageweb')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'events');
                $events_type->setImageweb($fileName);
            } else {
                $events_type->setImageweb($imagewebOld);
            }

            /** slug amigável */
            $events_type->setSlug($this->slugify($events_type->getName()));
            /** end */

            $events_type->setUpdateAt(new \DateTimeImmutable());
            $em = $this->getDoctrine()->getManager();
            $em->persist($events_type);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Eventos!', 'message' => 'atualizado com sucesso.']);
            return $this->redirectToRoute('admin_event_edit', ['id' => $events_type->getId()]);
        }

        return $this->render('admin/event/edit.html.twig', [
            'news_categories' => $events_type,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}/deletar", name="admin_event_delete")
     * @ParamConverter("id", class="App\Entity\Events", options={"id": "id"})
     */
    public function deleteAction(Request $request, Events $events)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'eventos', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */

        if (count($events->getUserEvents()) > 0) {
            foreach ($events->getUserEvents() as $user_events) {
                $events->removeUserEvent($user_events);

                $em->persist($events);
                $em->flush();
            }
        }
        
        if (count($events->getSpeakers()) > 0) {
            foreach ($events->getSpeakers() as $speakers) {
                $events->removeSpeaker($speakers);

                $em->persist($events);
                $em->flush();
            }
        }

        if (count($events->getSpecialty()) > 0) {
            foreach ($events->getSpecialty() as $specialty) {
                $events->removeSpecialty($specialty);

                $em->persist($events);
                $em->flush();
            }
        }

        $em->remove($events);
        $em->flush();

        $this->addFlash('danger', ['type' => 'danger', 'title' => 'Eventos!', 'message' => 'excluído com sucesso.']);
    
        return $this->redirectToRoute('admin_event');
    }

    /**
     * Creates a form to delete a news_categories entity.
     *
     * @param NewsCategories $news_categories The news_categories entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Events $events_type)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'eventos', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_event_delete', array('id' => $events_type->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function slugify($string)
    {
        $string = preg_replace('/[\t\n]/', ' ', $string);
        $string = preg_replace('/\s{2,}/', ' ', $string);
        $list = array(
            'Š' => 'S',
            'š' => 's',
            'Đ' => 'Dj',
            'đ' => 'dj',
            'Ž' => 'Z',
            'ž' => 'z',
            'Č' => 'C',
            'č' => 'c',
            'Ć' => 'C',
            'ć' => 'c',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'A',
            'Ç' => 'C',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ñ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            'Ø' => 'O',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ý' => 'Y',
            'Þ' => 'B',
            'ß' => 'Ss',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            'æ' => 'a',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ð' => 'o',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ý' => 'y',
            'ý' => 'y',
            'þ' => 'b',
            'ÿ' => 'y',
            'Ŕ' => 'R',
            'ŕ' => 'r',
            '/' => '-',
            ' ' => '-',
            '.' => '-',
        );

        $string = strtr($string, $list);
        $string = preg_replace('/-{2,}/', '-', $string);
        $string = strtolower($string);

        return $string;
    }
}
