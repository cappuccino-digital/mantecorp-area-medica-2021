<?php

namespace App\Controller\Admin;

use App\Entity\ScientificStudies;
use App\Form\ScientificStudiesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("admin")
 */
class StudyController extends AbstractController
{
    /**
     * @Route("/estudos_cientificos", name="admin_estudos_cientificos")
     */
    public function index()
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'estudos-cientificos', 'view');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */


        $studies = $em->getRepository('App:ScientificStudies')->findAll();

        return $this->render('admin/scientific_studies/index.html.twig', [
            "user_acess" => $user_acess['user_acess'],
            "studies" => $studies
        ]);
    }

    /**
     * @Route("/estudos_cientificos/novo", name="admin_estudos_cientificos_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'estudos-cientificos', 'create');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $news_study = new ScientificStudies();
        $form = $this->createForm(ScientificStudiesType::class, $news_study);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $news_study = $form->getData();

            $file = $form->get('file')->getData();
            if ($file) {
                $fileName = $fileUploader->upload($file, 'scientific-studies');
                $news_study->setFile($fileName);
            }


            $imageFile = $form->get('imageweb')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'notice');
                $news_study->setImageweb($fileName);
            }
            
            $news_study->setCreatedAt(new \DateTime());
            $news_study->setUpdatedAt(new \DateTime());
            $em->persist($news_study);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Especialidade!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_estudos_cientificos');
        }
        return $this->render('admin/scientific_studies/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/estudos_cientificos/editar/{id}", name="admin_study_edit")
     * @ParamConverter("id", class="App\Entity\ScientificStudies", options={"id": "id"})
     */
    public function edit(Request $request, ScientificStudies $edit_study, FileUploader $fileUploader)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'estudos-cientificos', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $deleteForm = $this->createDeleteForm($edit_study);
        $form = $this->createForm(ScientificStudiesType::class, $edit_study);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $news_specialty = $form->getData();

            $news_study = $form->getData();
            $file = $form->get('file')->getData();
            if ($file) {
                $fileName = $fileUploader->upload($file, 'scientific-studies');
                $news_study->setFile($fileName);
            }


            $imageFile = $form->get('imageweb')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'notice');
                $news_study->setImageweb($fileName);
            }

            $news_specialty->setUpdatedAt(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($news_specialty);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Especialidades!', 'message' => 'atualizado com sucesso.']);
            return $this->redirectToRoute('admin_estudos_cientificos');
        }


        return $this->render('admin/scientific_studies/edit.html.twig', [
            "user_acess" => $user_acess['user_acess'],
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * @Route("/estudos_cientificos/{id}/deletar", name="admin_estudos_cientificos_delete")
     * @ParamConverter("id", class="App\Entity\ScientificStudies", options={"id": "id"})
     */
    public function deleteAction(Request $request, ScientificStudies $studies_type)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'estudos-cientificos', 'delete');
        
        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */

        if ($studies_type) {
            $em->remove($studies_type);
            $em->flush();

            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Estudos!', 'message' => 'excluído com sucesso.']);
        }

        return $this->redirectToRoute('admin_estudos_cientificos');
    }


    private function createDeleteForm(ScientificStudies $new_study)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'tipo-de-noticia', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_estudos_cientificos_delete', array('id' => $new_study->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}