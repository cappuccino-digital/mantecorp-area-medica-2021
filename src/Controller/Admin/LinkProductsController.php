<?php

namespace App\Controller\Admin;

use App\Entity\Link;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Recipe;
use App\Form\LinkType;
use Symfony\Component\HttpFoundation\Request;

class LinkProductsController extends AbstractController
{
    /**
     * @Route("/admin/produtos/links", name="admin_products_links")
     */
    public function index()
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'link-de-produtos', 'view');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $links = $em->getRepository('App:Link')->findAllLinks([], ['created_at' => 'DESC']);

        return $this->render('admin/link_products/index.html.twig', [
            'links' => $links,
            'user_acess' => $user_acess['user_acess']
        ]);
    }

    /**
     * @Route("/admin/produtos/links/new", name="admin_products_links_new")
     */
    public function new(Request $request)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'link-de-produtos', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $link = new Link();
        $link->setCreatedAt(new \DateTime());
        $link->setUpdatedAt(new \DateTime());


        $form = $this->createForm(LinkType::class, $link);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $link = $form->getData();


            $em->persist($link);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Link!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_products_links');
        }

        return $this->render('admin/link_products/new.html.twig', [
            'user_acess' => $user_acess['user_acess'],
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/admin/produtos/links/{id}", name="admin_products_links_edit")
     */
    public function edit(Link $link, Request $request)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'link-de-produtos', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */



        $form = $this->createForm(LinkType::class, $link);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $link = $form->getData();
            $title_to_slug = str_replace(" ", "-", $link->getName());
            $link->setSlug($title_to_slug);
            $link->setUpdatedAt(new \DateTime());


            $em->persist($link);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Link!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_products_links');
        }

        return $this->render('admin/link_products/new.html.twig', [
            'user_acess' => $user_acess['user_acess'],
            'form' => $form->createView()
        ]);
    }



    /**
     * @Route("link/{id}/deletar", name="admin_products_links_delete")
     */
    public function delete(Request $request, Link $link)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'link-de-produtos', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */

        $form = $this->createDeleteForm($link);
        $form->handleRequest($request);

        if ($link) {
            $em->remove($link);
            $em->flush();
        }

        $this->addFlash('success', ['type' => 'success', 'title' => 'Link!', 'message' => 'deletado com sucesso.']);

        return $this->redirectToRoute('admin_products_links');
    }



    /**
     * Creates a form to delete a user entity.
     *
     * @param Products $products The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Link $link)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'link-de-produtos', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_products_delete', array('id' => $link->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}