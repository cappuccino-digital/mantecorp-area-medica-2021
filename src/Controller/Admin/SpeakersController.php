<?php

namespace App\Controller\Admin;

use App\Entity\Speakers;
use App\Form\SpeakersType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;

/**
 * @Route("/admin/speakers")
 */
class SpeakersController extends AbstractController
{


    /**
     * @Route("/", name="admin_speakers")
     */
    public function index()
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'eventos-palestrantes', 'view');


        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $speakers = $em->getRepository('App:Speakers')->findAll();

        return $this->render('admin/speakers/index.html.twig', [
            'speakers' => $speakers,
            'user_acess' => $user_acess['user_acess']
        ]);
    }



    /**
     * @Route("/novo", name="admin_speakers_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'eventos-palestrantes', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $new_speaker = new Speakers();
        $form = $this->createForm(SpeakersType::class, $new_speaker);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $new_speaker = $form->getData();

            $new_speaker->setCreatedAt(new \DateTimeImmutable());
            $new_speaker->setUpdateAt(new \DateTimeImmutable());

            $imageFile = $form->get('photo')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'speakers');
                $new_speaker->setPhoto($fileName);
            }

            $em->persist($new_speaker);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Palestrantes!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_speakers');
        }

        return $this->render('admin/event/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/editar/{id}", name="admin_speakers_edit")
     * @ParamConverter("id", class="App\Entity\Speakers", options={"id": "id"})
     */
    public function edit(Request $request, Speakers $events_type, FileUploader $fileUploader)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'eventos-palestrantes', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $photoOld = $events_type->getPhoto();

        $deleteForm = $this->createDeleteForm($events_type);
        $form = $this->createForm(SpeakersType::class, $events_type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imageFile = $form->get('photo')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'speakers');
                $events_type->setPhoto($fileName);
            } else {
                $events_type->setPhoto($photoOld);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($events_type);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Eventos!', 'message' => 'atualizado com sucesso.']);
            return $this->redirectToRoute('admin_speakers_edit', ['id' => $events_type->getId()]);
        }

        return $this->render('admin/event/edit.html.twig', [
            'news_categories' => $events_type,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}/deletar", name="admin_speakers_delete")
     * @ParamConverter("id", class="App\Entity\Speakers", options={"id": "id"})
     */
    public function deleteAction(Request $request, SpeakersType $events_type)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'eventos-palestrantes', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */
        $news = $em->getRepository('App:Events')->findOneBy(['type' => $events_type]);

        if ($news) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Eventos!', 'message' => 'não foi possível excluir, esse tipo de notícia está relacionado com outras notícias.']);
            return $this->redirectToRoute('admin_speakers');
        }

        if ($events_type) {
            $em->remove($events_type);
            $em->flush();

            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Eventos!', 'message' => 'excluído com sucesso.']);
        }

        return $this->redirectToRoute('admin_speakers');
    }

    /**
     * Creates a form to delete a news_categories entity.
     *
     * @param NewsCategories $news_categories The news_categories entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Speakers $events_type)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'eventos-palestrantes', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_speakers_delete', array('id' => $events_type->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
