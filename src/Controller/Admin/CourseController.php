<?php

namespace App\Controller\Admin;

use App\Entity\Course;
use App\Entity\Recipe;
use App\Form\CourseType;
use App\Form\ProductsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;

/**
 * @Route("/admin/curso")
 */
class CourseController extends AbstractController
{


    /**
     * @Route("", name="admin_course")
     */
    public function index()
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'cursos', 'view');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $courses = $em->getRepository('App:Course')->findAll();
        /* dump($courses);
        die(); */
        return $this->render('admin/course/index.html.twig', [
            'courses' => $courses,
            'user_acess' => $user_acess['user_acess']
        ]);
    }


    /**
     * @Route("/novo", name="admin_course_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'cursos', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $course = new Course();
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $course = $form->getData();


            $imageFile = $form->get('imageweb')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'notice');
                $course->setImageweb($fileName);
            }

            $em->persist($course);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Curso!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_course');
        }

        return $this->render('admin/course/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/editar/{id}", name="admin_course_edit")
     * @ParamConverter("id", class="App\Entity\Course", options={"id": "id"})
     */
    public function edit(Course $course, Request $request, FileUploader $fileUploader)
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'cursos', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $imageOld = $course->getImageweb();
        $imageMobileOld = $course->getImagemobile();



        $form = $this->createForm(CourseType::class, $course);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $course = $form->getData();


            $imageFile = $form->get('imageweb')->getData();
            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'course');
                $course->setImageweb($fileName);
            } else {


                $course->setImageweb($imageOld);
            }
            $em->persist($course);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Curso!', 'message' => 'atualizado com sucesso.']);

            return $this->redirectToRoute('admin_course');
        }
        return $this->render('admin/course/edit.html.twig', [
            'form' => $form->createView(),
            'courses' => $course
        ]);
    }

    /**
     * @Route("/{id}/deletar", name="admin_course_delete")
     * @ParamConverter("id", class="App\Entity\Course", options={"id": "id"})
     */
    public function delete(Request $request, Course $course)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'cursos', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */


        $form = $this->createDeleteForm($course);
        $form->handleRequest($request);

        if ($course) {
            $em->remove($course);
            $em->flush();
        }

        $this->addFlash('success', ['type' => 'success', 'title' => 'Cursos!', 'message' => 'deletado com sucesso.']);

        return $this->redirectToRoute('admin_course');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param Course $course The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Course $course)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'cursos', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_course_delete', array('id' => $course->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}