<?php

namespace App\Controller\Admin;

use App\Entity\Mementos;
use App\Form\MementosType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Recipe;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;

/**
 * @Route("/admin/mementos")
 */
class MementosController extends AbstractController
{


    /**
     * @Route("/", name="admin_mementos")
     */
    public function index()
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'mementos-residentes', 'view');


        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $mementos = $em->getRepository('App:Mementos')->findAll();

        return $this->render('admin/mementos/index.html.twig', [
            'mementos' => $mementos,
            'user_acess' => $user_acess['user_acess']
        ]);
    }



    /**
     * @Route("/novo", name="admin_mementos_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'mementos-residentes', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $mementos = new Mementos();

        $form = $this->createForm(MementosType::class, $mementos);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mementos = $form->getData();


            $file = $form->get('file')->getData();

            if ($file) {
                $fileName = $fileUploader->upload($file, 'mementos');
                $mementos->setFile($fileName);
            }

            $em->persist($mementos);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Mementos!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_mementos');
        }

        return $this->render('admin/mementos/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/editar/{id}", name="admin_mementos_edit")
     * @ParamConverter("id", class="App\Entity\Mementos", options={"id": "id"})
     */
    public function edit(Mementos $mementos, Request $request, FileUploader $fileUploader)
    {

        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'mementos-residentes', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $fileOld = $mementos->getFile();
        $form = $this->createForm(MementosType::class, $mementos);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $mementos = $form->getData();
            $file = $form->get('file')->getData();

            if ($file) {
                $fileName = $fileUploader->upload($file, 'mementos');
                $mementos->setFile($fileName);
            } else {
                $mementos->setFile($fileOld);
            }

            $em->persist($mementos);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Memento!', 'message' => 'atualizado com sucesso.']);

            return $this->redirectToRoute('admin_mementos');
        }
        return $this->render('admin/mementos/edit.html.twig', [
            'form' => $form->createView(),
            'mementos' => $mementos
        ]);
    }

    /**
     * @Route("/{id}/deletar", name="admin_mementos_delete")
     * @ParamConverter("id", class="App\Entity\Mementos", options={"id": "id"})
     */
    public function delete(Request $request, Mementos $mementos)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'mementos-residentes', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */



        $form = $this->createDeleteForm($mementos);
        $form->handleRequest($request);

        if ($mementos) {
            $em->remove($mementos);
            $em->flush();
        }

        $this->addFlash('success', ['type' => 'success', 'title' => 'Memento!', 'message' => 'deletado com sucesso.']);

        return $this->redirectToRoute('admin_mementos');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param Mementos $mementos The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Mementos $mementos)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'mementos-residentes', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_mementos_delete', array('id' => $mementos->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}