<?php

namespace App\Controller\Admin;

use App\Entity\MedicalUser;
use App\Form\MedicalUserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/medical-user")
 */
class MedicalUserController extends AbstractController
{
    /**
     * @Route("/", name="admin_medical_user")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('App:MedicalUser')->findAll();
        return $this->render('admin/medical_user/index.html.twig', [
            "users" => $users
        ]);
    }


    /**
     * @Route("/new", name="admin_medical_user_new")
     */
    public function new(Request $request)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'usuarios-medicos', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $medical_users = new MedicalUser();


        $form = $this->createForm(MedicalUserType::class, $medical_users);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $medical_users = $form->getData();

            $em->persist($medical_users);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Usuario!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_medical_user');
        }

        return $this->render('admin/medical_user/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/edit", name="admin_medical_user_edit")
     */
    public function edit()
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('admin/medical_user/index.html.twig', []);
    }


    /**
     * @Route("/delete", name="admin_medical_user_delete")
     */
    public function delete()
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('admin/medical_user/index.html.twig', []);
    }
}