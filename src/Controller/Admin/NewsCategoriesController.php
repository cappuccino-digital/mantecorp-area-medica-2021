<?php

namespace App\Controller\Admin;

use App\Entity\NewsCategories;
use App\Form\NewsCategoriesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin/noticias/categorias")
 */
class NewsCategoriesController extends AbstractController
{
    /**
     * @Route("/", name="admin_news_categories")
     */
    public function index()
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'categoria-de-noticias', 'view');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $news_categories = $em->getRepository('App:NewsCategories')->findAll();

        return $this->render('admin/news_categories/index.html.twig', [
            'news_categories' => $news_categories,
            'user_acess' => $user_acess['user_acess']
        ]);
    }

    /**
     * @Route("/novo", name="admin_news_categories_new")
     */
    public function new(Request $request)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'categoria-de-noticias', 'new');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $news_categories = new NewsCategories();
        $form = $this->createForm(NewsCategoriesType::class, $news_categories);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $news_categories = $form->getData();

            $news_categories->setCreatedAt(new \DateTime());
            $news_categories->setUpdatedAt(new \DateTime());
            $em->persist($news_categories);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Categoria de Notícias!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_news_categories');
        }

        return $this->render('admin/news_categories/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/editar/{id}", name="admin_news_categories_edit")
     * @ParamConverter("id", class="App\Entity\NewsCategories", options={"id": "id"})
     */
    public function edit(Request $request, NewsCategories $news_categories)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'categoria-de-noticias', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $deleteForm = $this->createDeleteForm($news_categories);
        $form = $this->createForm(NewsCategoriesType::class, $news_categories);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $news_categories->setUpdatedAt(new \DateTime());
            $em->persist($news_categories);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Categoria de Notícias!', 'message' => 'atualizado com sucesso.']);
            return $this->redirectToRoute('admin_news_categories_edit', ['id' => $news_categories->getId()]);
        }

        return $this->render('admin/news_categories/edit.html.twig', [
            'news_categories' => $news_categories,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}/deletar", name="admin_news_categories_delete")
     * @ParamConverter("id", class="App\Entity\NewsCategories", options={"id": "id"})
     */
    public function deleteAction(Request $request, NewsCategories $news_categories)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'categoria-de-noticias', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */
        $news = $em->getRepository('App:Notice')->findOneBy(['category' => $news_categories]);

        if ($news) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Categoria de Notícias!', 'message' => 'não foi possível excluir, essa categoria está relacionado com outras notícias.']);
            return $this->redirectToRoute('admin_news_categories');
        }

        if ($news_categories) {
            $em->remove($news_categories);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Categoria de Notícias!', 'message' => 'excluído com sucesso.']);
        }

        return $this->redirectToRoute('admin_news_categories');
    }

    /**
     * Creates a form to delete a news_categories entity.
     *
     * @param NewsCategories $news_categories The news_categories entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(NewsCategories $news_categories)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'categoria-de-noticias', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_news_categories_delete', array('id' => $news_categories->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}