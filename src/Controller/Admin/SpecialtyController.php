<?php

namespace App\Controller\Admin;

use App\Entity\Specialty;
use App\Form\SpecialtyType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin")
 */
class SpecialtyController extends AbstractController
{
    /**
     * @Route("/especialidades", name="admin_especialidades")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'cadastro-de-especialidades', 'view');
        $specialty = $em->getRepository('App:Specialty')->findAll();

        return $this->render('admin/specialty/index.html.twig', [
            'user_acess' => $user_acess['user_acess'],
            'specialtys' => $specialty

        ]);
    }

    /**
     * @Route("/especialidades/novo", name="admin_cadastro_especialidades")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {

        $em = $this->getDoctrine()->getManager();

        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'cadastro-de-especialidades', 'view');


        $news_specialty = new Specialty();
        $form = $this->createForm(SpecialtyType::class, $news_specialty);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $news_specialty = $form->getData();

            $imageFile = $form->get('icon')->getData();

            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'icon');
                $news_specialty->setIcon($fileName);
            }
            //$news_categories->setUpdatedAt(new \DateTime());
            $em->persist($news_specialty);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Especialidade!', 'message' => 'cadastrado com sucesso.']);

            return $this->redirectToRoute('admin_especialidades');
        }

        return $this->render('admin/specialty/new_specialty.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/especialidades/editar/{id}", name="admin_edit_especialidades")
     * @ParamConverter("id", class="App\Entity\Specialty", options={"id": "id"})
     */
    public function edit(Request $request, Specialty $news_specialty, FileUploader $fileUploader)
    {
        $em = $this->getDoctrine()->getManager();

        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'cadastro-de-especialidades', 'edit');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        $deleteForm = $this->createDeleteForm($news_specialty);
        $form = $this->createForm(SpecialtyType::class, $news_specialty);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $news_specialty = $form->getData();

            $imageFile = $form->get('icon')->getData();

            if ($imageFile) {
                $fileName = $fileUploader->upload($imageFile, 'icon');
                $news_specialty->setIcon($fileName);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($news_specialty);
            $em->flush();

            $this->addFlash('success', ['type' => 'success', 'title' => 'Especialidades!', 'message' => 'atualizado com sucesso.']);
            return $this->redirectToRoute('admin_especialidades');
        }

        return $this->render('admin/specialty/edit_specialty.html.twig', [
            'news_categories' => $news_specialty,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }



    /**
     * @Route("/{id}/deletar", name="admin_delete_especialidades")
     * @ParamConverter("id", class="App\Entity\Specialty", options={"id": "id"})
     */
    public function deleteAction(Request $request, Specialty $specialty_type)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'cadastro-de-especialidades', 'delete');

        if (!$user_acess['status']) {
            throw $this->createNotFoundException($user_acess['message']);
        }
        /* end validação */
        /* Validação se existe um usuário médico para a especialidade a ser excluida especialidade */
        $usuarios_medicos = $em->getRepository('App:MedicalUser')->findBySpecialtyId($specialty_type->getId());
        if (count($usuarios_medicos) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Especialidade!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com Usuários Médicos.']);
            return $this->redirectToRoute('admin_especialidades');
        }
        /* Validação se existe arquivos para pacientes para a especialidade a ser excluida especialidade */
        $your_patitent = $em->getRepository('App:YourPatient')->findBySpecialtyId($specialty_type->getId());

        if (count($your_patitent) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Especialidade!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com Você e Seu Paciente.']);
            return $this->redirectToRoute('admin_especialidades');
        }


        /* Validação se existe produtos a especialidade a ser excluida especialidade */
        $products = $em->getRepository('App:Products')->findBySpecialtyId($specialty_type->getId());

        if (count($products) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Especialidade!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com Produtos.']);
            return $this->redirectToRoute('admin_especialidades');
        }
        /* Validação se existe usuario temporario para especialidade a ser excluida */
        $userTemporary = $em->getRepository('App:UserTemporary')->findBySpecialtyId($specialty_type->getId());

        if (count($userTemporary) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Especialidade!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com  Médicos.']);
            return $this->redirectToRoute('admin_especialidades');
        }


        /* Validação se existe curso para especialidade a ser excluida */
        $course = $em->getRepository('App:Course')->findBySpecialtyId($specialty_type->getId());
        if (count($course) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Especialidade!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com  Educação Médica.']);
            return $this->redirectToRoute('admin_especialidades');
        }

        $notice = $em->getRepository('App:Notice')->findBySpecialtyId($specialty_type->getId());
        if (count($notice) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Especialidade!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com  Notícias.']);
            return $this->redirectToRoute('admin_especialidades');
        }


        $studies = $em->getRepository('App:ScientificStudies')->findByIdSpecialty($specialty_type->getId());
        if (count($studies) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Especialidade!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com  Estudos Científicos.']);
            return $this->redirectToRoute('admin_especialidades');
        }

        /*      $mementos = $em->getRepository('App:Mementos')->findByIdSpecialty($specialty_type->getId());
        if (count($mementos) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Especialidade!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com  Residentes.']);
            return $this->redirectToRoute('admin_especialidades');
        }

        $events = $em->getRepository('App:Events')->findByIdSpecialty($specialty_type->getId());
        if (count($events) > 0) {
            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Especialidade!', 'message' => 'não foi possível excluir, essa especialidade está relacionado com  Eventos.']);
            return $this->redirectToRoute('admin_especialidades');
        } */

        if ($specialty_type) {
            $em->remove($specialty_type);
            $em->flush();

            $this->addFlash('danger', ['type' => 'danger', 'title' => 'Especialidades!', 'message' => 'excluído com sucesso.']);
        }

        return $this->redirectToRoute('admin_especialidades');
    }



    private function createDeleteForm(Specialty $news_specialty)
    {
        /*** validação de permissão de usuário  */
        $em = $this->getDoctrine()->getManager();
        $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'cadastro-de-especialidades', 'delete');

        if (!$user_acess['status'])
            throw $this->createNotFoundException($user_acess['message']);
        /* end validação */

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_news_type_delete', array('id' => $news_specialty->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}