<?php

namespace App\Controller\Admin;

use App\Entity\UserAcess;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin")
 */
class UserAcessController extends AbstractController
{
    /**
     * @Route("/usuario/permissoes/{user_id}", name="admin_user_acess")
     * @IsGranted("ROLE_ADMIN")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function index(Request $request, $user_id)
    {  
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->findOneById($user_id);
       
        if (!$user) {
            $this->addFlash('danger', ['type'=>'danger', 'title'=>'Usuário!', 'message'=>'não encontrado.']);
            return $this->redirectToRoute('admin_users'); 
        }
       
        $menus_temp = $em->getRepository('App:Menu')->findBy(['is_active' => true, 'is_caterer' => false], ['menu_category'=>'ASC']);

        $menus = []; 
        foreach ($menus_temp as $menu) {
            $user_acess = $em->getRepository('App:UserAcess')->findOneBy(['users' => $user_id, 'menus' => $menu->getId()]);
            $menu_category = $menu->getMenuCategory() ? $menu->getMenuCategory()->getName() : null;
          
            if ($user_acess) {
                $menus[] = [
                    'id' => $menu->getId(),
                    'name' => $menu->getName(),
                    'category' => $menu_category,
                    'is_create' => $user_acess->getIsCreate(),
                    'is_update' => $user_acess->getIsUpdate(),
                    'is_delete' => $user_acess->getIsDelete(), 
                    'is_viewed' => $user_acess->getIsViewed()
                ];
            } else {
                $menus[] = [
                    'id' => $menu->getId(),
                    'name' => $menu->getName(),
                    'category' => $menu_category,
                    'is_create' => false,
                    'is_update' => false,
                    'is_delete' => false, 
                    'is_viewed' => false
                ];
            }
        }
     
        return $this->render('admin/user_acess/index.html.twig', [
            'user' => $user,
            'menus' => $menus,
        ]);
    }

    /**
     * @Route("/usuario/permissoes/salvar/{user_id}", name="admin_user_acess_save")
     * @IsGranted("ROLE_ADMIN")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function save(Request $request, $user_id)
    {  
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->findOneById($user_id);

        if ($request->request->has('permission')) { //salvar as permissões para o user
            $form = $request->request->get('permission');
           
            foreach ($form as $permission) {
                $menu = $em->getRepository('App:Menu')->find($permission['menu']);
                $user_menu = $em->getRepository('App:UserAcess')->findOneBy(['users' => $user_id, 'menus' => $permission['menu']]);
               
                $create = isset($permission['create']) == 'on' ? true : false;
                $edit = isset($permission['edit']) == 'on' ? true : false;
                $delete = isset($permission['delete']) == 'on' ? true : false;
                $view = isset($permission['view']) == 'on' ? true : false;

                if (!$user_menu) {
                    $user_menu = new UserAcess();
                }

                if (
                    $create ||
                    $edit ||
                    $delete ||
                    $view
                ) 
                {
                    $user_menu->setUsers($user);
                    $user_menu->setMenus($menu);
                    $user_menu->setIsCreate($create);
                    $user_menu->setIsUpdate($edit);
                    $user_menu->setIsDelete($delete);
                    $user_menu->setIsViewed($view);
                    $user_menu->setCreatedAt(new DateTime());
                    $user_menu->setUpdatedAt(new DateTime());

                    $em->persist($user_menu);
                    $em->flush();
                } else {
                    $em->remove($user_menu);
                    $em->flush();
                }
            }
            
        }

        return $this->redirectToRoute('admin_users');
    }
}
