<?php

namespace App\Controller\Front;

use App\Entity\Notice;
use App\Entity\NewsType;
use Doctrine\ORM\Query;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;

class NoticiasController extends AbstractController
{
    /**
     * @Route("/noticias", name="noticias")
     */
    public function index(Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $notices = $em->getRepository('App:Notice')->findBy([], ['created_at' => 'DESC']);

        $count = count($notices);

        for ($i = 0; $i < $count; $i++) {

            $description = $notices[$i]->getDescription();
            $description = substr_replace($description, '', 500, -1);
            $notices[$i]->setDescription($description);
        }

        if ($request->request->has('s')) {
            $search = $request->request->get('s');
            $notices = [];
        
            $q = $em->getRepository('App:Notice')->createQueryBuilder('n')
                    ->where('n.is_active = true')
                    ->andWhere('n.title like :busca')
                    ->andWhere('n.description like :description')
                    ->setParameter('busca', "%{$search}%")
                    ->setParameter('description', "%{$search}%")
                    ->getQuery()
                    ->getResult();

            foreach ($q as $key => $n) {
                $notices[] = [
                    'id' => $n->getId(),
                    'title' => $n->getTitle(),
                    'image' => $n->getImage(),
                    'slug' => $n->getSlug(),
                    'description' => substr_replace($n->getDescription(), '', 500, -1)
                ];
            }
                    
            return new JsonResponse([
                'status' => true,
                'response' => $notices
            ]);
        }

        $lastOnes = $em->getRepository('App:Notice')->findBy([], ['created_at' => 'DESC'], 3);
        $categories = $em->getRepository('App:NewsType')->findAll();

        return $this->render('front/noticias/index.html.twig', [
            "categories" => $categories,
            "notices" => $notices,
            "lastOnes" => $lastOnes,
            'slug' => null
        ]);
    }

    /**
     * @Route("/noticias/{slug}", name="blog_detail")
     * 
     */
    public function blogDetail(Notice $notices, Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }
       
        $em = $this->getDoctrine()->getManager();
        $lastOnes = $em->getRepository('App:Notice')->findBy([], ['created_at' => 'DESC'], 3);

        $categories = $em->getRepository('App:NewsType')->findAll();

        return $this->render('front/noticias/noticia.html.twig', [

            "categories" => $categories,
            "notice" => $notices,
            "lastOnes" => $lastOnes,
            'slug' => null
        ]);
    }

    /**
     * @Route("/noticias/categoria/{slug}", name="category")
     * 
     */
    public function categorias($slug, Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }
       
        $em = $this->getDoctrine()->getManager();
        $notices = $em->getRepository('App:Notice')->findByCategory($slug);
        $count = count($notices);
        for ($i = 0; $i < $count; $i++) {

            $notices[$i]["description"] = substr_replace($notices[$i]["description"], '', 500, -1);
        }

        $categories = $em->getRepository('App:NewsType')->findAll();
        $lastOnes = $em->getRepository('App:Notice')->findBy([], ['created_at' => 'DESC'], 3);

        return $this->render('front/noticias/index.html.twig', [

            "categories" => $categories,
            "notices" => $notices,
            "lastOnes" => $lastOnes,
            'controller_name' => 'NoticiasController',
            'slug' => $slug
        ]);
    }
}
