<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DoctorsController extends AbstractController
{
    /**
     * @Route("/medicos/material/clg-video01", name="front_doctors")
     */
    public function index(): Response
    {
        return $this->render('front/doctors/index.html.twig', [
            'controller_name' => 'DoctorsController',
        ]);
    }
}
