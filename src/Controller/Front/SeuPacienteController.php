<?php

namespace App\Controller\Front;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Cookie;

class SeuPacienteController extends AbstractController
{
    /**
     * @Route("/voce-e-o-seu-paciente", name="voce-e-o-seu-paciente")
     */
    public function index(Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        
        $products_your_patients = $em->getRepository('App:YourPatient')->createQueryBuilder('p')
                        ->select('distinct(p.products) as product_id')
                        ->getQuery()
                        ->getResult();

        $product_id = [];
        foreach ($products_your_patients as $key => $p) :
            $product_id[] = $p['product_id'];
        endforeach;
        
        if (count($product_id) > 0) { 
            $p = $em->getRepository('App:Products')->createQueryBuilder('p')
                        ->where('p.id IN(:productIds)')
                        ->andWhere('p.is_active = true')
                        ->setParameter('productIds', array_values($product_id));

            if ($request->request->has('e')) {
                if ($request->request->get('s')) {
                    $specialty = $em->getRepository('App:Specialty')->findOneBy(['id' => $request->request->get('s')]);
                    
                    //especialidades de produtos
                    $p->join('p.specialty', 's')
                        ->andWhere('s.id = :specialty')
                        ->setParameter('specialty', $specialty);
                }
            }
                        
            $products = $p->getQuery()
                          ->getResult();
        } else {
            $products = [];
        }

        $specialties = $em->getRepository('App:Specialty')->findAll();
        
        return $this->render('front/seu-paciente/index.html.twig', [
            'products' => $products,
            'specialties' => $specialties
        ]);
    }

    /**
     * @Route("/voce-e-o-seu-paciente-busca", name="voce-e-o-seu-paciente-busca")
     */
    public function busca(Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }
        
        $em = $this->getDoctrine()->getManager();
        $yourpatient = $em->getRepository('App:YourPatient')->findBySpecialty($request->request->get("s"));

        return new JsonResponse([
            'response' => $yourpatient
        ]);
    }
}