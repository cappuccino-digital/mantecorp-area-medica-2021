<?php

namespace App\Controller\Front;

use App\Entity\User;
use App\Entity\UserFavorite;
use App\Entity\UserNewPassword;
use App\Entity\UserTemporary;
use App\Entity\UserTemporaryRegisterStep;
use App\Service\ConsultaMedicosService;
use App\Service\CorreiosService;
use App\Service\EadBoxService;
use App\Service\EmailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGenerator;


use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use function Symfony\Component\VarDumper\Dumper\esc;

class UserController extends AbstractController
{

    /**
     * @Route("/cadastro", name="register-doctor-consultation")
     */
    public function doctorConsultation(Request $request): Response
    {
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('home');
        }

        $em = $this->getDoctrine()->getManager();
        //carregandos os dados do usuário 
        $user_id = $request->cookies->get('_u') ? $request->cookies->get('_u') : null;
        $user_temporary = null;

        if ($user_id) {
            $user_temporary = $em->getRepository('App:UserTemporary')->findOneBy(['id' => $user_id]);
        }

        if ($request->request->has('r')) {
            $crm    = trim($request->request->get('crm'));
            $uf_crm = trim($request->request->get('uf'));

             $errors = [];

            if (!$crm) {
                $errors['crm'] = 'CRM é obrigatório';
            } else {
                //checando se já existe o CRM na base
                $crm_check = $em->getRepository('App:User')->findOneBy(['crm' => $crm, 'uf_crm' => $uf_crm]);
                
                if ($crm_check) {
                    $errors['crm'] = 'CRM está sendo utilizado';
                }
            }

            if (!$uf_crm) {
                $errors['uf_crm'] = 'Estado é obrigatório';
            } 

            if ($crm && $uf_crm) {

                $doctor_consultation = new ConsultaMedicosService();
                $consulta_medicos = $doctor_consultation->consultaMedicos($crm, $uf_crm);
                //dump($consulta_medicos); die();
            
                if (!$consulta_medicos['status']) {
                    if (isset($consulta_medicos['error'])) {
                        if (isset($consulta_medicos['error']['situacao'])) {
                            $errors['crm'] = $consulta_medicos['error']['situacao'];
                        }

                        if (isset($consulta_medicos['error']['name'])) {
                            $errors['name'] = $consulta_medicos['error']['name'];
                        }

                        if (isset($consulta_medicos['error']['uf'])) {
                            $errors['uf_crm'] = $consulta_medicos['error']['uf'];
                        }
                    }
                } 
            }

            
            if (count($errors) > 0) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Campos obrigatórios',
                    'error' => $errors,
                ]);
            }
            
            if ($consulta_medicos['status']) {
                if (isset($consulta_medicos['data'])) {
                        
                    if (isset($consulta_medicos['data']['name'])) {
                        $name = $consulta_medicos['data']['name'];
                    }

                    if (isset($consulta_medicos['data']['uf'])) {
                        $uf_crm = $consulta_medicos['data']['uf'];
                    }

                    //se existir 
                    if (!$user_temporary) {
                        $user_temporary_check_crm = $em->getRepository('App:UserTemporary')->findOneBy(['crm' => $crm, 'uf_crm' => $uf_crm]);

                        if ($user_temporary_check_crm) {
                            $user_temporary = $user_temporary_check_crm;
                        } else {
                            $user_temporary = new UserTemporary();
                            $user_temporary->setCreatedAt(new \DateTimeImmutable());
                        }
                    }
                    
                    $user_temporary->setCrm($crm);
                    $user_temporary->setUfCrm($uf_crm);
                    $user_temporary->setName($name);
            
                    $em->persist($user_temporary);
                    $em->flush();
                    
                    //registrando as etapas do usuário
                    $this->saveStep($user_temporary, 'cadastro-consulta-crm');
                    //end
                   
                    $url = $this->generateUrl('register-profile');
                    
                    $response = new JsonResponse([
                        'status' => true,
                        'message' => 'success',
                        'url' => $url
                    ]);
                
                    $response->headers->setCookie(new Cookie('_u', $user_temporary->getId()));

                    return $response;
                }
            }
        }

        return $this->render('front/user/cadastro-consulta-crm.html.twig', [
            'user_temporary' => $user_temporary
        ]);

        
    }

    /**
     * @Route("/cadastro/perfil", name="register-profile")
     */
    public function registerProfile(Request $request): Response
    {
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('home');
        }

        $em = $this->getDoctrine()->getManager();
        
        //mecânica de etapas 
        $user_id = $request->cookies->get('_u') ? $request->cookies->get('_u') : null;
      
        if (!$user_id) {
            return $this->redirectToRoute('register-doctor-consultation'); 
        }

        $user_temporary = $em->getRepository('App:UserTemporary')->findOneBy(['id' => $user_id]);
       
        if (!$user_temporary) {
            return $this->redirectToRoute('register-doctor-consultation'); 
        }

        $checkStep = $this->checkStep($user_temporary); //verificando etapas preenchidas
        
        if (in_array('cadastro-consulta-crm', $checkStep)) {
            return $this->redirectToRoute('register-doctor-consultation');
        }
        //end

        if ($request->request->has('r')) {
            
            $crm            = $user_temporary->getCrm(); //está gravado na tabela
            $uf_crm         = $user_temporary->getUfCrm(); //está gravado na tabela
            $name           = $user_temporary->getName(); //está gravado na tabela
            $email          = trim($request->request->get('email'));
            $cellphone      = trim($request->request->get('cellphone'));
            $specialty      = $request->request->get('specialty');
            $is_resident    = $request->request->get('is_resident');
            $medical_center = trim($request->request->get('medical_center'));    
        
            //endereço
            $zip_code     = str_replace([' ', '-'], '', trim($request->request->get('zip_code')));
            $address      = trim($request->request->get('address'));
            $number       = trim($request->request->get('number'));
            $complement   = trim($request->request->get('complement'));
            $neighborhood = trim($request->request->get('neighborhood'));
            $city         = trim($request->request->get('city'));
            $uf           = trim($request->request->get('uf'));

            //permissões
            $term       = $request->request->get('is_term');
            $newsletter = $request->request->get('is_newsletter') ? $request->request->get('is_newsletter') : false;
            
            $errors = [];
            
            if (!$crm) {
                $errors['crm'] = 'CRM é obrigatório';
            } else {
                //checando se já existe o CRM na base
                $crm_check = $em->getRepository('App:User')->findOneBy(['crm' => $crm, 'uf_crm' => $uf_crm]);
                if ($crm_check) {
                    $errors['crm'] = 'CRM está sendo utilizado';
                }
            }

            if (!$uf_crm) {
                $errors['uf_crm'] = 'Estado é obrigatório';
            } 

            if (!$name) {
                $errors['name'] = 'Nome é obrigatório';
            } 

            $userCheckEmail = $em->getRepository('App:User')
                                ->createQueryBuilder('u')
                                ->andWhere('u.email = :email or u.username = :username')
                                ->setParameter('email', $email)
                                ->setParameter('username', $email)
                                ->getQuery()
                                ->getResult();

            //checando na tabela de usuário temporário
            $userTempCheckEmail = $em->getRepository('App:UserTemporary')
                                ->createQueryBuilder('ut')
                                ->andWhere('ut.email = :email or ut.username = :username')
                                ->setParameter('email', $email)
                                ->setParameter('username', $email)
                                ->getQuery()
                                ->getResult();
            
            if (!$email) {
                $errors['email'] = "E-mail é obrigatório";
            } else {
                if ($userCheckEmail) {
                    if (($user_temporary->getEmail() != $email) && ($user_temporary->getUsername() != $email)) {
                        $errors['email'] = "E-mail está sendo utilizado";
                    }
                   
                } else if ($userTempCheckEmail) { 
                    if (($user_temporary->getEmail() != $email) && ($user_temporary->getUsername() != $email)) {
                        $errors['email'] = "E-mail está sendo utilizado";
                    }
                    
                } else {
                    //validação e-mail
                    if (!$this->validaEmail($email)) {
                        $errors['email'] = 'E-mail inválido';
                    }
                }
            }

            if (!$cellphone) {
                $errors['cellphone'] = "Celular é obrigatório";
            } else {
                if (strlen($cellphone) < 15) { //contando com as maskaras
                    $errors['cellphone'] = "Celular inválido";
                }
            }  
            
            if (!$specialty) {
                $errors['specialty'] = "Especialidade é obrigatório";
            }  
            
            if (!$is_resident) {
                $errors['is_resident'] = "Residente é obrigatório";
            } 
            
            // if (!$medical_center) {
            //     $errors['medical_center'] = "Centro médico é obrigatório";
            // } 

            if (!$term) {
                $errors['is_term'] = 'A leitura do termo de uso é obrigatório.';
            }

            if (count($errors) > 0) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Campos obrigatórios',
                    'error' => $errors,
                ]);
            }
                
            // $user_temporary->setCrm($crm);
            // $user_temporary->setUfCrm($uf_crm);
            // $user_temporary->setName($name); 
            $user_temporary->setUsername(strtolower($email));
            $user_temporary->setEmail(strtolower($email));
            $user_temporary->setCellphone($cellphone);

            // deletar                    
            if ($user_temporary->getSpecialty()) {
                foreach ($user_temporary->getSpecialty() as $key => $s) {         
                    $user_temporary->removeSpecialty($s);

                    $em->persist($user_temporary);
                    $em->flush();
                }
            }

            //especialidades
            $specialties = $em->getRepository('App:Specialty')->findBy(array('id' => array_values($specialty)));

            if ($specialties) {
                foreach ($specialties as $key => $specialty) {         
                    $user_temporary->addSpecialty($specialty);
                }
            }
            //dump($is_resident == "false" ? false : true); die();
            $user_temporary->setIsResident($is_resident == "false" ? false : true);
            $user_temporary->setMedicalCenter($medical_center);

            $user_temporary->setZipCode($zip_code);
            $user_temporary->setAddress($address);
            $user_temporary->setNumber($number);
            $user_temporary->setComplement($complement);
            $user_temporary->setNeighborhood($neighborhood);
            $user_temporary->setCity($city);
            $user_temporary->setUf($uf);
            $user_temporary->setIsNewsletter($newsletter);
            $user_temporary->setIsTerm($term);
            
            $em->persist($user_temporary);
            $em->flush();

            //registrando as etapas do usuário
            $this->saveStep($user_temporary, 'cadastro-perfil');
            //end

            $url = $this->generateUrl('register-password');
            
            $response = new JsonResponse([
                'status' => true,
                'message' => 'success',
                'url' => $url
            ]);
           
            return $response;
        }
        
        //dump($user_temporary); die();
        $specialties = $em->getRepository('App:Specialty')->findAll();

        return $this->render('front/user/cadastro-perfil.html.twig', [
            'specialties' => $specialties,
            'user_temporary' => $user_temporary
        ]);
    }


    /**
     * @Route("/cadastro/senha", name="register-password")
     */
    public function registerPassword(Request $request, UserPasswordEncoderInterface $encoder, MailerInterface $mailer): Response
    {        
        $em = $this->getDoctrine()->getManager();
       
        //mecânica de etapas 
        $user_id = $request->cookies->get('_u') ? $request->cookies->get('_u') : null;

        if (!$user_id) {
            return $this->redirectToRoute('register-doctor-consultation'); 
        }

        $user_temporary = $em->getRepository('App:UserTemporary')->findOneBy(['id' => $user_id]);

        if (!$user_temporary) {
            return $this->redirectToRoute('register-doctor-consultation');  
        }
        //end
      
        if ($request->request->has('r')) {
            $password       = trim($request->request->get('password'));
            $password_confirm = trim($request->request->get('password_confirm'));
            
            $errors = [];

            if (!$password) {
                $errors['password'] = 'A senha é obrigatória';
            } else {
                if (!$this->passwordValidation($password)) {
                    $errors['password'] = '<p>A senha deve conter uma letra minúscula, uma letra maiúscula, um número e no mínimo 6 caracteres.</p>';
                }
            }
    
            if (!$password_confirm) {
                $errors['password_confirm'] = 'O campo repetir senha é obrigatório.';
            }
    
            if ($password != $password_confirm) {
                $errors['password_confirm'] = 'O campo senha e repetir senha estão diferentes.';
            } 

            if (count($errors) > 0) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Campos obrigatórios',
                    'error' => $errors,
                ]);
            }

            $expiresAt = new \DateTimeImmutable('+1 year');
           
            // $user_temporary->setSalt(md5(uniqid()));
            $user_temporary->setPassword($encoder->encodePassword($user_temporary, $password));
            $user_temporary->setToken($this->generateToken());
            $user_temporary->setExpiresAt($expiresAt);
            $user_temporary->setIsFinished(true);
            
            $em->persist($user_temporary);
            $em->flush();

            //registrando as etapas do usuário
            $this->saveStep($user_temporary, 'cadastro-senhas');
            
            //verificando etapas preenchidas
            $checkStep = $this->checkStep($user_temporary); 
            
            if (count($checkStep) > 0) {
                $response = new JsonResponse([
                    'status' => false,
                    'message' => 'Por favor, preencha todas etapas do cadastro',
                    'data' => $checkStep,
                ]);
                return $response;
            }
            //end
            
            //usuário do site antigo
            if ($user_temporary->getIsUserOld()) { 
                $expiresAt = new \DateTimeImmutable('+1 year');
                $user_temporary->setToken($this->generateToken());
                $user_temporary->setExpiresAt($expiresAt);

                $em->persist($user_temporary);
                $em->flush();

                $url = $this->generateUrl('user-confirm-register-token', array('token' => $user_temporary->getToken()), UrlGenerator::ABSOLUTE_URL);
                
                $response = new JsonResponse([
                    'status' => true,
                    'message' => 'success',
                    'url' => $url
                ]);
               
                return $response;
            }
            //end

            $url = $this->generateUrl('user-confirm-register-token', array('token' => $user_temporary->getToken()), UrlGenerator::ABSOLUTE_URL);

            //send e-mail           
            $email = (new Email())
                ->from(new Address('portalmantecorpfarmasa@gmail.com', 'Mantecorp Farmasa - Área médica'))
                ->to($user_temporary->getEmail())
                //->addBcc('ana.morales@cappuccinodigital.com.br')
                //->replyTo($contact->getEmail())
                ->priority(Email::PRIORITY_HIGH)
                ->subject('Mantecorp Farmasa - Confirmação de cadastro')
                ->html($this->renderView(
                    'front/home/mkt-user-confirm-register.html.twig',
                    [
                        'user_temporary' => $user_temporary,
                        'url' => $url
                    ]
                ));

            $mailer->send($email);
                                    
            $response = new JsonResponse([
                'status' => true,
                'message' => 'success',
                'url' => $this->generateUrl('home')
            ]);

            $response->headers->setCookie(new Cookie('_u', null));
           
            return $response;
        }
       
        return $this->render('front/user/cadastro-senha.html.twig', [
            'user_temporary' => $user_temporary
        ]);
        
    }

    /**
     * @Route("/confimacao-cadastro/{token}", name="user-confirm-register-token")
     */
    public function registrationConfirmation(Request $request, $token, UserPasswordEncoderInterface $encoder, EadBoxService $ead_box_service): Response
    {
        $em = $this->getDoctrine()->getManager();

        if ($token) {
            $user_temporary = $em->getRepository('App:UserTemporary')->findOneBy(['token' => $token]);

            if (!$user_temporary) {
                //exibir mensagem
                return $this->redirectToRoute('home');
            }
            
            //carregando as informações da tabela temporária para salvar na tabela de usuário
            $crm              = $user_temporary->getCrm();
            $uf_crm           = $user_temporary->getUfCrm();
            $name             = $user_temporary->getName();
            $email            = $user_temporary->getEmail();
            $cellphone        = $user_temporary->getCellphone();
            $specialty        = $user_temporary->getSpecialty();
            $is_resident      = $user_temporary->getIsResident();
            $medical_center   = $user_temporary->getMedicalCenter();
            $zip_code         = $user_temporary->getZipCode();
            $address          = $user_temporary->getAddress();
            $number           = $user_temporary->getNumber();
            $complement       = $user_temporary->getComplement();
            $neighborhood     = $user_temporary->getNeighborhood();
            $city             = $user_temporary->getCity();
            $uf               = $user_temporary->getUf();
            $is_newsletter    = $user_temporary->getIsNewsletter();
            $is_term          = $user_temporary->getIsTerm();
            $password         = $user_temporary->getPassword();
            $created_at       = $user_temporary->getCreatedAt();
            
            //verificando se existe um cadastro 
            $user_check_email = $em->getRepository('App:User')->findOneBy(['email' => $email]);
            $user_check_crm = $em->getRepository('App:User')->findOneBy(['crm' => $crm, 'uf_crm' => $uf_crm]);
            
            if ($user_check_email) {
                $user = $user_check_email;
            } else if ($user_check_crm) {
                $user = $user_check_crm;
            } else {
                //gravando na tabela de usuário
                $user = new User();
            }

            $user->setCrm($crm);
            $user->setUfCrm($uf_crm);
            $user->setName($name);
            $user->setEmail($email);
            $user->setUsername($email);
            $user->setEmailEadbox($email);
            $user->setCellphone($cellphone);

            //especialidades
            foreach ($user_temporary->getSpecialty() as $key => $specialty) {
                $user->addSpecialty($specialty);
            }
           
            $user->setIsResident($is_resident);
            $user->setMedicalCenter($medical_center);
            $user->setZipCode($zip_code);
            $user->setAddress($address);
            $user->setNumber($number);
            $user->setComplement($complement);
            $user->setNeighborhood($neighborhood);
            $user->setCity($city);
            $user->setUf($uf);
            $user->setIsNewsletter($is_newsletter);
            $user->setIsTerm($is_term);

            //usuário do site antigo
            if ($user_temporary->getIsUserOld()) { 
                $user->setIsUserOld(true);
                $user->setCreatedAt($created_at);
            } else {
                $user->setIsUserOld(false);
                $user->setCreatedAt(new \DateTime());
            }

            // $user->setSalt(md5(uniqid()));
            //$user->setPassword($encoder->encodePassword($user, $password));
            $user->setPassword($password);

            //add perfil
            $medical_role = $em->getRepository('App:Role')->findOneByRole('ROLE_MEDICAL');
            $user->addRole($medical_role);

            $medical_profile = $em->getRepository('App:Profile')->findOneBySlug('medico');
            $user->setProfile($medical_profile);

            $user->setIsActive(true);
            $user->setUpdatedAt(new \DateTime());
            $user->setLastLoginAt(new \DateTime());
            
            $em->persist($user);
            $em->flush();
            
            $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));

            // Dispara o evento de login manualmente
            $event = new InteractiveLoginEvent($request, $token);
            $dispatcher = new EventDispatcher(); //dispara
            $dispatcher->dispatch('security.interactive_login', $event);
            //$this->get('event_dispatcher')->dispatch('security.interactive_login', $event);
            

            //removendo da tabela temporária
            if ($user) {
                //deletando especialidade vinculado a tabela temporária                   
                if ($user_temporary->getSpecialty()) {
                    foreach ($user_temporary->getSpecialty() as $key => $s) {         
                        $user_temporary->removeSpecialty($s);

                        $em->persist($user_temporary);
                        $em->flush();
                    }
                }

                //deletando etapas do cadastro vinculado ao usuario da tabela temporária                   
                if ($user_temporary->getUserTemporaryRegisterSteps()) {
                    foreach ($user_temporary->getUserTemporaryRegisterSteps() as $key => $rs) {         
                        $user_temporary->removeUserTemporaryRegisterStep($rs);

                        $em->persist($user_temporary);
                        $em->flush();
                    }
                }

                //removendo registro da tabela temporária
                $em->remove($user_temporary);
                $em->flush();
            }
          
            return $this->redirectToRoute('home');
            
        }
               
    }

    /**
     * @Route("/consulta-cep", name="api-correios")
     */
    public function consultaCEP(Request $request)
    {
        if (!$request->request->has('cep')) {
           return new JsonResponse(['status' => false, 'message' => 'CEP não identificado.']);
        }
        
        $cep = str_replace([' ', '-'], '', $request->request->get('cep'));
      
        $correios = new CorreiosService();
        $response = $correios->consultaCEP($cep);

        return new JsonResponse($response);    
    }
    
    /**
     * @Route("/lembrar-senha", name="remember-password")
     */
    public function rememberPassword(Request $request, MailerInterface $mailer)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('home');
        }

        if ($request->isMethod('GET')) {
            return $this->render('front/user/remember-password.html.twig', []);
        }
        
        $email = trim($request->request->get('email'));

        if (!$email) {
            return $this->render('front/user/remember-password.html.twig', [
                'status' => 'Preencha o campo e-mail é obrigatório.',
            ]);
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->findOneByEmail($email);

        if (!$user) {
            return $this->render('front/user/remember-password.html.twig', [
                'status' => 'Usuário não encontrado.'
            ]);
        }
        
        $token = sha1($user->getId().$user->getCreatedAt()->getTimestamp().$user->getSalt().$user->getPassword()).dechex(time()).dechex($user->getId());
        
        $expiresAt = new \DateTime();
        $expiresAt->add(new \DateInterval('PT8H'));
        $newPassword = $em->getRepository('App:UserNewPassword')->findOneByUser($user);

        if (!$newPassword) {
            $newPassword = new UserNewPassword();
        }
        
        $newPassword->setExpiresAt($expiresAt);
        $newPassword->setToken($token);
        $newPassword->setIsValid(true);
        $newPassword->setUser($user);

        $em->persist($newPassword);
        $em->flush();

        //send e-mail
        $email = (new Email())
            ->from(new Address('portalmantecorpfarmasa@gmail.com', 'Mantecorp Farmasa - Área médica'))
            ->to($user->getEmail())
            //->addBcc('ana.morales@cappuccinodigital.com.br')
            //->replyTo($contact->getEmail())
            ->priority(Email::PRIORITY_HIGH)
            ->subject('Mantecorp Farmasa - Recuperar Senha')
            ->html($this->renderView(
                    'front/user/renew-password.html.twig',
                    ['newPassword' => $newPassword, 'user' => $user]
                ));

        $response = $mailer->send($email);
        /*** END */
    
        if($response == null){
            return $this->render('front/user/remember-password.html.twig', [
                'status' => 'Um email foi enviado com as instruções para a troca de senha. Verifique também a sua caixa de SPAM.'
            ]);
        }

        return $this->render('front/user/remember-password.html.twig', [
            'status' => 'Algo deu errado, tente novamente mais tarde'
        ]);
    }

    /**
     * @Route("/trocar-senha", name="change-password")
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer, EadBoxService $ead_box_service)
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('username') || $request->query->has('password')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        $token = $request->query->get('token');

        if (!$token) {
            return $this->redirectToRoute('home');
        }
        
        $em = $this->getDoctrine()->getManager();
        $newPassword = $em->getRepository('App:UserNewPassword')->findOneBy(['token' => $token, 'isValid' => true]);
       
        if (!$newPassword) {
            return $this->render('front/user/change-password.html.twig', [
                'status' => false,
                'message' => 'Este link não é mais válido, tente recuperar sua senha novamente'
            ]);
        }
               
        $now = new \DateTime();

        if ($newPassword->getExpiresAt()->getTimestamp() <= $now->getTimestamp()) {
            return $this->render('front/user/change-password.html.twig', [
                'status' => false,
                'message' => 'Este link expirou, tente recuperar sua senha novamente'
            ]);
        }
      
        $user = $newPassword->getUser();
        $password = $request->request->get('password');
        
        if ($request->isMethod('GET')) {
           
            return $this->render('front/user/change-password.html.twig', [
                'status' => true,
                'token' => $newPassword->getToken()
            ]); 
        }

        //validação senha
        if (!$this->passwordValidation($password)) {
            return $this->render('front/user/change-password.html.twig', [
                'status' => true,
                'token' => $newPassword->getToken(),
                'messagePassword' => '<p>A senha deve conter uma letra minúscula, uma letra maiúscula, um número e no mínimo 6 caracteres.</p>'
            ]); 
        }
        
        $user->setPassword($encoder->encodePassword($user, $password));
        $em->persist($user);

        $newPassword->setIsValid(false);
        $newPassword->setExpiresAt(new \DateTime());

        $em->persist($newPassword);
        $em->flush();

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRolesArray());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));

        // Dispara o evento de login manualmente
        $event = new InteractiveLoginEvent($request, $token);
        $dispatcher = new EventDispatcher(); //dispara
        $dispatcher->dispatch('security.interactive_login', $event);
        //$this->get('event_dispatcher')->dispatch('security.interactive_login', $event);

        //EadBox update da senha
        //$ead_box_service->updateUser($user);
        
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/usuario/perfil", name="user-profile")
     */
    public function userProfile(Request $request, EadBoxService $ead_box_service): Response
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('zip_code') || $request->query->has('medical_center')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('login');
        }

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
                    
        if ($request->request->has('r')) {
            
            $crm            = $user->getCrm();
            $uf_crm         = $user->getUfCrm();
            $name           = $user->getName();
            $email          = trim($request->request->get('email'));
            $cellphone      = trim($request->request->get('cellphone'));
            $specialty      = $request->request->get('specialty');
            $is_resident    = $request->request->get('is_resident');
            $medical_center = trim($request->request->get('medical_center'));    
            
            //endereço
            $zip_code     = str_replace([' ', '-'], '', trim($request->request->get('zip_code')));
            $address      = trim($request->request->get('address'));
            $number       = trim($request->request->get('number'));
            $complement   = trim($request->request->get('complement'));
            $neighborhood = trim($request->request->get('neighborhood'));
            $city         = trim($request->request->get('city'));
            $uf           = trim($request->request->get('uf'));

            //permissões
            $newsletter = $request->request->get('is_newsletter') ? $request->request->get('is_newsletter') : false;

            $errors = [];
            
            if (!$crm) {
                $errors['crm'] = 'CRM é obrigatório';
            } else {
                //checando se já existe o CRM na base
                $crm_check = $em->getRepository('App:User')->findOneBy(['crm' => $crm, 'uf_crm' => $uf_crm]);
                if ($crm_check) {
                    if (($user->getCrm() != $crm) && ($user->getCrm() != $crm)) {
                        $errors['crm'] = 'CRM está sendo utilizado';
                    }
                }
            }

            if (!$uf_crm) {
                $errors['uf_crm'] = 'Estado é obrigatório';
            } 

            if (!$name) {
                $errors['name'] = 'Nome é obrigatório';
            } 

            $userCheckEmail = $em->getRepository('App:User')
                                ->createQueryBuilder('u')
                                ->andWhere('u.email = :email or u.username = :username')
                                ->setParameter('email', $email)
                                ->setParameter('username', $email)
                                ->getQuery()
                                ->getResult();

            //checando na tabela de usuário temporário
            $userTempCheckEmail = $em->getRepository('App:UserTemporary')
                                ->createQueryBuilder('ut')
                                ->andWhere('ut.email = :email or ut.username = :username')
                                ->setParameter('email', $email)
                                ->setParameter('username', $email)
                                ->getQuery()
                                ->getResult();

            if (!$email) {
                $errors['email'] = "E-mail é obrigatório";
            } else {
                if ($userCheckEmail) {                    
                    if (($user->getEmail() != $email) && ($user->getUsername() != $email)) {
                        $errors['email'] = "E-mail está sendo utilizado";
                    }

                } else if ($userTempCheckEmail) { 
                    if (($user->getEmail() != $email) && ($user->getUsername() != $email)) {
                        $errors['email'] = "E-mail está sendo utilizado";
                    }
                    
                } else {
                    //validação e-mail
                    if (!$this->validaEmail($email)) {
                        $errors['email'] = 'E-mail inválido';
                    }
                }
            }
            
            if (!$cellphone) {
                $errors['cellphone'] = "Celular é obrigatório";
            } else {
                if (strlen($cellphone) < 15) { //contando com as maskaras
                    $errors['cellphone'] = "Celular inválido";
                }
            }  
            
            if (!$specialty) {
                $errors['specialty'] = "Especialidade é obrigatório";
            }  
            
            if (!$is_resident) {
                $errors['is_resident'] = "Residente é obrigatório";
            } 
            
            // if (!$medical_center) {
            //     $errors['medical_center'] = "Centro médico é obrigatório";
            // } 
          
            if (count($errors) > 0) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Campos obrigatórios',
                    'error' => $errors,
                ]);
            }

            // $user_temporary->setCrm($crm);
            // $user_temporary->setUfCrm($uf_crm);
            // $user_temporary->setName($name); 
            $user->setUsername(strtolower($email));
            $user->setEmail(strtolower($email));
            $user->setCellphone($cellphone);

            // deletar                    
            if ($user->getSpecialty()) {
                foreach ($user->getSpecialty() as $key => $s) {         
                    $user->removeSpecialty($s);

                    $em->persist($user);
                    $em->flush();
                }
            }

            //especialidades
            $specialties = $em->getRepository('App:Specialty')->findBy(array('id' => array_values($specialty)));

            if ($specialties) {
                foreach ($specialties as $key => $specialty) {         
                    $user->addSpecialty($specialty);
                }
            }
            
            $user->setIsResident($is_resident == "false" ? false : true);
            $user->setMedicalCenter($medical_center);

            $user->setZipCode($zip_code);
            $user->setAddress($address);
            $user->setNumber($number);
            $user->setComplement($complement);
            $user->setNeighborhood($neighborhood);
            $user->setCity($city);
            $user->setUf($uf);
            $user->setIsNewsletter($newsletter);
                       
            $em->persist($user);
            $em->flush();
            
            $url = $this->generateUrl('user-password');
            
            $response = new JsonResponse([
                'status' => true,
                'message' => 'success',
                'url' => $url
            ]);
           
            return $response;
        }
        
        //dump($user_temporary); die();
        $specialties = $em->getRepository('App:Specialty')->findAll();

        return $this->render('front/user/usuario-perfil.html.twig', [
            'specialties' => $specialties,
            'user' => $user,
            'medicalCenters' => $this->medicalCenter()
        ]);
    }

    /**
     * @Route("/usuario/senha", name="user-password")
     */
    public function userPassword(Request $request, UserPasswordEncoderInterface $encoder, EadBoxService $ead_box_service): Response
    {        
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('login');
        }

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        
        if ($request->request->has('r')) {
            $password       = trim($request->request->get('password'));
            $password_confirm = trim($request->request->get('password_confirm'));
            
            $errors = [];

            if (!$password) {
                $errors['password'] = 'A senha é obrigatória';
            } else {
                if (!$this->passwordValidation($password)) {
                    $errors['password'] = '<p>A senha deve conter uma letra minúscula, uma letra maiúscula, um número e no mínimo 6 caracteres.</p>';
                }
            }
    
            if (!$password_confirm) {
                $errors['password_confirm'] = 'O campo repetir senha é obrigatório.';
            }
    
            if ($password != $password_confirm) {
                $errors['password_confirm'] = 'O campo senha e repetir senha estão diferentes.';
            } 

            if (count($errors) > 0) {
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Campos obrigatórios',
                    'error' => $errors,
                ]);
            }

            $user->setPassword($encoder->encodePassword($user, $password));
            $user->setUpdatedAt(new \DateTime());
            
            $em->persist($user);
            $em->flush();

            //EadBox update da senha
            //$ead_box_service->updateUser($user);
            
            $url = $this->generateUrl('home');
            
            $response = new JsonResponse([
                'status' => true,
                'message' => 'success',
                'url' => $url
            ]);
           
            return $response;
            
        }
       
        return $this->render('front/user/usuario-senha.html.twig', [
            'user' => $user
        ]);
        
    }

    /**
     * @Route("/add-favorite/{id}/{type}", name="add-favorite")
     */
    public function addFavorite($id, $type)
    {
        $user = $this->getUser();
      
        if (!$user) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Você precisa estar logado'
            ]);    
        }
        //dump('test', $user); die();
        $em = $this->getDoctrine()->getManager();
        $favorite = $em->getRepository('App:UserFavorite')->findOneBy(['typeId' => $id, 'type' => $type, 'user' => $user]);

        if ($favorite) {
            if($favorite->getIsActive()){
                $favorite->setIsActive(false);
            } else {
                $favorite->setIsActive(true);
            }
            $favorite->setUpdatedAt(new \DateTime());
        } else {
            $favorite = new UserFavorite();
            $favorite->setIsActive(true);
            $favorite->setTypeId($id);
            $favorite->setType($type);
            $favorite->setUpdatedAt(new \DateTime());
            $favorite->setCreatedAt(new \DateTime());
            $favorite->setUser($user);
        }

        $em->persist($favorite);
        $em->flush();

        $favorite = $em->getRepository('App:UserFavorite')->findOneBy(['typeId' => $id, 'type' => $type, 'user' => $user]);

        if ($favorite->getIsActive()) {
            $message = 'Esse conteúdo foi adicionado na sua lista de favoritos';
        } else {
            $message = 'Esse conteúdo foi removido da sua lista de favoritos';
        }
 
        return new JsonResponse([
            'status' => true,
            'message' => $message,
            'isActive' => $favorite->getIsActive()
        ]); 

    }

    private function saveStep($user_temporary, $step) 
    {
        $em = $this->getDoctrine()->getManager();
        $register_step = $em->getRepository('App:RegisterStep')->findOneBy(['slug' => $step, 'is_active' => true]);
                    
        $user_temporary_register_step = $em->getRepository('App:UserTemporaryRegisterStep')->findOneBy(['user_temporary' => $user_temporary, 'register_step' => $register_step]);
        if (!$user_temporary_register_step) {
            $user_temporary_register_step = new UserTemporaryRegisterStep();
            $user_temporary_register_step->setCreatedAt(new \DateTime());
            $user_temporary_register_step->setUserTemporary($user_temporary);
            $user_temporary_register_step->setRegisterStep($register_step);
        }
        
        $user_temporary_register_step->setUpdatedAt(new \DateTime());

        $em->persist($user_temporary_register_step);
        $em->flush();
        
        return $user_temporary_register_step;
    }

    private function checkStep($user_temporary) 
    {
        $em = $this->getDoctrine()->getManager();
        $register_steps = $em->getRepository('App:RegisterStep')->findBy(['is_active' => true]);
                         
        $user_temporary_register_steps = $em->getRepository('App:UserTemporaryRegisterStep')->findBy(['user_temporary' => $user_temporary]);

        $user_step = []; 
        foreach ($user_temporary_register_steps as $key => $user_register_step) {
            $user_step[] = $user_register_step->getRegisterStep()->getSlug();
        }
        
        $step_register = [];
        $step_not_register = [];
        
        foreach ($register_steps as $key => $step) : 
            if (in_array($step->getSlug(), $user_step)) {
                $step_register[] = $step->getSlug(); //etapas preenchida pelo usuário
            } else {
                $step_not_register[] = $step->getSlug();
            }
        endforeach;
        
        return $step_not_register;
    }

    /**
     * @Route("/cron-usuarios-pendentes", name="cron-user-pending")
     */
    public function cronUserPending(Request $request, MailerInterface $mailer)
    {
        $em = $this->getDoctrine()->getManager();
        //$offset = $request->query->has('offset') ? (int) $request->query->get('offset') * 1 : 1;
        $offset = $request->query->has('offset') ? (int) $request->query->get('offset') * 100 : 100;
        
        if ($request->query->has('q')) {          
            $amount = $request->query->has('amount') ? $request->query->get('amount') : null;

            $em = $this->getDoctrine()->getManager();
            $user_temporaries       = $em->getRepository('App:UserTemporary')->findBy(['is_user_old' => true, 'is_cron_email' => true], ['created_at' => 'DESC']);
            $total_user_temporaries = $em->getRepository('App:UserTemporary')->findBy(['is_user_old' => true], ['created_at' => 'DESC']);;
           
            $total_user_temporaries = count($total_user_temporaries) - $offset;

            if (count($user_temporaries) > $total_user_temporaries) {
               
                foreach ($user_temporaries as $user_temporary) {

                    //send e-mail           
                    $email = (new Email())
                    ->from(new Address('portalmantecorpfarmasa@gmail.com', 'Mantecorp Farmasa - Área médica'))
                    ->to($user_temporary->getEmail())
                    //->to('ana.morales@cappuccinodigital.com.br')
                    //->addBcc('wellington.santos@cappuccinodigital.com.br')
                    // ->to('samira.pegoraro@hypera.com.br')
                    // ->addBcc('Gabriela.Benicio@cappuccinodigital.com.br')
                    //->addBcc('ana.morales@cappuccinodigital.com.br')
                    //->replyTo($contact->getEmail())
                    ->priority(Email::PRIORITY_HIGH)
                    ->subject('Sua plataforma de conteúdo está de cara nova!')
                    ->html($this->renderView(
                        'front/user/mkt-user-pendentes.html.twig',
                        []
                    ));

                    $mailer->send($email);

                    $user_temporary->setIsCronEmail(false);
                    
                    $em->persist($user_temporary);
                    $em->flush();                                                  
                
                    return new JsonResponse([
                        'status' => true,
                        'pending' => count($user_temporaries),
                        'total' => count($user_temporaries),
                        'message' => 'Enviado com sucessso'
                    ]);  
                }//end foreach

            } else {
                if ((count($user_temporaries) == $total_user_temporaries)) {
                    return new JsonResponse([
                        'status' => true,
                        'message' => 'Todos os contatos foram enviados com sucesso'
                    ]);  
                }

            } //end if   

            return new JsonResponse([
                'status' => false,
                'message' => 'Nenhum registro foi encontrado'
            ]);  
        
        }//end if 
       
        return $this->render('front/user/cron-usuario-pendente.html.twig', [
              // 'when' => $when
        ]);
        
    }

    private function validaEmail($mail) {
        if(preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(.[[:lower:]]{2,3})(.[[:lower:]]{2})?$/", $mail)) {
            return true;
        }else{
            return false;
        }
    }

    public function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }

    function passwordValidation($password) {
        return preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[\w$@]{6,}$/', $password);
    }

    function medicalCenter() 
    {
        return [
            "",
            "CEMA HOSPITAL ESPECIALIZADO LTDA",
            "CEMIL",
            "CLINICA OTORRINUS",
            "CLÍNICA PEDRO CAVALCANTI",
            "CLINIVAN CLÍNICA DE OTORRINOLARINGOLOGIA BP",
            "CONJUNTO HOSPITALAR (LEONOR, PUC E REGIONAL)",
            "CONJUNTO HOSPITALAR DO MANDAQUI",
            "ESCOLA PAULISTA DE MEDICINA",
            "FACULDADE DE MEDICINA DE PETROPOLIS",
            "GAFREE - UNIRIO",
            "GAFREE - UNIRIO",
            "HC - HOSPITAL DAS CLÍNICAS",
            "HC - HOSPITAL DAS CLÍNICAS-GO.",
            "HCFMUSP- HOSPITAL DAS CLÍNICAS DE SÃO PAULO",
            "HCFMUSP- HOSPITAL DAS CLÍNICAS DE SÃO PAULO",
            "HCRP - HOSPITAL DAS CLINICAS DE RIBEIRÃO PRETO",
            "HGV - AMBULATÓRIO ZAUM",
            "HOSP. INFTANTIL ALBERT SABIN",
            "HOSP. UNIV. ONOFRE LOPES (HOSPEP - RES. PED)",
            "HOSP. VARELA SANTIAGO",
            "HOSPITA PAULISTA DE OTORRINO",
            "HOSPITAL AGAMENOM MAGALHAES",
            "HOSPITAL ARLINDA MARQUES",
            "HOSPITAL BARAO DE LUCENA",
            "HOSPITAL BELO HORIZONTE",
            "HOSPITAL BETTINA FERRO",
            "HOSPITAL BOS SOROCABA",
            "HOSPITAL CELSO RAMOS",
            "HOSPITAL DA CRIANÇA",
            "HOSPITAL DA CRUZ VERMELHA BRASILEIRA",
            "HOSPITAL DA LAGOA",
            "HOSPITAL DARCY VARGAS",
            "HOSPITAL DAS CLINICAS",
            "HOSPITAL DAS CLINICAS DE MARILIA",
            "HOSPITAL DAS CLINICAS DE SÃO PAULO - HCFMUSP",
            "HOSPITAL DAS FORÇAS ARMADAS - HFA",
            "HOSPITAL DE BASE",
            "HOSPITAL DE BASE",
            "HOSPITAL DE CLÍNICAS",
            "HOSPITAL DO SERVIDOR - IAMSPE",
            "HOSPITAL E MATERNIDADE SANTA CASA DE MISERICÓRDIA",
            "HOSPITAL ESAU MATOS",
            "HOSPITAL ESTADUAL DE FEIRA DE SANTANA",
            "HOSPITAL FEDERAL DA LAGOA",
            "HOSPITAL FELÍCIO ROCHO",
            "HOSPITAL GERAL DE CUIBA",
            "HOSPITAL GERAL DE FORTALEZA",
            "HOSPITAL GERAL DE FORTALIZA",
            "HOSPITAL GUILHERME ALVARO",
            "HOSPITAL HAOC",
            "HOSPITAL INFANTIL DE CACHOEIRO DE ITAPEMIRIM",
            "HOSPITAL INFANTIL DE JOINVILLE",
            "HOSPITAL INFANTIL DE PALMAS",
            "HOSPITAL INFANTIL JOANA DE GUSMÃO",
            "HOSPITAL INFANTIL LUCILIO PORTELA",
            "HOSPITAL INFANTIL SEARA DO BEM",
            "HOSPITAL IRMÃ DULCE",
            "HOSPITAL JUVENCIO MATOS",
            "HOSPITAL MARCILIO DIAS",
            "HOSPITAL MATERNO INFANTIL",
            "HOSPITAL MATERNO INFANTIL (PED)",
            "HOSPITAL MATERNO INFANTIL PRESIDENTE VARGAS",
            "HOSPITAL MUNICIPAL DE FOZ",
            "HOSPITAL MUNICIPAL INFANTIL MENINO JESUS",
            "HOSPITAL MUNICIPAL TATUAPÉ",
            "HOSPITAL OTORRINO DE MATO GROSSO",
            "HOSPITAL PEQUENO ANJO",
            "HOSPITAL PEQUENO PRINCIPE",
            "HOSPITAL PROF EDMUNDO VASCONCELOS",
            "HOSPITAL PUC",
            "HOSPITAL PUCC",
            "HOSPITAL REGIONAL",
            "HOSPITAL REGIONAL DE PONTA GROSSA",
            "HOSPITAL REGIONAL DO PARANOÁ",
            "HOSPITAL SAMUEL LIBÂNIO",
            "HOSPITAL SANTA CASA",
            "HOSPITAL SANTA CASA",
            "HOSPITAL SANTA CASA DE CURITIBA/ HOSPITAL CAJURU",
            "HOSPITAL SANTA ISABEL",
            "HOSPITAL SANTA MARCELINA",
            "HOSPITAL SANTA MARCELINA ITAQUERA",
            "HOSPITAL SANTO ANTONIO",
            "HOSPITAL SÃO CAMILO",
            "HOSPITAL SÃO LUCAS DE CASCAVEL",
            "HOSPITAL SÃO VICENTE DE PAULO",
            "HOSPITAL SERVIDOR PUBLICO MUNICIPAL",
            "HOSPITAL UNESP BOTUCATU",
            "HOSPITAL UNESP DE BOTUCATU",
            "HOSPITAL UNIVERSITÁRIO",
            "HOSPITAL UNIVERSITÁRIO - C. GRANDE",
            "HOSPITAL UNIVERSITARIO ADRIANO JORGE",
            "HOSPITAL UNIVERSITÁRIO DA ULBRA",
            "HOSPITAL UNIVERSITÁRIO DE BRASÍLIA",
            "HOSPITAL UNIVERSITÁRIO DE LONDRINA PR",
            "HOSPITAL UNIVERSITÁRIO DE SANTA MARIA - HUSM",
            "HOSPITAL UNIVERSITARIO JULIO MULLER",
            "HOSPITAl UNIVERSITÁRIO(HU)",
            "HOSPITAL VEREDAS",
            "HOSPTAL DA CRIANÇA",
            "HOSPTAL DE OTORRINOS DE SJRP (HIORP)",
            "HRC - HOSPITAL REGIONAL DE CEILÂNDIA",
            "HRT - HOSPITAL REGIONAL DE TAGUATINGA",
            "HSVP",
            "HU - UNIVERSIDADE FEDERAL DE JUIZ DE FORA",
            "HUAP",
            "HUPE - HOSPITAL UNIVERSITARIO PEDRO ERNESTO",
            "IAMSP",
            "IMIP",
            "INOOA",
            "INSTITUTO DE OTORRINO DR.AUREO COLOMBI",
            "INSTITUTO DE OTORRINOLARINGOLOGIA DE BELO HORIZONTE",
            "INSTITUTO HOSPITAL DE BASE DO DISTRITO FEDERAL",
            "INSTITUTO PENIDO BURNIER",
            "IPO",
            "NOROSPAR",
            "POLICLINICA DE BOTAFOGO",
            "POLICLINICA PATO BRANCO",
            "PPRONTO SOCORRO TRAUMATOLOGICO MARCILIO DIAS LTDA",
            "PRONTO BABY",
            "RES. OTO HOSPITAL ONOFRE LOPES - HUOL",
            "SANTA CASA",
            "SANTA CASA DE BELÉM",
            "SANTA CASA DE MISERICÓRDIA",
            "SANTA CASA DE MISERICÓRDIA DE SÃO PAULO",
            "SANTA CASA DE MISERICORDIA DE SÃO PAULO",
            "SANTA CASA DE PORTO ALEGRE",
            "SANTA ISABEL - HSI",
            "SENSORIUM",
            "SEPTO SERVIÇOS ESPECIALIZADOS DE TRATAMENTO OTORRINOLARINGOL",
            "SOS OTORRINO",
            "STA CASA DE MISERICORDIA DE JUIZ DE FORA",
            "UFTM - UNIVERSIDADE FEDERAL DO TRIANGULO MINEIRO / PEDIATRIA",
            "UFU - UNIVERDADE FEDERAL DE UBERLÂNDIA",
            "UNICAMP",
            "UNISC",
            "USF/HOSPITAL UNIVERSITÁRIO SÃO FRANCISCO DE ASSIS",
            "USP - UNIVERSIDADE DE SÃO PAULO - CAMPUS BAURU",
            "OUTROS"
        ];
    }

}
