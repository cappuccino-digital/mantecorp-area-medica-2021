<?php

namespace App\Controller\Front;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AtuacaoController extends AbstractController
{
    /**
     * @Route("/area-de-atuacao", name="area-de-atuacao")
     */
    public function index(): Response
    {
        return $this->render('front/area-de-atuacao/index.html.twig', [
            'controller_name' => 'AtuacaoController',
        ]);
    }
}

