<?php

namespace App\Controller\Front;

use App\Entity\UserAvaliation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;

class EstudosCientificosController extends AbstractController
{
    /**
     * @Route("/estudos-cientificos", name="estudos-cientificos")
     */
    public function index(Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        if ($request->request->get('e')) {
            $avaliation = $request->request->get("a");
            $studies_id = $request->request->get('s'); //ID do estudo cientifico enviado da view
      
            $scientific_studies = $em->getRepository('App:ScientificStudies')->findOneBy(['id' => $studies_id]);
            if ($scientific_studies) {
                $user_avaliation = $em->getRepository('App:UserAvaliation')->findOneBy(['user' => $user, 'scientific_studies' => $studies_id]);

                if (!$user_avaliation) {
                    $user_avaliation = new UserAvaliation();
                    $user_avaliation->setUser($user);
                    $user_avaliation->setScientificStudies($scientific_studies);
                    $user_avaliation->setCreatedAt(new \DateTime());
                } 
    
                $user_avaliation->setAvaliation($avaliation);
                $user_avaliation->setUpdatedAt(new \DateTime());
                
                $em->persist($user_avaliation);
                $em->flush();
            }
            
                
            return new JsonResponse([
                'status' => true,
                'message' => 'Avaliação feita com sucesso'
            ]);
        }

        $studies = $em->getRepository('App:ScientificStudies')->findBy([], ['name' => 'ASC']);
        $specialties = $em->getRepository('App:Specialty')->findAll();

        return $this->render('front/estudos-cientificos/index.html.twig', [
            'controller_name' => 'EstudosCientificosController',
            'studies' => $studies,
            'specialties' => $specialties,
            'letter' => null
        ]);
    }


    /**
     * @Route("/estudos-cientificos/buscar/{letra}", name="estudos-cientificos-buscar-letra")
     */
    public function buscarPorLetra($letra, Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('login');
        }

        $em = $this->getDoctrine()->getManager();
        $studies = $em->getRepository('App:ScientificStudies')->createQueryBuilder('s')
                    ->where('s.name like :busca')
                    ->setParameter('busca', "{$letra}%")
                    ->getQuery()
                    ->getResult();

        $specialty = $em->getRepository('App:Specialty')->findAll();

        return $this->render('front/estudos-cientificos/index.html.twig', [
            'controller_name' => 'EstudosCientificosController',
            'studies' => $studies,
            'specialties' => $specialty,
            'letter' => $letra
        ]);
    }

    /**
     * @Route("/estudos-cientificos/busca", name="estudos_cientificos_busca")
     */
    public function buscar(Request $request): Response
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('s') || $request->query->has('type')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }
        
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('login');
        }

        $em = $this->getDoctrine()->getManager();
        if ($request->request->get('type') == 'specialty') {

            $studies = $em->getRepository('App:ScientificStudies')->findByNomeSpecialty($request->request->get('s'));
        }

        if ($request->request->get('type') == 'nome') {

            $studies = $em->getRepository('App:ScientificStudies')->findByNome($request->request->get('s'));
        }

        if (!$studies) {
            return new JsonResponse([
                'response' => "",
            ]);
        }
        for ($i = 0; $i < count($studies); $i++) {
            $products = array(
                "id" => $studies[$i]["product_id"],
                "name" => $studies[$i]["product"],
                "image" => $studies[$i]["product_image"]
            );

            $specialties = array(
                "id" => $studies[$i]["specialty_id"],
                "specialty" => $studies[$i]["specialty"]
            );
            $study[$i] = array(
                "id" => $studies[$i]["id"],
                "name" => $studies[$i]["name"],
                "resume" => $studies[$i]["resume"],
                "imageweb" => $studies[$i]["imageweb"],
                "file" => $studies[$i]["file"],
                "specialty" => $specialties,
                "product" => $products

            );
        }
        return new JsonResponse([
            'response' => $studies,
        ]);
    }

    /**
     * @Route("/estudos-cientificos/avaliation", name="estudos_cientificos_avaliation")
     */
    public function avaliacao(Request $request)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $avaliation = $request->request->get("avaliation");
        $studies_id = $request->request->get('study_id'); //ID do estudo cientifico enviado da view

        $scientific_studies = $em->getRepository('App:ScientificStudies')->findOneBy(['id' => $studies_id]);

        $user_avaliation = $em->getRepository('App:UserAvaliation')->findOneBy(["user" => $user, "study" => $studies_id]);

        if ($user_avaliation) {
            $user_avaliation = new UserAvaliation();
            $user_avaliation->setUser($user);
            $user_avaliation->setScientificStudies($scientific_studies);
            $user_avaliation->setCreatedAt(new \DateTime());
        } 

        $user_avaliation->setAvaliation($avaliation);
        $user_avaliation->setUpdatedAt(new \DateTime());
           
        $em->persist($user_avaliation);
        $em->flush();
            
        return new JsonResponse([
            'status' => true,
            'message' => 'Avaliação feita com sucesso'
        ]);
    }
}