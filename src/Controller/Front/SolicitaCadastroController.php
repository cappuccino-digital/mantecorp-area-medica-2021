<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;

class SolicitaCadastroController extends AbstractController
{
    /**
     * @Route("/cadastre-se", name="cadastre-se")
     */
    public function index(Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        return $this->render('front/cadastre-se/index.html.twig', [
            'controller_name' => 'SolicitaCadastroController',
        ]);
    }
}
