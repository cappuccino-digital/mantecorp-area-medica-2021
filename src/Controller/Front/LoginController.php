<?php

namespace App\Controller\Front;

use App\Entity\MedicalUser;
use App\Entity\Specialty;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


use App\Entity\UserAcess;
use App\Service\FileUploader;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


class LoginController extends AbstractController
{

    /**
     * @Route("/signin", name="signin")
     */
    public function login(Request $request, UserPasswordEncoderInterface $encoder)
    {

        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('GET')) {

            return $this->render('front/home/login.html.twig', [         
                
            ]);
        }

        $medicalUser = new User();
        $login = $request->request->get('login');

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:MedicalUser')->findOneBy(['email' => $login["email"], 'is_active' => true]);

        $password = $encoder->encodePassword($medicalUser, $login["password"]);
        if ($password == $user->getPassword()) {
            // pega o primeiro usuário
            if (!isset($_SESSION)) {
                session_start();
                $date = new DateTime();
                $_SESSION['logged_in'] = true;
                $_SESSION['userName'] = $user->getUserName();
                $_SESSION['token'] = base64_encode(hash_hmac('sha256', base64_encode($user->getUserName()) . "." . base64_encode(strlen($date->format('Y-m-d-H-i-s'))) . "encrypt", true));

                return $this->redirect('/');
            }
        } else {
            return $this->render('front/home/login.html.twig', [
                'error' => 'Usuário ou senha incorretos',
                'is_loged' => false
            ]);
        }
    }



    // /**
    //  * @Route("signup", name="signup") 
    //  */
    // public function index(Request $request, UserPasswordEncoderInterface $encoder): Response
    // {


    //     $em = $this->getDoctrine()->getManager();
    //     $specialty = $em->getRepository('App:Specialty')->findAll();
    //     if ($request->isMethod('GET')) {

    //         return $this->render('front/home/cadastro.html.twig', [
    //             "specialties" => $specialty,
    //             "is_loged" => " "
    //         ]);
    //     }


    //     $user_acess = $em->getRepository('App:UserAcess')->getPermission($this->getUser(), 'usuarios-medicos', 'new');
    //     $user = new User();
    //     if (!$user_acess['status'])
    //         throw $this->createNotFoundException($user_acess['message']);
    //     /* end validação */

    //     $register = $request->request->get('register');
    //     $specialty = $em->getRepository('App:Specialty')->findById($register['specialty']);
    //     $specialty_save = new Specialty();

    //     $specialty_save->setId($specialty[0]['id']);
    //     $specialty_save->setSpecialty($specialty[0]['specialty']);

    //     $medical_user = new MedicalUser();
    //     $medical_user->setUsername($register['name']);
    //     $medical_user->setCrm($register['crm']);
    //     $medical_user->setCrmState($register['crm_state']);
    //     $medical_user->setEmail($register['email']);
    //     $medical_user->setCellphone($register['cellphone']);
    //     $medical_user->setSpecialty($specialty_save);
    //     $medical_user->setResidentDoctor($register['resident_doctor']);
    //     $medical_user->setMedicalCenter($register['medical_center']);
    //     $medical_user->setCep($register['cep']);
    //     $medical_user->setStreet($register['street']);
    //     $medical_user->setNumber($register['number']);
    //     $medical_user->setComplement($register['complement']);
    //     $medical_user->setDistrict($register['district']);
    //     $medical_user->setCity($register['city']);
    //     $medical_user->setState($register['uf']);
    //     $medical_user->setReceiveNews($register['receive_news']);
    //     $medical_user->setTermsOfUse($register['terms_of_use']);
    //     $medical_user->setPassword($encoder->encodePassword($user, $register['password']));
    //     $medical_user->setCreatedAt(new \DateTime());
    //     $medical_user->setUpdatedAt(new \DateTime());
    //     $medical_user->setIsActive(true);
    //     $em->persist($medical_user);
    //     $em->flush();


    //     $lastOnes = $em->getRepository('App:Notice')->findBy([], ['created_at' => 'DESC'], 3);
    //     $count = count($lastOnes);

    //     for ($i = 0; $i < $count; $i++) {

    //         $description = $lastOnes[$i]->getDescription();
    //         $description = substr_replace($description, '', 500, -1);
    //         $lastOnes[$i]->setDescription($description);
    //     }

    //     return $this->render('front/home/index.html.twig', [
    //         "lastOnes" => $lastOnes,
    //         'controller_name' => 'HomeController',
    //         "is_loged" => false
    //     ]);
    // }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request)
    {
        session_destroy();
    }

    /**
     * @Route("/verifica/{email}", name="verifica_email")
     */
    public function verificaEmail(Request $request, $email)
    {

        $em = $this->getDoctrine()->getManager();
        $email = $em->getRepository('App:MedicalUser')->verificaEmail($email);
        if ($email) {
            return new JsonResponse([
                'hasEmail' => true,
            ]);
        }
        return new JsonResponse([
            'hasEmail' => false,
        ]);
    }
}