<?php

namespace App\Controller\Front;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;

class AboutController extends AbstractController
{
    /**
     * @Route("/quem-somos", name="quem-somos")
     */
    public function index(Request $request): Response
    {
        return $this->render('front/quem-somos/index.html.twig', []);
    }
}
