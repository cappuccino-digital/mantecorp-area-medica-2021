<?php

namespace App\Controller\Front;


use App\Entity\Specialty;
use App\Entity\Notice;
use App\Entity\Products;
use Doctrine\ORM\Query;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Validator\Constraints as Assert;

class ProdutosController extends AbstractController
{
    /**
     * @Route("/produtos", name="produtos")
     */
    public function index(Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        if ($request->request->has('s')) {
            $search = $request->request->get('s');

            $products = [];

            $q = $em->getRepository('App:Products')->createQueryBuilder('p')
                ->where('p.is_active = true')
                ->orderBy('p.name', 'ASC');

            if ($request->request->get('e')) {
                $specialty = $request->request->get('e');
                $specialties = $em->getRepository('App:Specialty')->findOneBy(['id' => $specialty]);

                if ($specialties) {
                    $response = $em->getRepository('App:Products')->createQueryBuilder('p')
                        ->join('p.specialty', 's')
                        ->where('s.id = :specialty')
                        ->andWhere('p.is_active = true')
                        ->setParameter('specialty', $specialties)
                        ->orderBy('p.name', 'ASC')
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);

                    return new JsonResponse([
                        'status' => true,
                        'response' => $response
                    ]);
                }
            }

            if ($request->request->has('t')) {
                $product_type = $request->request->get('t');

                if (($product_type != 'todos') && ($product_type != 'lancamento')) {
                    $q = $q->andWhere('p.product_type = :type')
                        ->setParameter('type', "{$product_type}");
                }
            }

            if ($request->request->has('l')) {
                $letter = $request->request->get('l');

                $products = $q->andWhere('p.name like :letter OR p.descriptive like :letter')
                    ->setParameter('letter', "{$letter}%");

                if ($search) {
                    $products = $q->andWhere('p.name like :search OR p.descriptive like :search')
                        ->setParameter('search', "%{$search}%");
                }
            } else if (trim($search) != '') {
                $products = $q->andWhere('p.name like :search OR p.descriptive like :search')
                    ->setParameter('search', "{$search}%");
            } else {
                $products = $q;
            }

            $response = $products->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);

            return new JsonResponse([
                'status' => true,
                'response' => $response
            ]);
        }

        return $this->render('front/produtos/index.html.twig', ['letra' => null]);
    }

    /**
     * @Route("/produtos/prescritos", name="prescritos")
     */
    public function prescritos(Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $specialty = $em->getRepository('App:Specialty')->findAll();
        $products = $em->getRepository('App:Products')->findBy(['product_type' => 'com-prescricao'], ['name' => 'ASC']);

        return $this->render('front/produtos/index.html.twig', [
            "specialties" => $specialty,
            "products" => $products,
            'type' => 'com-prescricao',
            'letra' => null,
        ]);
    }

    /**
     * @Route("/produtos/isentos", name="isentos")
     */
    public function isentos(Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $specialty = $em->getRepository('App:Specialty')->findAll();
        $products = $em->getRepository('App:Products')->findBy(['product_type' => 'isento'], ['name' => 'ASC']);

        return $this->render('front/produtos/index.html.twig', [
            "specialties" => $specialty,
            "products" => $products,
            'type' => 'isento',
            'letra' => null,
        ]);
    }

    /**
     * @Route("/produtos/lancamentos", name="lancamentos")
     */
    public function lancamentos(Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }
        
        $em = $this->getDoctrine()->getManager();
        $specialty = $em->getRepository('App:Specialty')->findAll();
        $products = $em->getRepository('App:Products')->findBy(['is_lancamento' => true], ['name' => 'ASC']);

        return $this->render('front/produtos/index.html.twig', [
            "specialties" => $specialty,
            "products" => $products,
            'type' => 'lancamento',
            'letra' => null,
        ]);
    }

    /**
     * @Route("/produtos/todos", name="todos")
     */
    public function todos(Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $specialty = $em->getRepository('App:Specialty')->findAll();
        $products = $em->getRepository('App:Products')->findBy([], ['name' => 'ASC']);

        return $this->render('front/produtos/index.html.twig', [
            "specialties" => $specialty,
            "products" => $products,
            'type' => 'todos',
            'letra' => null,
        ]);
    }

    /**
    * @Route("/produtos/letra/{letra} ", name="letra", requirements={"letra"="[A-Z]"})
    */  
    public function letra($letra, Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $specialty = $em->getRepository('App:Specialty')->findAll();
     
        $products = $em->getRepository('App:Products')->createQueryBuilder('p')
                        ->where('p.name like :letter OR p.descriptive like :letter')
                        ->setParameter('letter', "{$letra}%")
                        ->orderBy('p.name', 'ASC')
                        ->getQuery()
                        ->getResult();
     
        return $this->render('front/produtos/index.html.twig', [
            "specialties" => $specialty,
            "products" => $products,
            'type' => 'letra',
            'letra' => $letra,
        ]);
    }

    /**
     * @Route("/produtos/categoria/{categoria} ", name="produto_categoria")
     */
    public function produto_categoria($categoria, Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $specialty = $em->getRepository('App:Specialty')->findAll();
        $products = $em->getRepository('App:Products')->findAllByCategoria($categoria, ['created_at' => 'DESC']);

        return $this->render('front/produtos/index.html.twig', [
            "specialties" => $specialty,
            "products" => $products,
            'type' => 'categoria',
            'letra' => null,
        ]);
    }

    /**
     * @Route("/produtos/especialidade/{especialidade} ", name="produto_especialidade")
     */
    public function produto_especialidade($especialidade, Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $specialty = $em->getRepository('App:Specialty')->findOneBy(['id' => $especialidade], []);
        $products = $em->getRepository('App:Products')->findBy(['specialty' => $specialty], ['name' => 'ASC']);

        $specialties = $em->getRepository('App:Specialty')->findAll();
        
        return $this->render('front/produtos/index.html.twig', [
            "specialties" => $specialties,
            "products" => $products,
            'type' => 'categoria',
            'letra' => null,
        ]);
    }

    /**
     * @Route("/produto/{slug}", name="produto")
     */
    public function produto(Products $product, $slug, Request $request): Response
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('q')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        if (!$product) {
            return $this->redirectToRoute('produtos'); 
        }

        $em = $this->getDoctrine()->getManager();
        $specialties = $em->getRepository('App:Specialty')->findAll();
        $categorias = $em->getRepository('App:Products')->findProductCategory($product->getSlug(), ['created_at' => 'DESC']);
        $notices = $em->getRepository('App:Notice')->findBy(['is_active' => true], ['created_at' => 'DESC'], 4);
        $flyers = $em->getRepository('App:Flyer')->findByProduct($product->getId(), ['created_at' => 'DESC'], 4);
        $scientific_studies = $em->getRepository('App:ScientificStudies')->findBy(['product' => $product]);
        // dump($scientific_studies);
        // die();
        return $this->render('front/produtos/produto.html.twig', [
            'controller_name' => 'ProdutosController',
            "flyers" => $flyers,
            "specialties" => $specialties,
            'product' => $product,
            'categorias' => $categorias,
            'type' => 'produto',
            'notices' => $notices,
            'scientific_studies' => $scientific_studies
        ]);
    }


    /**
     * @Route("/produto/busca/{slug} ", name="produto_busca")
     */
    public function produto_busca($slug, Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('App:Products')->findBySlug($slug, [], ['name' => 'ASC']);
        $product = $product[0];
        $categorias = $em->getRepository('App:Products')->findProductCategory($slug, [], ['name' => 'ASC']);
        $products_list = $em->getRepository('App:Products')->findList($slug, ['created_at' => 'DESC'], 4);

        return $this->render('front/produtos/produto.html.twig', [
            'controller_name' => 'ProdutosController',
            'categorias' => $categorias,
            'product' => $product,
            'type' => 'busca',
            'products_list' => $products_list
        ]);
    }
}