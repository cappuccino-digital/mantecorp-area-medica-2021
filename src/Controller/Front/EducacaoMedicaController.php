<?php

namespace App\Controller\Front;


use App\Entity\Course;

use App\Service\EadBoxService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;


class EducacaoMedicaController extends AbstractController
{
    /**
     * @Route("/educacao-medica", name="educacao-medica")
     */
    public function index(Request $request, EadBoxService $ead_box_service): Response
    {
        //validação para evitar roubo de dados confidenciais 
        if ($request->query->has('q') || $request->query->has('s')) {
            throw $this->createNotFoundException('A página não foi encontrada');
        }

        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $q = $em->getRepository('App:Course')->createQueryBuilder('c');
        
        if ($request->request->has('e')) {
            if ($request->request->get('s')) {
                $specialty = $em->getRepository('App:Specialty')->findOneBy(['id' => $request->request->get('s')]);

                             $q->where('c.specialty = :specialty')
                                ->setParameter('specialty', $specialty);
            }

            if ($request->request->get('t')) {
                $education = $em->getRepository('App:Education')->findOneBy(['id' => $request->request->get('t')]);
               
                            $q->andWhere('c.type = :education')
                                ->setParameter('education', $education);
            }

            if (trim($request->request->get('q'))) {
                    $q->andWhere('c.name LIKE :search or c.resume LIKE :search')
                    ->setParameter('search', '%' .$request->request->get('q').'%');
            }
        } else {
            $user_ead = $em->getRepository('App:UserEad')->findOneBy(['email' => $user->getEmailEadbox()]);
        
            if ($user_ead) { 
                if ($user_ead->getPassword() != $user->getPassword()) {
                    //EadBox update da senha
                    $ead_box_service->updateUser($user);
                }
            }
        }

        $courses = $q->getQuery()->getResult();
    
        $specialty = $em->getRepository('App:Specialty')->findAll();
        $educations = $em->getRepository('App:Education')->findAll();

        
        return $this->render('front/educacao-medica/index.html.twig', [
            'controller_name' => 'EducacaoMedicaController',
            'courses' => $courses,
            'specialties' => $specialty,
            'educations' => $educations,
            'filtro' => $request->request->has('e') ? true : false
        ]);
    }

    /**
     * @Route("/educacao-medica/busca", name="educacao_medica_filtro_especialidade")
     */
    public function buscaPorEspecialidade(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $courses = $em->getRepository('App:Course')->findBySpecialty($request->request->get("s"));

        return new JsonResponse([
            'response' => $courses,
        ]);
    }

    /**
     * @Route("/educacao-medica/eadbox", name="educacao-medica-eadbox")
     */
    public function eadbox(Request $request, EadBoxService $ead_box_service): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $user = $this->getUser();

        $access_eadbox = $ead_box_service->accessEadbox($user);
       
        if ($access_eadbox['status']) {
            $url = $access_eadbox['url'];
            return $this->redirect($url);
        }

        return $this->redirectToRoute('educacao-medica');
    }
}