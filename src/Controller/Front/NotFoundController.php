<?php

namespace App\Controller\Front;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NotFoundController extends AbstractController
{
    /**
     * @Route("/404", name="404")
     */
    public function show(): Response
    {
        return $this->render('front/404/index.html.twig', [
            'controller_name' => 'NotFoundController',
        ]);
    }
}
