<?php

namespace App\Controller\Front;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;

class ResidentesController extends AbstractController
{
    /**
     * @Route("/residentes", name="residentes")
     */
    public function index(Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $c = $em->getRepository('App:Course')->createQueryBuilder('c')
                ->where('c.is_resident = true');

        $m = $em->getRepository('App:Mementos')->createQueryBuilder('m')
                ->join('m.specialty', 's');
               
        if ($request->request->has('e')) {
            if ($request->request->get('s')) {
                $specialty = $em->getRepository('App:Specialty')->findOneBy(['id' => $request->request->get('s')]);
                
                //cursos especialidades
                $c->andWhere('c.specialty = :specialty')
                    ->setParameter('specialty', $specialty);

                //residentes especialidades
                $m->where('s.id = :specialty')
                    ->setParameter('specialty', $specialty);
            }
        }

        $courses = $c->getQuery()->getResult();
        $mementos = $m->getQuery()->getResult();

        //$mementos = $em->getRepository('App:Mementos')->findAll();
        $specialties = $em->getRepository('App:Specialty')->findAll();

        return $this->render('front/residentes/index.html.twig', [
            'controller_name' => 'ResidentesController',
            "mementos" => $mementos,
            'courses' => $courses,
            'specialties' => $specialties,
        ]);
    }
}