<?php

namespace App\Controller\Front;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Cookie;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home") 
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $lastOnes = $em->getRepository('App:Notice')->findBy([], ['created_at' => 'DESC'], 3);
        $count = count($lastOnes);

        for ($i = 0; $i < $count; $i++) {

            $description = $lastOnes[$i]->getDescription();
            $description = substr_replace($description, '', 500, -1);
            $lastOnes[$i]->setDescription($description);
        }

        $studies = $em->getRepository('App:ScientificStudies')->findBy([], ['created_at' => 'DESC'], 3);

        return $this->render('front/home/index.html.twig', [
            "lastOnes" => $lastOnes,
            'studies' => $studies
        ]);
    }


    /**
     * @Route("/busca", name="home_search")
     */
    public function search(PaginatorInterface $paginator, Request $request)
    {
        $searchs = [];
        $em = $this->getDoctrine()->getManager();

        if ($request->query->has('s')) {
            $q = $em->getRepository('App:Notice')->findByNoticesAndProducts($request->query->get('s'));

            if (count($q) > 0) {
                foreach ($q as $key => $n) {
                    $searchs[] = [
                        'id' => $n['id'],
                        'title' => $n['title'],
                        'image' => $n['image'],
                        'slug' => $n['slug'],
                        'is_type' => $n['is_type'],
                        'description' => substr_replace($n['description'], '', 500, -1)
                    ];
                }
            }

            $searchs = $paginator->paginate(
                $searchs,
                $request->query->getInt('page', 1),
                4
            );
        }

        return $this->render('front/home/busca.html.twig', [
            'searchs' => $searchs,
        ]);
    }

    /**
     * @Route("/termos-de-uso", name="home_termos")
     */
    public function terms()
    {
        return $this->render('front/home/termos.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

     /**
     * @Route("/politica-de-privacidade", name="home_privacidade")
     */
    public function privacy()
    {
        return $this->render('front/home/politica.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

}