<?php

namespace App\Controller\Front;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class SiteMapController extends AbstractController
{

    function __construct()
    {
        $this->letra = "A";
    }

    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function index(Request $request)
    {


        $router = $this->get('router')->getRouteCollection()->all();
        $routes = [];
        $changefreq = [];

        $em = $this->getDoctrine();


        $noticias =  $em->getRepository('App:Notice')->findBy([], ['created_at' => 'DESC']);
        $produtos = $this->getDoctrine()
            ->getRepository('App:Products')
            ->findBy(['is_active' => true]);

        $produto_especialidade = $this->getDoctrine()
            ->getRepository('App:Specialty');

        $noticia_categoria = $em->getRepository('App:NewsType')->findAll();

        $i = 0;
        foreach ($router as $id => $route) {

            if (substr($id, 0, 1) != '_') {
                if (str_contains($id, "admin") == false) {
                    if (
                        str_contains($route->getPath(), "produto")
                    ) {
                        if (str_contains($route->getPath(), "{letra}")) {
                            for ($letra = 65; $letra <= 90; $letra++) {
                                $routes[$i] = array(
                                    "router"  => $this->validaSlug($route->getPath(), "", $letra),
                                    "changefreq" => "weekly"
                                );
                                $i++;
                            }
                        } else if (str_contains($route->getPath(), "/produto/{slug}")) {
                            foreach ($produtos as $produto) {
                                $routes[$i] = array(
                                    "router"  =>
                                    $this->validaSlug($route->getPath(), $produto, ""),
                                    "changefreq" => "monthly"
                                );
                                $i++;
                            }
                        } else if (str_contains($route->getPath(), "{categoria}") || str_contains($route->getPath(), "{especialidade}")) {
                            foreach ($produto_especialidade as $categoria) {
                                $routes[$i] = array(
                                    "router"  =>
                                    $this->validaSlug($route->getPath(), $categoria, ""),
                                    "changefreq" => "daily"
                                );
                                $i++;
                            }
                        }
                    } else  if (
                        str_contains($route->getPath(), "noticias")
                    ) {

                        if (str_contains($route->getPath(), "categoria/{slug}")) {

                            foreach ($noticia_categoria as $categoria) {

                                $routes[$i] = array(
                                    "router"  => $this->validaSlug($route->getPath(), $categoria, ""),
                                    "changefreq" => "daily"
                                );
                                $i++;
                            }
                        } else if (str_contains($route->getPath(), "noticias/{slug}")) {
                            foreach ($noticias as $noticia) {
                                $routes[$i] = array(
                                    "router"  => $this->validaSlug($route->getPath(), $noticia, ""),
                                    "changefreq" => "monthly"
                                );
                                $i++;
                            }
                        } else {
                            $routes[$i] = array(
                                "router"  => $route->getPath(),
                                "changefreq" => "daily"
                            );
                            $i++;
                        }
                    } else if (
                        str_contains($route->getPath(), "politica-de-privacidade")
                        || str_contains($route->getPath(), "termos-de-uso")
                        || str_contains($route->getPath(), "/login")
                        || str_contains($route->getPath(), "/logout")
                    ) {
                    } else if (
                        str_contains($route->getPath(), "404")
                        || str_contains($route->getPath(), "quem-somos")
                        || str_contains($route->getPath(), "contato")
                        || str_contains($route->getPath(), "problemas-na-navegacao")
                    ) {
                        $routes[$i] = array(
                            "router"  => $route->getPath(),
                            "changefreq" => "monthly"
                        );
                        $i++;
                    } else {
                        $routes[$i] = array(
                            "router"  => $route->getPath(),
                            "changefreq" => "daily"
                        );
                        $i++;
                    }
                }
            }
        }
        return $this->render('front/config/sitemap.xml.twig', [
            'routes' => $routes
        ]);
    }


    public function validaSlug($path, $obj, $letra)
    {

        switch ($path) {
            case (str_contains($path, "{slug}")):
                return $new_path = str_replace("{slug}", $obj->getSlug(), $path);

            case (str_contains($path, "{letra}")):

                $new_path = str_replace("{letra}", chr($letra), $path);

                return $new_path;

            case (str_contains($path, "{especialidade}")):
                return $new_path = str_replace("{especialidade}", $obj->getSlug(), $path);

            case (str_contains($path, "{categoria}")):
                return $new_path = str_replace("{categoria}", $obj->getSlug(), $path);
        }
    }
}