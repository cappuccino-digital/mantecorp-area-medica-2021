<?php

namespace App\Controller\Front;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;

class FavoritosController extends AbstractController
{
    /**
     * @Route("/favoritos", name="favorites")
     */
    public function index(Request $request): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
       
        $favorites = $em->getRepository('App:UserFavorite')->getUserFavoriteContent($user->getId());
      
        return $this->render('front/favoritos/index.html.twig', [
            'favorites' => $favorites,
        ]);
    }
}
