<?php

namespace App\Controller\Front;

use App\Entity\UserEvents;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGenerator;

class EventosController extends AbstractController
{
    /**
     * @Route("/eventos/{slug}", name="eventos", defaults={"slug" = null})
     */
    public function index(Request $request, $slug): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
        $q = $em->getRepository('App:Events')->createQueryBuilder('e')
                ->where('e.isActive = true');
        
        if ($request->request->has('e')) {
            if ($request->request->get('s')) {
                $specialty = $em->getRepository('App:Specialty')->findOneBy(['id' => $request->request->get('s')]);

                $q->join('e.specialty', 's')
                    ->andWhere('s.id = :specialty')
                    ->setParameter('specialty', $specialty);
            }
        }

        if ($slug) {
            $event = $em->getRepository('App:Events')->findOneBy(['isActive' => true, 'slug' => $slug]);
        } else {
            $event = $em->getRepository('App:Events')->findOneBy(['isActive' => true], ['created_at' => 'DESC'], 1);
        }

        $events = $q->getQuery()->getResult();
        $specialties = $em->getRepository('App:Specialty')->findAll();
        
        return $this->render('front/eventos/index.html.twig', [
            'event' => $event,
            'events' => $events,
            'specialties' => $specialties,
            'filtro' => $request->request->get('s')
        ]);
    }

    /**
     * @Route("/evento/inscricao", name="event-subscription")
     */
    public function subscription(Request $request, MailerInterface $mailer): Response
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            /** Gravando a referencia do conteúdo */
            $url_reference = $request->getUri();
            $response = $this->redirectToRoute('login');
            $response->headers->setCookie(new Cookie('_referenceURL', $url_reference));
            return $response;
            /*** END  */
        }

        $em = $this->getDoctrine()->getManager();
               
        if ($request->request->get('ev')) {
            $event = $em->getRepository('App:Events')->findOneBy(['id' => $request->request->get('ev')]);

            $user = $this->getUser();
            $emails = explode(";", $event->getEmail());

            //send e-mail
            $sendEmail = (new Email())
                ->from(new Address('portalmantecorpfarmasa@gmail.com', 'Mantecorp Farmasa - Área médica'));
                //E-mails cadastrados para receber as inscrições do evento
                foreach ($emails as $key => $email) {
                    if ($key == 0) {
                        $sendEmail->to($email);
                    } else {
                        $sendEmail->addTo($email);
                    }
                }

                $sendEmail->priority(Email::PRIORITY_HIGH)
                    ->subject('Mantecorp Farmasa - Inscrição de Evento')
                    ->html("
                        <strong>Nome:</strong> ".$user->getName()." <br>
                        <strong>E-mail:</strong>".$user->getEmail()." <br>
                        <strong>CRF:</strong> ".$user->getUfCrm()." <br>
                        <strong>Estado:</strong> ".$user->getUfCrm()." <br>
                        <strong>Celular:</strong> ".$user->getCellphone()." <br>
                        <strong>Evento:</strong> ".$event->getName()." <br>
                        <strong>Data e horário:</strong> ".$event->getDatetimeend()->format('d/m/Y H:i')." <br>
                    ");

            $mailer->send($sendEmail);
            
            $user_event = $em->getRepository('App:UserEvents')->findOneBy(['id' => $request->query->get('ev')]);
            if (!$user_event) {
                $user_event = new UserEvents();
                $user_event->setCreatedAt(new \DateTime());
                $user_event->setTypeSubscription('email');
            }
            
            $user_event->setUser($user);
            $user_event->setEvent($event);
            $user_event->setUpdatedAt(new \DateTime());

            $em->persist($user_event);
            $em->flush();

            return new JsonResponse([
                'status' => true, 
                'message' => 'Inscrição realizada com sucesso'
            ]);
        } 

        return $this->redirectToRoute('eventos');
    }
}
