<?php

namespace App\Controller\Front;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Contact;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

class ContatoController extends AbstractController
{
    /**
     * @Route("/contato", name="contato")
     */
    public function index(): Response
    {
        return $this->render('front/contato/index.html.twig', [
            'controller_name' => 'ContatoController',
        ]);
    }


    /**
     * @Route("/problemas-na-navegacao", name="contato_problemas")
     */
    public function contatoProblemas(Request $request, MailerInterface $mailer)
    {
        $message = null;

        if ($request->isMethod('get')) {
            return $this->render('front/contato/problemas.html.twig', [
                'status' => false,
                'message' => $message
            ]);
        }
      
        if ($request->request->get('contact')) {
            $contact = $request->request->get('contact');
            $name = isset($contact['name']) ? $contact['name'] : null;
            $ddd = isset($contact['ddd']) ? $contact['ddd'] : null;
            $phone = isset($contact['phone']) ? $contact['phone'] : null;
            $email = isset($contact['email']) ? $contact['email'] : null;
            $message = isset($contact['message']) ? $contact['message'] : null;
            
            $errors = [];

            if (!$name) {
                $errors['name'] = 'Nome é obrigatório';
            }

            if (!$ddd) {
                $errors['ddd'] = 'DDD é obrigatório';
            } else {
                // if (strlen($ddd) < 2) { //contando com as maskaras
                //     $errors['ddd'] = "Celular inválido";
                // }
            }

            if (!$phone) {
                $errors['phone'] = 'Celular é obrigatório';
            } else {
               // if ($phone) {
                                  
                    // if (strlen($phone) < 9) { //contando com as maskaras
                    //     $errors['cellphone'] = "Celular inválido";
                    // }
                //}  
            }

            if (!$email) {
                $errors['email'] = 'E-mail é obrigatório';
            }

            if (!$name) {
                $errors['message'] = 'Comentario é obrigatório';
            }

            $secret = '6LcgV24eAAAAAJUL9GHzfE6zTa1cl-cfR-HoUtO1';  // check if reCaptcha has been validated by Google     
            $captchaId = $request->request->get('g-recaptcha-response'); /** REcaptcha */
            $responseCaptcha = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$captchaId)); //send recaptcha
            
            if ($responseCaptcha->success == true) {
                         
            } else {
               $errors['recaptcha'] = "Selecione o recaptcha é obrigatório";
            }

            //dump($errors); die();


            if (count($errors) > 0) {
                return $this->render('front/contato/problemas.html.twig', [
                    'status' => false,
                    'errors' => $errors,
                    'message' => 'Preencha os campos obrigatórios'
                ]);
            }
            
            $c = new Contact();

            $c->setName($contact['name']);
            $c->setDdd($contact['ddd']);
            $c->setPhone($contact['phone']);
            $c->setEmail($contact['email']);
            $c->setMessage($contact['message']);
            $c->setCreatedAt(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($c);
            $em->flush();

            $response = $this->sendContact($c, $mailer, 'suporteportal@mantecorp.com.br', 'front/component/email.html.twig');
            
            if ($response) {
                $message = 'Obrigado, seu contato foi enviado com sucesso. Em breve enviaremos uma resposta.';
            }

            return $this->render('front/contato/problemas.html.twig', [
                'status' => true,
                'message' => $message
            ]);
        }
        
    }

    private function sendContact($contact, $mailer, $to,  $render)
    {


        $email = (new Email())
            ->from(new Address('portalmantecorpfarmasa@gmail.com', 'Contato Mantecorp Farmasa'))
            ->to($to)
            ->addBcc('ana.morales@cappuccinodigital.com.br')
            //->replyTo($contact->getEmail())
            ->priority(Email::PRIORITY_HIGH)
            ->subject('')
            ->html($this->renderView(
                $render,
                ['contact' => $contact]
            ));

        $mailer->send($email);
        return true;
    }
}