<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ErrorController extends AbstractController
{

    /**
     * @Route("/_error/{statusCode}", name="error")
     */
    public function show(Request $request)
    {
        $handleError = $request->get("exception");

        $code = $handleError->getCode();
        $message = $handleError->getMessage();

        if (str_contains($message, "No route found for ")) {
            $code = 404;
        }
        return $this->render('error/error' . $code . '.html.twig', []);
    }
}