<?php

namespace App\Entity;

use App\Repository\UserAvaliationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserAvaliationRepository::class)
 */
class UserAvaliation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userAvaliations")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=ScientificStudies::class, inversedBy="userAvaliations")
     */
    private $scientific_studies;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $avaliation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getScientificStudies(): ?ScientificStudies
    {
        return $this->scientific_studies;
    }

    public function setScientificStudies(?ScientificStudies $scientific_studies): self
    {
        $this->scientific_studies = $scientific_studies;

        return $this;
    }

    public function getAvaliation(): ?string
    {
        return $this->avaliation;
    }

    public function setAvaliation(?string $avaliation): self
    {
        $this->avaliation = $avaliation;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
