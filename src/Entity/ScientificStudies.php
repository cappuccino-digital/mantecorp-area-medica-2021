<?php

namespace App\Entity;

use App\Repository\ScientificStudiesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ScientificStudiesRepository::class)
 */
class ScientificStudies
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $resume;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageweb;

    /**
     * @ORM\ManyToOne(targetEntity=Specialty::class, inversedBy="scientificStudies")
     */
    private $specialty;

    /**
     * @ORM\ManyToOne(targetEntity=Products::class, inversedBy="scientificStudies")
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity=UserAvaliation::class, mappedBy="scientific_studies")
     */
    private $userAvaliations;

    public function __construct()
    {
        $this->userAvaliations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }


    public function getImageweb(): ?string
    {
        return $this->imageweb;
    }

    public function setImageweb(?string $imageweb): self
    {
        $this->imageweb = $imageweb;

        return $this;
    }

    public function getSpecialty(): ?Specialty
    {
        return $this->specialty;
    }

    public function setSpecialty(?Specialty $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }

    public function getProduct(): ?Products
    {
        return $this->product;
    }

    public function setProduct(?Products $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|UserAvaliation[]
     */
    public function getUserAvaliations(): Collection
    {
        return $this->userAvaliations;
    }

    public function addUserAvaliation(UserAvaliation $userAvaliation): self
    {
        if (!$this->userAvaliations->contains($userAvaliation)) {
            $this->userAvaliations[] = $userAvaliation;
            $userAvaliation->setScientificStudies($this);
        }

        return $this;
    }

    public function removeUserAvaliation(UserAvaliation $userAvaliation): self
    {
        if ($this->userAvaliations->removeElement($userAvaliation)) {
            // set the owning side to null (unless already changed)
            if ($userAvaliation->getScientificStudies() === $this) {
                $userAvaliation->setScientificStudies(null);
            }
        }

        return $this;
    }
}