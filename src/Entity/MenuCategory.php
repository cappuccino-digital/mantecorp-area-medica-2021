<?php

namespace App\Entity;

use App\Repository\MenuCategoryRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass=MenuCategoryRepository::class)
 */
class MenuCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $icon;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_active;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_caterer = false;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Menu", mappedBy="menu_category")
     */
    private $menus;

    /**
     * @ORM\OneToMany(targetEntity=ProfileAcess::class, mappedBy="menu")
     */
    private $profileAcesses;

    public function __construct()
    {
        $this->menus = new ArrayCollection();
        $this->profileAcesses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getIsCaterer(): ?bool
    {
        return $this->is_caterer;
    }

    public function setIsCaterer(bool $is_caterer): self
    {
        $this->is_caterer = $is_caterer;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getMenus(): Collection
    {
        return $this->menus;
    }

    /**
     * @return Collection|ProfileAcess[]
     */
    public function getProfileAcesses(): Collection
    {
        return $this->profileAcesses;
    }

    public function addProfileAcess(ProfileAcess $profileAcess): self
    {
        if (!$this->profileAcesses->contains($profileAcess)) {
            $this->profileAcesses[] = $profileAcess;
            $profileAcess->setMenu($this);
        }

        return $this;
    }

    public function removeProfileAcess(ProfileAcess $profileAcess): self
    {
        if ($this->profileAcesses->contains($profileAcess)) {
            $this->profileAcesses->removeElement($profileAcess);
            // set the owning side to null (unless already changed)
            if ($profileAcess->getMenu() === $this) {
                $profileAcess->setMenu(null);
            }
        }

        return $this;
    }
}
