<?php

namespace App\Entity;

use App\Repository\UserTemporaryRegisterStepRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserTemporaryRegisterStepRepository::class)
 */
class UserTemporaryRegisterStep
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=UserTemporary::class, inversedBy="userTemporaryRegisterSteps")
     */
    private $user_temporary;

    /**
     * @ORM\ManyToOne(targetEntity=RegisterStep::class, inversedBy="userTemporaryRegisterSteps")
     */
    private $register_step;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserTemporary(): ?UserTemporary
    {
        return $this->user_temporary;
    }

    public function setUserTemporary(?UserTemporary $user_temporary): self
    {
        $this->user_temporary = $user_temporary;

        return $this;
    }

    public function getRegisterStep(): ?RegisterStep
    {
        return $this->register_step;
    }

    public function setRegisterStep(?RegisterStep $register_step): self
    {
        $this->register_step = $register_step;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
