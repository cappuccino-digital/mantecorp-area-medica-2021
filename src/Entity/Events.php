<?php

namespace App\Entity;

use App\Repository\EventsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EventsRepository::class)
 */
class Events
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetimestart;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetimeend;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $local;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $relatedspecialties;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageweb;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagemobile;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $update_at;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $registration_type;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\ManyToMany(targetEntity=Speakers::class, inversedBy="events")
     */
    private $speakers;

    /**
     * @ORM\ManyToMany(targetEntity=Specialty::class, inversedBy="events")
     */
    private $specialty;

    /**
     * @ORM\OneToMany(targetEntity=UserEvents::class, mappedBy="event")
     */
    private $userEvents;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    public function __construct()
    {
        $this->speakers = new ArrayCollection();
        $this->specialty = new ArrayCollection();
        $this->userEvents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDatetimestart(): ?\DateTimeInterface
    {
        return $this->datetimestart;
    }

    public function setDatetimestart(\DateTimeInterface $datetimestart): self
    {
        $this->datetimestart = $datetimestart;

        return $this;
    }

    public function getDatetimeend(): ?\DateTimeInterface
    {
        return $this->datetimeend;
    }

    public function setDatetimeend(\DateTimeInterface $datetimeend): self
    {
        $this->datetimeend = $datetimeend;

        return $this;
    }

    public function getLocal(): ?string
    {
        return $this->local;
    }

    public function setLocal(?string $local): self
    {
        $this->local = $local;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRelatedspecialties(): ?string
    {
        return $this->relatedspecialties;
    }

    public function setRelatedspecialties(?string $relatedspecialties): self
    {
        $this->relatedspecialties = $relatedspecialties;

        return $this;
    }

    public function getImageweb(): ?string
    {
        return $this->imageweb;
    }

    public function setImageweb(?string $imageweb): self
    {
        $this->imageweb = $imageweb;

        return $this;
    }

    public function getImagemobile(): ?string
    {
        return $this->imagemobile;
    }

    public function setImagemobile(?string $imagemobile): self
    {
        $this->imagemobile = $imagemobile;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->update_at;
    }

    public function setUpdateAt(\DateTimeImmutable $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }


    public function getRegistrationtype(): ?string
    {
        return $this->registration_type;
    }

    public function setRegistrationtype(?string $registration_type): self
    {
        $this->registration_type = $registration_type;

        return $this;
    }


    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|Speakers[]
     */
    public function getSpeakers(): Collection
    {
        return $this->speakers;
    }

    public function addSpeaker(Speakers $speaker): self
    {
        if (!$this->speakers->contains($speaker)) {
            $this->speakers[] = $speaker;
        }

        return $this;
    }

    public function removeSpeaker(Speakers $speaker): self
    {
        $this->speakers->removeElement($speaker);

        return $this;
    }

    /**
     * @return Collection|Specialty[]
     */
    public function getSpecialty(): Collection
    {
        return $this->specialty;
    }

    public function addSpecialty(Specialty $specialty): self
    {
        if (!$this->specialty->contains($specialty)) {
            $this->specialty[] = $specialty;
        }

        return $this;
    }

    public function removeSpecialty(Specialty $specialty): self
    {
        $this->specialty->removeElement($specialty);

        return $this;
    }

    /**
     * @return Collection|UserEvents[]
     */
    public function getUserEvents(): Collection
    {
        return $this->userEvents;
    }

    public function addUserEvent(UserEvents $userEvent): self
    {
        if (!$this->userEvents->contains($userEvent)) {
            $this->userEvents[] = $userEvent;
            $userEvent->setEvent($this);
        }

        return $this;
    }

    public function removeUserEvent(UserEvents $userEvent): self
    {
        if ($this->userEvents->removeElement($userEvent)) {
            // set the owning side to null (unless already changed)
            if ($userEvent->getEvent() === $this) {
                $userEvent->setEvent(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
