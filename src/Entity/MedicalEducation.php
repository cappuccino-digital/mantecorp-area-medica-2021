<?php

namespace App\Entity;

use App\Repository\MedicalEducationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MedicalEducationRepository::class)
 */
class MedicalEducation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imageweb;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imagemobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specialty;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImageweb(): ?string
    {
        return $this->imageweb;
    }

    public function setImageweb(string $imageweb): self
    {
        $this->imageweb = $imageweb;

        return $this;
    }

    public function getImagemobile(): ?string
    {
        return $this->imagemobile;
    }

    public function setImagemobile(string $imagemobile): self
    {
        $this->imagemobile = $imagemobile;

        return $this;
    }

    public function getSpecialty(): ?Specialty
    {
        return $this->specialty;
    }

    public function setSpecialty(?Specialty $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }
}
