<?php

namespace App\Entity;

use App\Repository\YourPatientRepository;
use App\Entity\Products;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass=YourPatientRepository::class)
 */
class YourPatient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255 ,nullable=true)
     */
    private $title_v;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $resume_v;

    /**
     * @ORM\Column(type="string", length=1000,nullable=true)
     */
    private $video;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $title_m;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $resume_m;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $file;



    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $materialtype;

    /**
     * @ORM\ManyToMany(targetEntity=Specialty::class, inversedBy="yourPatients")
     */
    private $specialty;

    /**
     * @ORM\ManyToOne(targetEntity=Products::class, inversedBy="yourPatients")
     */
    private $products;

    public function __construct()
    {
        $this->specialty = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleV(): ?string
    {
        return $this->title_v;
    }

    public function setTitleV(string $title_v = null): self
    {
        $this->title_v = $title_v;

        return $this;
    }

    public function getResumeV(): ?string
    {
        return $this->resume_v;
    }

    public function setResumeV(string $resume_v = null): self
    {
        $this->resume_v = $resume_v;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(string $video = null): self
    {
        $this->video = $video;

        return $this;
    }

    public function getTitleM(): ?string
    {
        return $this->title_m;
    }

    public function setTitleM(string $title_m = null): self
    {
        $this->title_m = $title_m;

        return $this;
    }

    public function getResumeM(): ?string
    {
        return $this->resume_m;
    }

    public function setResumeM(string $resume_m = null): self
    {
        $this->resume_m = $resume_m;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file = null): self
    {
        $this->file = $file;

        return $this;
    }


    public function getMaterialtype(): ?string
    {
        return $this->materialtype;
    }

    public function setMaterialtype(?string $materialtype): self
    {
        $this->materialtype = $materialtype;

        return $this;
    }

    /**
     * @return Collection|Specialty[]
     */
    public function getSpecialty(): Collection
    {
        return $this->specialty;
    }

    public function addSpecialty(Specialty $specialty): self
    {
        if (!$this->specialty->contains($specialty)) {
            $this->specialty[] = $specialty;
        }

        return $this;
    }

    public function removeSpecialty(Specialty $specialty): self
    {
        $this->specialty->removeElement($specialty);

        return $this;
    }

    public function getProducts(): ?Products
    {
        return $this->products;
    }

    public function setProducts(?Products $products): self
    {
        $this->products = $products;

        return $this;
    }
}