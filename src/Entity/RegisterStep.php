<?php

namespace App\Entity;

use App\Repository\RegisterStepRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RegisterStepRepository::class)
 */
class RegisterStep
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_active;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_form_register;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity=UserTemporaryRegisterStep::class, mappedBy="register_step")
     */
    private $userTemporaryRegisterSteps;

    public function __construct()
    {
        $this->userTemporaryRegisterSteps = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getIsFormRegister(): ?bool
    {
        return $this->is_form_register;
    }

    public function setIsFormRegister(?bool $is_form_register): self
    {
        $this->is_form_register = $is_form_register;

        return $this;
    }


    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection|UserTemporaryRegisterStep[]
     */
    public function getUserTemporaryRegisterSteps(): Collection
    {
        return $this->userTemporaryRegisterSteps;
    }

    public function addUserTemporaryRegisterStep(UserTemporaryRegisterStep $userTemporaryRegisterStep): self
    {
        if (!$this->userTemporaryRegisterSteps->contains($userTemporaryRegisterStep)) {
            $this->userTemporaryRegisterSteps[] = $userTemporaryRegisterStep;
            $userTemporaryRegisterStep->setRegisterStep($this);
        }

        return $this;
    }

    public function removeUserTemporaryRegisterStep(UserTemporaryRegisterStep $userTemporaryRegisterStep): self
    {
        if ($this->userTemporaryRegisterSteps->removeElement($userTemporaryRegisterStep)) {
            // set the owning side to null (unless already changed)
            if ($userTemporaryRegisterStep->getRegisterStep() === $this) {
                $userTemporaryRegisterStep->setRegisterStep(null);
            }
        }

        return $this;
    }
}
