<?php

namespace App\Entity;

use App\Repository\CourseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CourseRepository::class)
 */
class Course
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageweb;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagemobile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $resume;

    /**
     * @ORM\ManyToOne(targetEntity=Specialty::class, inversedBy="course")
     */
    private $specialty;

    /**
     * @ORM\ManyToOne(targetEntity=Education::class, inversedBy="course")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_resident;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImageweb(): ?string
    {
        return $this->imageweb;
    }

    public function setImageweb(?string $imageweb): self
    {
        $this->imageweb = $imageweb;

        return $this;
    }

    public function getImagemobile(): ?string
    {
        return $this->imagemobile;
    }

    public function setImagemobile(?string $imagemobile): self
    {
        $this->imagemobile = $imagemobile;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getSpecialty(): ?Specialty
    {
        return $this->specialty;
    }

    public function setSpecialty(?Specialty $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }

    public function getType(): ?Education
    {
        return $this->type;
    }

    public function setType(?Education $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getIsResident(): ?bool
    {
        return $this->is_resident;
    }

    public function setIsResident(?bool $is_resident): self
    {
        $this->is_resident = $is_resident;

        return $this;
    }
}