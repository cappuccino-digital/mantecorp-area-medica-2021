<?php

namespace App\Entity;

use App\Repository\ProductsRepository;
use App\Repository\ProductCategoryRepository;
use App\Repository\StoreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductsRepository::class)
 */
class Products
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=ProductCategory::class, inversedBy="products" , cascade={"persist"})
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=Store::class, inversedBy="products" , cascade={"persist"})
     */
    private $store;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $descriptive;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image_title;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_active;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_lancamento;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    private $presentation;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    private $activeIngredient;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $recommendation;

    /**
     * @ORM\ManyToOne(targetEntity=ScientificStudies::class, inversedBy="products")
     */
    private $scientificstudies;

    /**
     * @ORM\ManyToMany(targetEntity=Specialty::class, inversedBy="products")
     */
    private $specialty;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $product_type;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $bula;

    /** 
     * @ORM\Column(type="string", length=500,  nullable=true)
     */
    private $find;

    /** 
     * @ORM\Column(type="string", length=500,  nullable=true)
     */
    private $knowmore;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    public function __construct()
    {

        $this->category = new ArrayCollection();

        $this->specialty = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescriptive(): ?string
    {
        return $this->descriptive;
    }

    public function setDescriptive(?string $descriptive): self
    {
        $this->descriptive = $descriptive;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory()
    {

        return  $this->category;
    }

    public function setCategory($category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getStore(): ?Store
    {

        return  $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    public function addType(?ProductCategory $category): self
    {
        $this->category[] = $category;

        return $this;
    }

    public function getSpecialty()
    {
        return $this->specialty;
    }

    public function setSpecialty($specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }

    public function addSpecialty(?Specialty $specialty): self
    {
        $this->specialty[] = $specialty;

        return $this;
    }


    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageTitle(): ?string
    {
        return $this->image_title;
    }

    public function setImageTitle(?string $image_title): self
    {
        $this->image_title = $image_title;

        return $this;
    }

    public function getTableNutritional(): ?string
    {
        return $this->table_nutritional;
    }

    public function setTableNutritional(?string $table_nutritional): self
    {
        $this->table_nutritional = $table_nutritional;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getIsLancamento(): ?bool
    {
        return $this->is_lancamento;
    }

    public function setIsLancamento(bool $is_lancamento): self
    {
        $this->is_lancamento = $is_lancamento;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }



    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(?string $presentation): self
    {
        $this->presentation = $presentation;

        return $this;
    }

    public function getActiveIngredient(): ?string
    {
        return $this->activeIngredient;
    }

    public function setActiveIngredient(string $activeIngredient): self
    {
        $this->activeIngredient = $activeIngredient;

        return $this;
    }



    public function getScientificstudies(): ?ScientificStudies
    {
        return $this->scientificstudies;
    }

    public function setScientificstudies(?ScientificStudies $scientificstudies): self
    {
        $this->scientificstudies = $scientificstudies;

        return $this;
    }


    public function getRecommendation(): ?string
    {
        return $this->recommendation;
    }

    public function setRecommendation(?string $recommendation): self
    {
        $this->recommendation = $recommendation;

        return $this;
    }


    public function getProducttype(): ?string
    {
        return $this->product_type;
    }

    public function setProducttype(string $product_type): self
    {
        $this->product_type = $product_type;

        return $this;
    }

    public function getBula(): ?string
    {
        return $this->bula;
    }

    public function setBula(?string $bula): self
    {
        $this->bula = $bula;

        return $this;
    }

    public function getFind(): ?string
    {
        return $this->find;
    }

    public function setFind(?string $find): self
    {
        $this->find = $find;

        return $this;
    }

    public function getKnowmore(): ?string
    {
        return $this->knowmore;
    }

    public function setKnowmore(?string $knowmore): self
    {
        $this->knowmore = $knowmore;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}