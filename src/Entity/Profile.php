<?php

namespace App\Entity;

use App\Repository\ProfileRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass=ProfileRepository::class)
 */
class Profile
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_active;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_caterer = false;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_food = false;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="profile")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=ProfileAcess::class, mappedBy="profile")
     */
    private $profileAcesses;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->profileAcesses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
    
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getIsCaterer(): ?bool
    {
        return $this->is_caterer;
    }

    public function setIsCaterer(bool $is_caterer): self
    {
        $this->is_caterer = $is_caterer;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->userAcesses;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setProfile($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getProfile() === $this) {
                $user->setProfile(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProfileAcess[]
     */
    public function getProfileAcesses(): Collection
    {
        return $this->profileAcesses;
    }

    public function addProfileAcess(ProfileAcess $profileAcess): self
    {
        if (!$this->profileAcesses->contains($profileAcess)) {
            $this->profileAcesses[] = $profileAcess;
            $profileAcess->setProfile($this);
        }

        return $this;
    }

    public function removeProfileAcess(ProfileAcess $profileAcess): self
    {
        if ($this->profileAcesses->contains($profileAcess)) {
            $this->profileAcesses->removeElement($profileAcess);
            // set the owning side to null (unless already changed)
            if ($profileAcess->getProfile() === $this) {
                $profileAcess->setProfile(null);
            }
        }

        return $this;
    }
    
    public function getIsFood(): ?bool
    {
        return $this->is_food;
    }

    public function setIsFood(bool $is_food): self
    {
        $this->is_food = $is_food;

        return $this;
    }
}
