<?php

namespace App\Entity;

use App\Repository\UserTemporaryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserTemporaryRepository::class)
 */
class UserTemporary implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $crm;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $uf_crm;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $cellphone;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_resident;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $medical_center;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $zip_code;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $complement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $neighborhood;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $uf;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_newsletter;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_term;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $step;

    /**
     * @ORM\ManyToMany(targetEntity=Specialty::class, inversedBy="userTemporaries")
     */
    private $specialty;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $salt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $expires_at;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_user_old;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_finished;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity=UserTemporaryRegisterStep::class, mappedBy="user_temporary")
     */
    private $userTemporaryRegisterSteps;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_cron_email;

    public function __construct()
    {
        $this->specialty = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->userTemporaryRegisterSteps = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCrm(): ?string
    {
        return $this->crm;
    }

    public function setCrm(?string $crm): self
    {
        $this->crm = $crm;

        return $this;
    }

    public function getUfCrm(): ?string
    {
        return $this->uf_crm;
    }

    public function setUfCrm(?string $uf_crm): self
    {
        $this->uf_crm = $uf_crm;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCellphone(): ?string
    {
        return $this->cellphone;
    }

    public function setCellphone(?string $cellphone): self
    {
        $this->cellphone = $cellphone;

        return $this;
    }

    public function getIsResident(): ?bool
    {
        return $this->is_resident;
    }

    public function setIsResident(?bool $is_resident): self
    {
        $this->is_resident = $is_resident;

        return $this;
    }

    public function getMedicalCenter(): ?string
    {
        return $this->medical_center;
    }

    public function setMedicalCenter(?string $medical_center): self
    {
        $this->medical_center = $medical_center;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    public function setZipCode(?string $zip_code): self
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(?string $complement): self
    {
        $this->complement = $complement;

        return $this;
    }

    public function getNeighborhood(): ?string
    {
        return $this->neighborhood;
    }

    public function setNeighborhood(?string $neighborhood): self
    {
        $this->neighborhood = $neighborhood;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getUf(): ?string
    {
        return $this->uf;
    }

    public function setUf(?string $uf): self
    {
        $this->uf = $uf;

        return $this;
    }

    public function getIsNewsletter(): ?bool
    {
        return $this->is_newsletter;
    }

    public function setIsNewsletter(?bool $is_newsletter): self
    {
        $this->is_newsletter = $is_newsletter;

        return $this;
    }

    public function getIsTerm(): ?bool
    {
        return $this->is_term;
    }

    public function setIsTerm(?bool $is_term): self
    {
        $this->is_term = $is_term;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getStep(): ?string
    {
        return $this->step;
    }

    public function setStep(?string $step): self
    {
        $this->step = $step;

        return $this;
    }

    /**
     * @return Collection|Specialty[]
     */
    public function getSpecialty(): Collection
    {
        return $this->specialty;
    }

    public function addSpecialty(Specialty $specialty): self
    {
        if (!$this->specialty->contains($specialty)) {
            $this->specialty[] = $specialty;
        }

        return $this;
    }

    public function removeSpecialty(Specialty $specialty): self
    {
        $this->specialty->removeElement($specialty);

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function eraseCredentials() {
        
    }

    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->email,
            $this->password,
        ));
    }

    public function unserialize($serialized) {
        list (
            $this->id,
            $this->username,
            $this->email,
            $this->password,
            ) = unserialize($serialized, ['allowed_classes' => false]);
    }


    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }


    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getExpiresAt(): ?\DateTimeImmutable
    {
        return $this->expires_at;
    }

    public function setExpiresAt(?\DateTimeImmutable $expires_at): self
    {
        $this->expires_at = $expires_at;

        return $this;
    }

    public function getIsUserOld(): ?bool
    {
        return $this->is_user_old;
    }

    public function setIsUserOld(?bool $is_user_old): self
    {
        $this->is_user_old = $is_user_old;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getIsFinished(): ?bool
    {
        return $this->is_finished;
    }

    public function setIsFinished(?bool $is_finished): self
    {
        $this->is_finished = $is_finished;

        return $this;
    }

    /**
     * @return Array|Role[]
     */
    public function getRoles(): array
    {
        $roles = [];
        
        foreach($this->roles as $role){
            $roles[] = $role->getRole();
        }

        return $roles;
    }

    /**
     * @return Array|Role[]
     */
    public function getRolesArray(): Array
    {
        $roles = [];
        
        foreach($this->roles as $role){
            $roles[] = $role->getRole();
        }

        return $roles;
    }

    public function addRole(Role $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole(Role $role): self
    {
        if ($this->roles->contains($role)) {
            $this->roles->removeElement($role);
        }

        return $this;
    }

    /**
     * @return Collection|UserTemporaryRegisterStep[]
     */
    public function getUserTemporaryRegisterSteps(): Collection
    {
        return $this->userTemporaryRegisterSteps;
    }

    public function addUserTemporaryRegisterStep(UserTemporaryRegisterStep $userTemporaryRegisterStep): self
    {
        if (!$this->userTemporaryRegisterSteps->contains($userTemporaryRegisterStep)) {
            $this->userTemporaryRegisterSteps[] = $userTemporaryRegisterStep;
            $userTemporaryRegisterStep->setUserTemporary($this);
        }

        return $this;
    }

    public function removeUserTemporaryRegisterStep(UserTemporaryRegisterStep $userTemporaryRegisterStep): self
    {
        if ($this->userTemporaryRegisterSteps->removeElement($userTemporaryRegisterStep)) {
            // set the owning side to null (unless already changed)
            if ($userTemporaryRegisterStep->getUserTemporary() === $this) {
                $userTemporaryRegisterStep->setUserTemporary(null);
            }
        }

        return $this;
    }

    public function getIsCronEmail(): ?bool
    {
        return $this->is_cron_email;
    }

    public function setIsCronEmail(?bool $is_cron_email): self
    {
        $this->is_cron_email = $is_cron_email;

        return $this;
    }

}
