<?php

namespace App\Entity;

use App\Repository\UserAcessRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserAcessRepository::class)
 */
class UserAcess
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userAcesses")
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity=Menu::class, inversedBy="userAcesses")
     */
    private $menus;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_create;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_update;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_viewed;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_delete;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsers(): ?User
    {
        return $this->users;
    }

    public function setUsers(?User $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getMenus(): ?Menu
    {
        return $this->menus;
    }

    public function setMenus(?Menu $menus): self
    {
        $this->menus = $menus;

        return $this;
    }

    public function getIsCreate(): ?bool
    {
        return $this->is_create;
    }

    public function setIsCreate(bool $is_create): self
    {
        $this->is_create = $is_create;

        return $this;
    }

    public function getIsUpdate(): ?bool
    {
        return $this->is_update;
    }

    public function setIsUpdate(bool $is_update): self
    {
        $this->is_update = $is_update;

        return $this;
    }

    public function getIsViewed(): ?bool
    {
        return $this->is_viewed;
    }

    public function setIsViewed(bool $is_viewed): self
    {
        $this->is_viewed = $is_viewed;

        return $this;
    }

    public function getIsDelete(): ?bool
    {
        return $this->is_delete;
    }

    public function setIsDelete(bool $is_delete): self
    {
        $this->is_delete = $is_delete;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
