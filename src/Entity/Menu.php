<?php

namespace App\Entity;

use App\Repository\MenuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MenuRepository::class)
 */
class Menu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_active;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_caterer = false;

    /**
     * @ORM\OneToMany(targetEntity=UserAcess::class, mappedBy="menus")
     */
    private $userAcesses;

    /**
     * @ORM\ManyToOne(targetEntity=MenuCategory::class, inversedBy="menus")
     */
    private $menu_category;

    public function __construct()
    {
        $this->userAcesses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|UserAcess[]
     */
    public function getUserAcesses(): Collection
    {
        return $this->userAcesses;
    }

    public function addUserAcess(UserAcess $userAcess): self
    {
        if (!$this->userAcesses->contains($userAcess)) {
            $this->userAcesses[] = $userAcess;
            $userAcess->setMenus($this);
        }

        return $this;
    }

    public function removeUserAcess(UserAcess $userAcess): self
    {
        if ($this->userAcesses->contains($userAcess)) {
            $this->userAcesses->removeElement($userAcess);
            // set the owning side to null (unless already changed)
            if ($userAcess->getMenus() === $this) {
                $userAcess->setMenus(null);
            }
        }

        return $this;
    }

    public function getMenuCategory(): ?MenuCategory
    {
        return $this->menu_category;
    }

    public function setMenuCategory(?MenuCategory $menu_category): self
    {
        $this->menu_category = $menu_category;

        return $this;
    }

    public function getIsCaterer(): ?bool
    {
        return $this->is_caterer;
    }

    public function setIsCaterer(bool $is_caterer): self
    {
        $this->is_caterer = $is_caterer;

        return $this;
    }
}
