<?php

namespace App\Entity;

use App\Repository\SpecialtyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SpecialtyRepository::class)
 */
class Specialty
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $specialty;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $icon;

    /**
     * @ORM\ManyToMany(targetEntity=UserTemporary::class, mappedBy="specialty")
     */
    private $userTemporaries;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="specialty")
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity=Mementos::class, mappedBy="specialty")
     */
    private $mementos;

    /**
     * @ORM\ManyToMany(targetEntity=Events::class, mappedBy="specialty")
     */
    private $events;

    public function __construct()
    {
        $this->userTemporaries = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->mementos = new ArrayCollection();
        $this->events = new ArrayCollection();
    }

    public function __toString() {
        return $this->specialty;
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getSpecialty(): ?string
    {
        return $this->specialty;
    }

    public function setSpecialty(string $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return Collection|UserTemporary[]
     */
    public function getUserTemporaries(): Collection
    {
        return $this->userTemporaries;
    }

    public function addUserTemporary(UserTemporary $userTemporary): self
    {
        if (!$this->userTemporaries->contains($userTemporary)) {
            $this->userTemporaries[] = $userTemporary;
            $userTemporary->addSpecialty($this);
        }

        return $this;
    }

    public function removeUserTemporary(UserTemporary $userTemporary): self
    {
        if ($this->userTemporaries->removeElement($userTemporary)) {
            $userTemporary->removeSpecialty($this);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addSpecialty($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeSpecialty($this);
        }

        return $this;
    }

    /**
     * @return Collection|Mementos[]
     */
    public function getMementos(): Collection
    {
        return $this->mementos;
    }

    public function addMemento(Mementos $memento): self
    {
        if (!$this->mementos->contains($memento)) {
            $this->mementos[] = $memento;
            $memento->addSpecialty($this);
        }

        return $this;
    }

    public function removeMemento(Mementos $memento): self
    {
        if ($this->mementos->removeElement($memento)) {
            $memento->removeSpecialty($this);
        }

        return $this;
    }

    /**
     * @return Collection|Events[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Events $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->addSpecialty($this);
        }

        return $this;
    }

    public function removeEvent(Events $event): self
    {
        if ($this->events->removeElement($event)) {
            $event->removeSpecialty($this);
        }

        return $this;
    }
}