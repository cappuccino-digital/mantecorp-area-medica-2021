<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields="email", message="Esse e-mail está sendo utilizado.")
 * @UniqueEntity(fields="username", message="Esse username está sendo utilizado.")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true, length=100)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    // /**
    //  * @ORM\Column(type="string", length=255)
    //  */
    // private $salt;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $avatar;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_active;

 

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $last_login_at;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity=UserAcess::class, mappedBy="users")
     */
    private $userAcesses;

    /**
     * @ORM\ManyToOne(targetEntity=Profile::class, inversedBy="users")
     */
    private $profile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $crm;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $uf_crm;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $cellphone;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_resident;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $medical_center;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $zip_code;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $complement;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $neighborhood;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $uf;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_newsletter;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_term;

    /**
     * @ORM\ManyToMany(targetEntity=Specialty::class, inversedBy="users")
     */
    private $specialty;

    /**
     * @ORM\OneToOne(targetEntity="UserNewPassword")
     *
     */
    private $newPasswordToken;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_user_old;

    /**
     * @ORM\OneToMany(targetEntity="UserFavorite", mappedBy="user")
     */
    private $favorites;

    /**
     * @ORM\OneToMany(targetEntity=UserEvents::class, mappedBy="user")
     */
    private $userEvents;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email_eadbox;

    /**
     * @ORM\OneToMany(targetEntity=UserAvaliation::class, mappedBy="user")
     */
    private $userAvaliations;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->userAcesses = new ArrayCollection();
        $this->specialty = new ArrayCollection();
        $this->favorites = new ArrayCollection();
        $this->userEvents = new ArrayCollection();
        $this->userAvaliations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    

    public function eraseCredentials() {
        
    }

    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->email,
            $this->password,
        ));
    }

    public function unserialize($serialized) {
        list (
            $this->id,
            $this->username,
            $this->email,
            $this->password,
            ) = unserialize($serialized, ['allowed_classes' => false]);
    }


    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    
    /**
     * @return Array|Role[]
     */
    public function getRoles(): array
    {
        $roles = [];
        
        foreach($this->roles as $role){
            $roles[] = $role->getRole();
        }

        return $roles;
    }

    /**
     * @return Array|Role[]
     */
    public function getRolesArray(): Array
    {
        $roles = [];
        
        foreach($this->roles as $role){
            $roles[] = $role->getRole();
        }

        return $roles;
    }

    public function addRole(Role $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole(Role $role): self
    {
        if ($this->roles->contains($role)) {
            $this->roles->removeElement($role);
        }

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getLastLoginAt(): ?\DateTimeInterface
    {
        return $this->last_login_at;
    }

    public function setLastLoginAt(\DateTimeInterface $last_login_at): self
    {
        $this->last_login_at = $last_login_at;

        return $this;
    }

    /**
     * @return Collection|UserAcess[]
     */
    public function getUserAcesses(): Collection
    {
        return $this->userAcesses;
    }

    public function addUserAcess(UserAcess $userAcess): self
    {
        if (!$this->userAcesses->contains($userAcess)) {
            $this->userAcesses[] = $userAcess;
            $userAcess->setUsers($this);
        }

        return $this;
    }

    public function removeUserAcess(UserAcess $userAcess): self
    {
        if ($this->userAcesses->contains($userAcess)) {
            $this->userAcesses->removeElement($userAcess);
            // set the owning side to null (unless already changed)
            if ($userAcess->getUsers() === $this) {
                $userAcess->setUsers(null);
            }
        }

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    public function getCrm(): ?string
    {
        return $this->crm;
    }

    public function setCrm(?string $crm): self
    {
        $this->crm = $crm;

        return $this;
    }

    public function getUfCrm(): ?string
    {
        return $this->uf_crm;
    }

    public function setUfCrm(?string $uf_crm): self
    {
        $this->uf_crm = $uf_crm;

        return $this;
    }

    public function getCellphone(): ?string
    {
        return $this->cellphone;
    }

    public function setCellphone(?string $cellphone): self
    {
        $this->cellphone = $cellphone;

        return $this;
    }

    public function getIsResident(): ?bool
    {
        return $this->is_resident;
    }

    public function setIsResident(?bool $is_resident): self
    {
        $this->is_resident = $is_resident;

        return $this;
    }

    public function getMedicalCenter(): ?string
    {
        return $this->medical_center;
    }

    public function setMedicalCenter(?string $medical_center): self
    {
        $this->medical_center = $medical_center;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    public function setZipCode(?string $zip_code): self
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(?string $complement): self
    {
        $this->complement = $complement;

        return $this;
    }

    public function getNeighborhood(): ?string
    {
        return $this->neighborhood;
    }

    public function setNeighborhood(?string $neighborhood): self
    {
        $this->neighborhood = $neighborhood;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getUf(): ?string
    {
        return $this->uf;
    }

    public function setUf(?string $uf): self
    {
        $this->uf = $uf;

        return $this;
    }

    public function getIsNewsletter(): ?bool
    {
        return $this->is_newsletter;
    }

    public function setIsNewsletter(?bool $is_newsletter): self
    {
        $this->is_newsletter = $is_newsletter;

        return $this;
    }

    public function getIsTerm(): ?bool
    {
        return $this->is_term;
    }

    public function setIsTerm(?bool $is_term): self
    {
        $this->is_term = $is_term;

        return $this;
    }

    /**
     * @return Collection|Specialty[]
     */
    public function getSpecialty(): Collection
    {
        return $this->specialty;
    }

    public function addSpecialty(Specialty $specialty): self
    {
        if (!$this->specialty->contains($specialty)) {
            $this->specialty[] = $specialty;
        }

        return $this;
    }

    public function removeSpecialty(Specialty $specialty): self
    {
        $this->specialty->removeElement($specialty);

        return $this;
    }

    public function getNewPasswordToken(): ?UserNewPassword
    {
        return $this->newPasswordToken;
    }

    public function setNewPasswordToken(?UserNewPassword $newPasswordToken): self
    {
        $this->newPasswordToken = $newPasswordToken;

        return $this;
    }

    public function getIsUserOld(): ?bool
    {
        return $this->is_user_old;
    }

    public function setIsUserOld(?bool $is_user_old): self
    {
        $this->is_user_old = $is_user_old;

        return $this;
    }

    /**
     * @return Collection|UserFavorite[]
     */
    public function getFavorites(): Collection
    {
        return $this->favorites;
    }

    public function addFavorite(UserFavorite $favorite): self
    {
        if (!$this->favorites->contains($favorite)) {
            $this->favorites[] = $favorite;
            $favorite->setUser($this);
        }

        return $this;
    }

    public function removeFavorite(UserFavorite $favorite): self
    {
        if ($this->favorites->contains($favorite)) {
            $this->favorites->removeElement($favorite);
            // set the owning side to null (unless already changed)
            if ($favorite->getUser() === $this) {
                $favorite->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserEvents[]
     */
    public function getUserEvents(): Collection
    {
        return $this->userEvents;
    }

    public function addUserEvent(UserEvents $userEvent): self
    {
        if (!$this->userEvents->contains($userEvent)) {
            $this->userEvents[] = $userEvent;
            $userEvent->setUser($this);
        }

        return $this;
    }

    public function removeUserEvent(UserEvents $userEvent): self
    {
        if ($this->userEvents->removeElement($userEvent)) {
            // set the owning side to null (unless already changed)
            if ($userEvent->getUser() === $this) {
                $userEvent->setUser(null);
            }
        }

        return $this;
    }

    public function getEmailEadbox(): ?string
    {
        return $this->email_eadbox;
    }

    public function setEmailEadbox(?string $email_eadbox): self
    {
        $this->email_eadbox = $email_eadbox;

        return $this;
    }

    /**
     * @return Collection|UserAvaliation[]
     */
    public function getUserAvaliations(): Collection
    {
        return $this->userAvaliations;
    }

    public function addUserAvaliation(UserAvaliation $userAvaliation): self
    {
        if (!$this->userAvaliations->contains($userAvaliation)) {
            $this->userAvaliations[] = $userAvaliation;
            $userAvaliation->setUser($this);
        }

        return $this;
    }

    public function removeUserAvaliation(UserAvaliation $userAvaliation): self
    {
        if ($this->userAvaliations->removeElement($userAvaliation)) {
            // set the owning side to null (unless already changed)
            if ($userAvaliation->getUser() === $this) {
                $userAvaliation->setUser(null);
            }
        }

        return $this;
    }
}
