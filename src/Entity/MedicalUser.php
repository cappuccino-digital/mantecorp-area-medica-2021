<?php

namespace App\Entity;

use App\Repository\MedicalUserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=MedicalUserRepository::class)
 * @UniqueEntity(fields="email", message="Esse e-mail está sendo utilizado.")
 * @UniqueEntity(fields="username", message="Esse username está sendo utilizado.")
 */
class MedicalUser implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $crm;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $crm_state;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cpf;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cellphone;

    /**
     * @ORM\ManyToOne(targetEntity=Specialty::class, inversedBy="usuarios-medicos", cascade={"persist"})
     */
    private $specialty;

    /**
     * @ORM\Column(type="boolean")
     */
    private $resident_doctor;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $medical_center;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cep;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $complement;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $district;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="boolean", nullable=true))
     */
    private $receive_news;

    /**
     * @ORM\Column(type="boolean")
     */
    private $terms_of_use;

    /**
     * @ORM\Column(type="datetime" , nullable=true))
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true))
     */
    private $updated_at;

    /**
     * @ORM\Column(type="boolean"))
     */
    private $is_active;


    /**
     * @ORM\Column(type="datetime" ,nullable=true)
     */
    private $last_login_at;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="usuario-medico")
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity=UserAcess::class, mappedBy="usuario-medico")
     */
    private $userAcesses;

    /**
     * @ORM\ManyToOne(targetEntity=Profile::class, inversedBy="usuario-medico")
     */
    private $profile;


    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->userAcesses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getCrm(): ?string
    {
        return $this->crm;
    }

    public function setCrm(string $crm): self
    {
        $this->crm = $crm;

        return $this;
    }

    public function getCrmState(): ?string
    {
        return $this->crm_state;
    }

    public function setCrmState(string $crm_state): self
    {
        $this->crm_state = $crm_state;

        return $this;
    }


    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsers(): ?User
    {
        return $this->users;
    }

    public function setUsers(?User $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): self
    {
        $this->cpf = $cpf;

        return $this;
    }

    public function getCellphone(): ?string
    {
        return $this->cellphone;
    }

    public function setCellphone(string $cellphone): self
    {
        $this->cellphone = $cellphone;

        return $this;
    }

    public function getSpecialty(): ?Specialty
    {
        return $this->specialty;
    }

    public function setSpecialty(?Specialty $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }

    public function getResidentDoctor(): ?bool
    {
        return $this->resident_doctor;
    }

    public function setResidentDoctor(bool $resident_doctor): self
    {
        $this->resident_doctor = $resident_doctor;

        return $this;
    }

    public function getMedicalCenter(): ?string
    {
        return $this->medical_center;
    }

    public function setMedicalCenter(string $medical_center): self
    {
        $this->medical_center = $medical_center;

        return $this;
    }

    public function getCep(): ?string
    {
        return $this->cep;
    }

    public function setCep(string $cep): self
    {
        $this->cep = $cep;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(string $complement): self
    {
        $this->complement = $complement;

        return $this;
    }

    public function getDistrict(): ?string
    {
        return $this->district;
    }

    public function setDistrict(string $district): self
    {
        $this->district = $district;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }


    public function eraseCredentials()
    {
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->email,
            $this->password,
        ));
    }

    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->email,
            $this->password,
        ) = unserialize($serialized, ['allowed_classes' => false]);
    }

    /**
     * @return Array|Role[]
     */
    public function getRoles(): array
    {
        $roles = [];

        foreach ($this->roles as $role) {
            $roles[] = $role->getRole();
        }

        return $roles;
    }

    /**
     * @return Array|Role[]
     */
    public function getRolesArray(): array
    {
        $roles = [];

        foreach ($this->roles as $role) {
            $roles[] = $role->getRole();
        }

        return $roles;
    }

    public function addRole(Role $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole(Role $role): self
    {
        if ($this->roles->contains($role)) {
            $this->roles->removeElement($role);
        }

        return $this;
    }


    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getReceiveNews(): ?bool
    {
        return $this->receive_news;
    }

    public function setReceiveNews(bool $receive_news): self
    {
        $this->receive_news = $receive_news;

        return $this;
    }

    public function getTermsOfUse(): ?bool
    {
        return $this->terms_of_use;
    }

    public function setTermsOfUse(bool $terms_of_use): self
    {
        $this->terms_of_use = $terms_of_use;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }


    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }


    public function getLastLoginAt(): ?\DateTimeInterface
    {
        return $this->last_login_at;
    }

    public function setLastLoginAt(\DateTimeInterface $last_login_at): self
    {
        $this->last_login_at = $last_login_at;

        return $this;
    }

    /**
     * @return Collection|MedicalUser[]
     */
    public function getUserAcesses(): Collection
    {
        return $this->userAcesses;
    }

    public function addUserAcess(MedicalUser $userAcess): self
    {
        if (!$this->userAcesses->contains($userAcess)) {
            $this->userAcesses[] = $userAcess;
            $userAcess->setUsers($this);
        }

        return $this;
    }

    public function removeUserAcess(MedicalUser $userAcess): self
    {
        if ($this->userAcesses->contains($userAcess)) {
            $this->userAcesses->removeElement($userAcess);
            // set the owning side to null (unless already changed)
            if ($userAcess->getUsers() === $this) {
                $userAcess->setUsers(null);
            }
        }

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }
}