<?php

namespace App\Repository;

use App\Entity\UserEad;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserEad|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserEad|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserEad[]    findAll()
 * @method UserEad[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserEadRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserEad::class);
    }

    // /**
    //  * @return UserEad[] Returns an array of UserEad objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserEad
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
