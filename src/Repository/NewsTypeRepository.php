<?php

namespace App\Repository;

use App\Entity\NewsType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NewsType|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewsType|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewsType[]    findAll()
 * @method NewsType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method NewsType[]    getNoticeByTypeId($id)
 */
class NewsTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NewsType::class);
    }

    public function getNoticeByTypeId($id)
    {
        $sql = "SELECT 
                    *
                FROM
                    news_type AS nt
                        INNER JOIN
                    notice_news_type AS nnt ON nt.id = nnt.news_type_id
                        INNER JOIN
                    notice AS n ON nnt.notice_id = n.id
                WHERE
                    nt.id = " . $id;
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    // /**
    //  * @return NewsType[] Returns an array of NewsType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NewsType
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}