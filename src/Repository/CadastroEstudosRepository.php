<?php

namespace App\Repository;

use App\Entity\CadastroEstudos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CadastroEstudos|null find($id, $lockMode = null, $lockVersion = null)
 * @method CadastroEstudos|null findOneBy(array $criteria, array $orderBy = null)
 * @method CadastroEstudos[]    findAll()
 * @method CadastroEstudos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CadastroEstudosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CadastroEstudos::class);
    }

    // /**
    //  * @return CadastroEstudos[] Returns an array of CadastroEstudos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CadastroEstudos
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
