<?php

namespace App\Repository;

use App\Entity\UserFavorite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserFavorite|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserFavorite|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserFavorite[]    findAll()
 * @method UserFavorite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserFavoriteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserFavorite::class);
    }


    public function getUserFavoriteContent($id){
        $sql = "SELECT id, type, type_id
                FROM user_favorite
                WHERE user_id = '{$id}' AND is_active = true
                ORDER BY created_at DESC";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $content = $stmt->fetchAll();

        $response = [];

        foreach ($content as $key => $c) {
            switch ($c['type']) {
                case 'produtos' : {
                    $result = $em->getRepository('App:Product')->find($c['type_id']);
                    $response[$key]['object'] = $result;
                    $response[$key]['contents_type'] = 'produtos';
                    $response[$key]['contents'] = 'Produtos';
                    break;
                }
                case 'estudos-cientificos' : {
                    $result = $em->getRepository('App:ScientificStudies')->find($c['type_id']);
                    $response[$key]['object'] = $result;
                    $response[$key]['contents_type'] = 'estudos-cientificos';
                    $response[$key]['contents'] = 'Estudos Científicos';
                    break;
                }
                case 'cursos' : {
                    $result = $em->getRepository('App:Course')->find($c['type_id']);
                    $response[$key]['object'] = $result;
                    $response[$key]['contents_type'] = 'cursos';
                    $response[$key]['contents'] = 'Cursos';
                    break;
                }
                case 'noticias' : {
                    $result = $em->getRepository('App:Notice')->find($c['type_id']);
                    $response[$key]['object'] = $result;
                    $response[$key]['contents_type'] = 'noticias';
                    $response[$key]['contents'] = 'Notícias';
                    break;
                }
            }
        }

        return $response;
    }

    // /**
    //  * @return UserFavorite[] Returns an array of UserFavorite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserFavorite
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
