<?php

namespace App\Repository;

use App\Entity\UserAvaliation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserAvaliation|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserAvaliation|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserAvaliation[]    findAll()
 * @method UserAvaliation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserAvaliationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserAvaliation::class);
    }

    // /**
    //  * @return UserAvaliation[] Returns an array of UserAvaliation objects
    //  */

    public function findMidiaAvaliation($studies_id)
    {
        $media = $this->getEntityManager()->createQueryBuilder()
            ->select('AVG(a.avaliation) as media')
            ->from('App:UserAvaliation', 'a')
            ->where('a.scientific_studies = :scientific_studies')
            ->setParameter('scientific_studies', $studies_id)
            ->getQuery()
            ->getResult();

        return isset($media[0]['media']) ? $media[0]['media'] : 0;
    }
    

    /*
    public function findOneBySomeField($value): ?UserAvaliation
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
