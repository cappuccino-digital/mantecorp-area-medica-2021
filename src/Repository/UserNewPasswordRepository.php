<?php

namespace App\Repository;

use App\Entity\UserNewPassword;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserNewPassword|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserNewPassword|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserNewPassword[]    findAll()
 * @method UserNewPassword[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserNewPasswordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserNewPassword::class);
    }

    // /**
    //  * @return UserNewPassword[] Returns an array of UserNewPassword objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserNewPassword
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
