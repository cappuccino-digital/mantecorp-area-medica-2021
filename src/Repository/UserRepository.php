<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
   
    public function findByUserRoles()
    {
        $em = $this->getEntityManager();

        $sql = "SELECT u.* FROM user as u
                inner join user_role as ur
                on ur.user_id = u.id
                inner join role as r
                on r.id = ur.role_id
                where r.role in('ROLE_ADMIN', 'ROLE_DEV')";
    
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;   
    }
    
    public function findByUserCatererRoles()
    {
        $em = $this->getEntityManager();

        $sql = "SELECT u.* FROM user as u
                inner join user_role as ur
                on ur.user_id = u.id
                inner join role as r
                on r.id = ur.role_id
                where r.role in('ROLE_ADMIN_CATERER')";
    
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;   
    }

    
}
