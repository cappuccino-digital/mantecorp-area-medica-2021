<?php

namespace App\Repository;

use App\Entity\Course;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Course|null find($id, $lockMode = null, $lockVersion = null)
 * @method Course|null findOneBy(array $criteria, array $orderBy = null)
 * @method Course[]    findAll()
 * @method Course[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Course[]    findBySpecialty($specialty)
 * @method MedicalUser[]    updateCourseSpecialty($id)
 * @method MedicalUser[]    findBySpecialtyId($id) 
 * @method MedicalUser[]    deleteCourseSpecialty($id)
 */
class CourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Course::class);
    }

    public function findBySpecialty($specialty)
    {
        $sql = "     SELECT 
                           c.name as curso , c.imageweb , c.resume, c.link , s.specialty , e.name as type
                        FROM
                            course AS c
                                INNER JOIN
                            specialty AS s ON c.specialty_id = s.id
                                INNER JOIN
                            education AS e ON c.type_id = e.id
                        WHERE
                            s.specialty LIKE '%" . $specialty . "%'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }



    public function findBySpecialtyId($id)
    {
        $sql = "SELECT * FROM course where specialty_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }


    public function deleteCourseSpecialty($id)
    {
        $sql = "DELETE FROM your_patient_specialty 
                WHERE specialty_id = " . $id . ";";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function updateCourseSpecialty($id)
    {
        $sql = "UPDATE course 
                SET 
                    specialty_id = null
                WHERE
                    specialty_id = '" . $id . "';";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    // /**
    //  * @return Course[] Returns an array of Course objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Course
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}