<?php

namespace App\Repository;

use App\Entity\RegisterStep;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RegisterStep|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegisterStep|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegisterStep[]    findAll()
 * @method RegisterStep[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegisterStepRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegisterStep::class);
    }

    // /**
    //  * @return RegisterStep[] Returns an array of RegisterStep objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RegisterStep
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
