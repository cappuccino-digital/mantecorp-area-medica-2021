<?php

namespace App\Repository;

use App\Entity\UserTemporary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserTemporary|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserTemporary|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserTemporary[]    findAll()
 * @method UserTemporary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method MedicalUser[]    updateUserTemporarySpecialty($id)
 * @method MedicalUser[]    findBySpecialtyId($id) 
 * @method MedicalUser[]    deleteUserTemporarySpecialty($id) 
 */
class UserTemporaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserTemporary::class);
    }



    public function findBySpecialtyId($id)
    {
        $sql = "SELECT * FROM medical_user where specialty_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }


    public function deleteUserTemporarySpecialty($id)
    {
        $sql = "DELETE FROM user_specialty 
                WHERE specialty_id = " . $id . ";";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function updateUserTemporarySpecialty($id)
    {
        $sql = "UPDATE medical_user 
                SET 
                    specialty_id = null
                WHERE
                    specialty_id = '" . $id . "';";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    // /**
    //  * @return UserTemporary[] Returns an array of UserTemporary objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserTemporary
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}