<?php

namespace App\Repository;

use App\Entity\YourPatient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method YourPatient|null find($id, $lockMode = null, $lockVersion = null)
 * @method YourPatient|null findOneBy(array $criteria, array $orderBy = null)
 * @method YourPatient[]    findAll()
 * @method YourPatient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method YourPatient[]    getAll()
 * @method YourPatient[]    updateYourPatientSpecialty($id)
 * @method YourPatient[]    findBySpecialtyId($id) 
 * @method YourPatient[]    deleteYourPatientSpecialty($id)* 
 * @method YourPatient[]    findBySpecialty($specialty)
 * @method YourPatient[]    findByProductId($id) 
 */
class YourPatientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, YourPatient::class);
    }

    // /**
    //  * @return YourPatient[] Returns an array of YourPatient objects
    //  */
    public function getAllTitles()
    {
        $sql = "SELECT id, title_v as video, title_m as material FROM your_patient";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }



    public function findBySpecialtyId($id)
    {
        $sql = "SELECT 
                    *
                FROM
                    your_patient yp
                        INNER JOIN
                    your_patient_specialty ys ON yp.id = ys.your_patient_id
                WHERE
                    ys.your_patient_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }


    public function findByProductId($id)
    {
        $sql = "select * from your_patient where products_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function deleteYourPatientSpecialty($id)
    {
        $sql = "DELETE FROM your_patient_specialty 
                WHERE specialty_id = " . $id . ";";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function updateYourPatientSpecialty($id)
    {
        $sql = "UPDATE your_patient 
                SET 
                    specialty_id = null
                WHERE
                    specialty_id = '" . $id . "';";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }



    public function findBySpecialty($specialty)
    {
        $sql = " SELECT 
                    yp.id, yp.title_v, yp.resume_v, yp.video, yp.title_m , yp.resume_m, yp.file, yp.materialtype , p.image 
                FROM
                    your_patient yp
                        INNER JOIN
                    your_patient_specialty AS ys ON yp.id = ys.your_patient_id
                        INNER JOIN
                    specialty s ON ys.specialty_id = s.id
                        INNER JOIN products p on yp.products_id = p.id
                WHERE
                    s.specialty = '" . $specialty . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
    // /**
    //  * @return YourPatient[] Returns an array of YourPatient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('y')
            ->andWhere('y.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('y.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?YourPatient
    {
        return $this->createQueryBuilder('y')
            ->andWhere('y.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}