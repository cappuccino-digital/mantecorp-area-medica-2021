<?php

namespace App\Repository;

use App\Entity\Mementos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Mementos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mementos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mementos[]    findAll()
 * @method Mementos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Mementos[]    findBySpecialtyId($id)
 * @method Mementos[]    findByProductId($id)
 */
class MementosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mementos::class);
    }

    public function findBySpecialtyId($id)
    {
        $sql = "SELECT 
                    *
                FROM
                    mementos 
                WHERE
                    specialty_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }


    public function findByProductId($id)
    {
        $sql = "select * from mementos where products_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    // /**
    //  * @return Mementos[] Returns an array of Mementos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Mementos
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}