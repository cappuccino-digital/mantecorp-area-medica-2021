<?php

namespace App\Repository;

use App\Entity\Flyer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Flyer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Flyer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Flyer[]    findAll()
 * @method Flyer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Flyer[]    findAllFlyers()
 * @method Flyer[]    findByProduct($id)
 * @method Flyer[]    findByProductId($id) 
 */
class FlyerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Flyer::class);
    }

    public function findAllFlyers()
    {
        $sql = "SELECT 
                    flyer.id, flyer.name, flyer.file, products.name AS product
                FROM
                    flyer
                        INNER JOIN
                    products ON flyer.product_id = products.id;";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $row = $stmt->fetchAll();

        return $row;
    }

    public function findByProduct($id)
    {
        $sql = "SELECT 
                    flyer.id, flyer.name, flyer.file, products.name AS product
                FROM
                    flyer
                        INNER JOIN
                    products ON flyer.product_id = products.id
                WHERE
                    products.id = '" . $id . "'   
                    ;";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $row = $stmt->fetchAll();

        return $row;
    }

    public function findByProductId($id)
    {
        $sql = "select * from flyer where product_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    // /**
    //  * @return Flyer[] Returns an array of Flyer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Flyer
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}