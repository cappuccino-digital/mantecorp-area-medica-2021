<?php

namespace App\Repository;

use App\Entity\Products;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Products|null find($id, $lockMode = null, $lockVersion = null)
 * @method Products|null findOneBy(array $criteria, array $orderBy = null)
 * @method Products[]    findAll()
 * @method Products[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)* 
 * @method Products[]    findPrescritos()
 * @method Products[]    findByType()
 * @method Products[]    findAllByLetra($letra)
 * @method Products[]    findProductCategory($slug)
 * @method Products[]    findList($slug)
 * @method Products[]    findAllLancamentos()
 * @method Products[]    findBySlug($slug)
 * @method Products[]    findAllByEspecialidade($especialidade)
 * @method Products[]    updateProductsSpecialty($id)
 * @method Products[]    findBySpecialtyId($id) 
 * @method Products[]    findByProductId($id) 
 * @method Products[]    deleteProductsSpecialty($id)
 */
class ProductsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Products::class);
    }

    /**
     * @return Products[] Returns an array of Products objects
     */
    public function findByType($value)
    {
        $sql = "SELECT * from products where product_type = '$value' and is_active = '1'";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $row = $stmt->fetchAll();

        return $row;
    }

    /**
     * @return Products[] Returns an array of Products objects
     */
    public function findAllByLetra($letra)
    {
        $sql = "SELECT * from products where name like '" . $letra . "%' and is_active = '1'";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $row = $stmt->fetchAll();

        return $row;
    }

    public function findBySlug($slug)
    {
        $sql = "SELECT * from products where slug = '" . $slug . "' and is_active = '1'";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $row = $stmt->fetchAll();

        return $row;
    }

    public function findAllLancamentos()
    {
        $sql = "SELECT * from products where is_lancamento = 1";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $row = $stmt->fetchAll();

        return $row;
    }

    public function findProductCategory($slug)
    {

        $sql = "SELECT product_category.name from product_category
                    inner join
                         products_product_category on product_category.id = products_product_category.product_category_id 
                    inner join 
                        products on products_product_category.products_id = products.id
                    where 
                        products.slug = '" . $slug . "'
                    order by product_category.updated_at desc;";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $row = $stmt->fetchAll();

        return $row;
    }

    public function findList($slug)
    {

        $sql = "SELECT  * FROM products where products.slug != '" . $slug . "'";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $row = $stmt->fetchAll();

        return $row;
    }


    public function findAllByCategoria($categoria)
    {

        $sql = "SELECT  * FROM products where products.slug != '" . $categoria . "'";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $row = $stmt->fetchAll();

        return $row;
    }

    public function findAllByEspecialidade($especialidade)
    {

        $sql = "SELECT 
    *
FROM
    products
        INNER JOIN
    products_specialty ON products.id = products_specialty.products_id
        INNER JOIN
    specialty ON products_specialty.specialty_id = specialty.id
     where specialty.specialty = '" . $especialidade . "'
    group by products_id;";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $row = $stmt->fetchAll();

        return $row;
    }



    public function findBySpecialtyId($id)
    {
        $sql = "SELECT * from products as p inner join products_specialty  as ps on p.id = ps.products_id where  ps.specialty_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }



    public function findByProductId($id)
    {
        $sql = "SELECT * from products as p inner join products_specialty  as ps on p.id = ps.products_id where  ps.specialty_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function deleteProductsSpecialty($id)
    {
        $sql = "DELETE FROM products_specialty 
                WHERE specialty_id = " . $id . ";";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function updateProductsSpecialty($id)
    {
        $sql = "UPDATE products_specialty 
                SET 
                    specialty_id = null
                WHERE
                    specialty_id = '" . $id . "';";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    // /**
    //  * @return Products[] Returns an array of Products objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Products
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}