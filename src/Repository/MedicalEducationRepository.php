<?php

namespace App\Repository;

use App\Entity\MedicalEducation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MedicalEducation|null find($id, $lockMode = null, $lockVersion = null)
 * @method MedicalEducation|null findOneBy(array $criteria, array $orderBy = null)
 * @method MedicalEducation[]    findAll()
 * @method MedicalEducation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MedicalEducationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MedicalEducation::class);
    }

    // /**
    //  * @return MedicalEducation[] Returns an array of MedicalEducation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MedicalEducation
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
