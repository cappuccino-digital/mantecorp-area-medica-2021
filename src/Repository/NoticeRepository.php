<?php

namespace App\Repository;

use App\Entity\Notice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Notice|null find($id, $lockMode = null, $lockVersion = null)
 * @method Notice|null findOneBy(array $criteria, array $orderBy = null)
 * @method Notice[]    findAll()
 * @method Notice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Notice[]    findById($id)
 * @method Notice[]    findBySpecialtyId($id)
 * @method Notice[]    findByCategory($slug)
 * @method Notice[]    findCategoryByNotice($slug)
 * @method Notice[]    updateProductId($id)
 * @method Notice[]    updateSpecialtyId($id)
 * @method Notice[]    findByProductId($id)
 */
class NoticeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notice::class);
    }

    // /**
    //  * @return Notice[] Returns an array of Notice objects
    //  */

    public function findByDates()
    {
        $sql = "SELECT distinct(year(created_at)) as years FROM notice where is_active = true and type_id = 2 order by created_at desc";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }




    public function findByProductId($id)
    {
        $sql = "select * from notice where products_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }


    public function findByCategoryDates($category)
    {
        $sql = "SELECT distinct(year(n.created_at)) as years FROM notice as n
            inner join notice_news_categories as nc
            on nc.notice_id = n.id
            inner join news_categories as c
            on c.id = nc.news_categories_id
            where n.is_active = true and c.slug = 'olimpiadas' order by n.created_at desc";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function    findById($id)
    {
        $sql = "SELECT * FROM notice where id = " . $id;

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }



    public function findBySpecialtyId($id)
    {
        $sql = "SELECT 
                    *
                FROM
                    notice where specialty_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function findByCategory($slug)
    {
        $sql = "SELECT 
                    notice.id, notice.title, notice.slug, notice.image,notice.link,notice.description
                FROM
                    notice
                        INNER JOIN
                    notice_news_type on notice.id = notice_news_type.notice_id
                        INNER JOIN
                    news_type on notice_news_type.news_type_id = news_type.id
                WHERE
                    news_type.slug = '" . $slug . "'
                    group by notice_id
                    order by notice.updated_at desc;";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function findCategoryByNotice($slug)
    {
        $sql = " SELECT 
                   news_type_id, news_type.slug, news_type.name
                FROM
                    notice
                        INNER JOIN
                    notice_news_type on notice.id = notice_news_type.notice_id
                        INNER JOIN
                    news_type on notice_news_type.news_type_id = news_type.id
                WHERE
                     notice.slug= '" . $slug . "'
                    group by news_type_id
                    order by notice.updated_at desc;";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function findByNoticesAndProducts($search)
    {
        $sql = "SELECT id, title, description, image, slug, is_active, created_at, 'notice' as is_type FROM notice where title like '%$search%' or description like '%$search%'
                    UNION
                SELECT id, name, descriptive as description, image, slug, is_active, created_at, 'product' as is_type FROM products where name like '%$search%' or descriptive like '%$search%'";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function updateProductId($id)
    {
        $sql = "UPDATE notice
                    SET notice.products_id = null
                    WHERE notice.products_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();
    }

    public function updateSpecialtyId($id)
    {
        $sql = "UPDATE notice
                    SET notice.specialty_id = null
                    WHERE notice.specialty_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();
    }
    /*
    public function findOneBySomeField($value): ?Notice
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}