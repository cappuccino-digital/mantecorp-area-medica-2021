<?php

namespace App\Repository;

use App\Entity\UserAcess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserAcess|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserAcess|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserAcess[]    findAll()
 * @method UserAcess[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserAcessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserAcess::class);
    }

    public function getPermission($user, $menu, $action)
    {
        $em = $this->getEntityManager();
        $menu = $em->getRepository('App:Menu')->findOneBy(['slug' => $menu]); 

        if (!$menu)
            return ['status' => false, 'message' => '[00] - Acesso negado.']; // Menu não encontrado
                
        $user_acess = $em->getRepository('App:UserAcess')->findOneBy(['users' => $user->getId(), 'menus' => $menu]);

        if (!$user_acess) 
            return ['status' => false, 'message' => '[01] - Acesso negado.']; // Não tem permissão para acessar o menu
              
        switch ($action) {
            case 'new': 
                if (!$user_acess->getIsCreate()) // Não tem permissão para criar o contéudo
                    return ['status' => false, 'message' => '[02] - Acesso negado.'];
            break;

            case 'edit': //Não tem permissão para editar o contéudo
                if (!$user_acess->getIsUpdate()) 
                    return ['status' => false, 'message' => '[03] - Acesso negado.'];
            break;

            case 'delete': //Não tem permissão para deletar o contéudo
                if (!$user_acess->getIsDelete()) 
                    return ['status' => false, 'message' => '[04] - Acesso negado.'];
            break;

            case 'view': //Não tem permissão para visualizar o contéudo
                if (!$user_acess->getIsViewed()) 
                    return ['status' => false, 'message' => '[05] - Acesso negado.'];
            break;
        }

        return ['status' => true, 'message' => 'Acesso permitido.', 'user_acess' => $user_acess];
    }

    public function getUserMenu($user, $menu)
    {
        $em = $this->getEntityManager();
        $user_menu = $em->getRepository('App:UserAcess')->findBy(['user' => $user]);
       
        if (!$user_menu) {
            return null;
        }
        
        $r = $em->getRepository('App:UserAcess')->createQueryBuilder('ua')
                    ->innerJoin('App:Menu','m','WITH','m.id = ua.menu')
                    ->where('m.slug = :slug')
                    ->andWhere('ua.user = :user')
                    ->setParameter('slug', $menu)
                    ->setParameter('user', $user)
                    ->getQuery()
                    ->getResult();

        //duap($r); die();
        return $r[0];
    }


    // /**
    //  * @return UserAcess[] Returns an array of UserAcess objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserAcess
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
