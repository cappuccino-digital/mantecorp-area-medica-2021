<?php

namespace App\Repository;

use App\Entity\UserTemporaryRegisterStep;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserTemporaryRegisterStep|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserTemporaryRegisterStep|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserTemporaryRegisterStep[]    findAll()
 * @method UserTemporaryRegisterStep[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserTemporaryRegisterStepRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserTemporaryRegisterStep::class);
    }

    // /**
    //  * @return UserTemporaryRegisterStep[] Returns an array of UserTemporaryRegisterStep objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserTemporaryRegisterStep
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
