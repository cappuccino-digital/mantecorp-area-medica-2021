<?php

namespace App\Repository;

use App\Entity\Reproved;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Reproved|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reproved|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reproved[]    findAll()
 * @method Reproved[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReprovedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reproved::class);
    }

    // /**
    //  * @return Reproved[] Returns an array of Reproved objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Reproved
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
