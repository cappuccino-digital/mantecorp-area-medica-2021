<?php

namespace App\Repository;

use App\Entity\Speakers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Speakers|null find($id, $lockMode = null, $lockVersion = null)
 * @method Speakers|null findOneBy(array $criteria, array $orderBy = null)
 * @method Speakers[]    findAll()
 * @method Speakers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpeakersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Speakers::class);
    }

    // /**
    //  * @return Speakers[] Returns an array of Speakers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Speakers
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
