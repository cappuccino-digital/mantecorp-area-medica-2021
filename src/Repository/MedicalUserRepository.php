<?php

namespace App\Repository;

use App\Entity\MedicalUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MedicalUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method MedicalUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method MedicalUser[]    findAll()
 * @method MedicalUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null) 
 * @method MedicalUser[]    updateMedicalUserSpecialty($id)
 * @method MedicalUser[]    findBySpecialtyId($id) 
 * @method MedicalUser[]    deleteUserSpecialty($id) 
 */
class MedicalUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MedicalUser::class);
    }


    public function verificaEmail($email)
    {
        $sql = "SELECT 
                    email
                FROM
                    medical_user
                WHERE
                    email = '" . $email . "'
                AND is_active = TRUE";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }



    public function findBySpecialtyId($id)
    {
        $sql = "SELECT * FROM medical_user where specialty_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }


    public function deleteUserSpecialty($id)
    {
        $sql = "DELETE FROM user_specialty 
                WHERE specialty_id = " . $id . ";";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function updateMedicalUserSpecialty($id)
    {
        $sql = "UPDATE medical_user 
                SET 
                    specialty_id = null
                WHERE
                    specialty_id = '" . $id . "';";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
    // /**
    //  * @return MedicalUser[] Returns an array of MedicalUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MedicalUser
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}