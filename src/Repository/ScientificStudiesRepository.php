<?php

namespace App\Repository;

use App\Entity\ScientificStudies;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ScientificStudies|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScientificStudies|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScientificStudies[]    findAll()
 * @method ScientificStudies[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method ScientificStudies[]    findByIdSpecialty($id)
 * @method ScientifiStudies[]    findByProductId($id) 
 * @method ScientifiStudies[]    findByLetra($letra)
 * @method ScientifiStudies[]     findByNome($nome)
 * @method ScientificStudies[]    findByNomeSpecialty($id)
 */
class ScientificStudiesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ScientificStudies::class);
    }

    public function findByIdSpecialty($id)
    {
        $sql = "SELECT 
                    ss.id,
                    ss.name,
                    ss.resume,
                    ss.imageweb,
                    ss.file,
                    s.id AS specialty_id,
                    s.specialty,
                    p.id AS product_id,
                    p.name AS product,
                    p.image AS product_image
                FROM
                    scientific_studies AS ss
                        INNER JOIN
                    specialty AS s ON ss.specialty_id = s.id
                        INNER JOIN
                    products AS p ON ss.product_id = p.id
                WHERE
                    s.id = '" . $id . "'";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }


    public function findByNomeSpecialty($nome)
    {
        $sql = "SELECT 
                    ss.id,
                    ss.name,
                    ss.resume,
                    ss.imageweb,
                    ss.file,
                    s.id AS specialty_id,
                    s.specialty,
                    p.id AS product_id,
                    p.name AS product,
                    p.image AS product_image
                FROM
                    scientific_studies AS ss
                        INNER JOIN
                    specialty AS s ON ss.specialty_id = s.id
                        INNER JOIN
                    products AS p ON ss.product_id = p.id
                WHERE
                    s.specialty = '" . $nome . "'";


        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }



    public function findByProductId($id)
    {
        $sql = "select * from scientific_studies where product_id = '" . $id . "'";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }


    public function findByLetra($letra)
    {
        $sql = "SELECT 
                    ss.id,
                    ss.name,
                    ss.resume,
                    ss.imageweb,
                    ss.file,
                    s.id AS specialty_id,
                    s.specialty,
                    p.id AS product_id,
                    p.name AS product,
                    p.image AS product_image
                FROM
                    scientific_studies ss
                        INNER JOIN
                    products p ON ss.product_id = p.id
                        INNER JOIN
                    specialty s ON ss.specialty_id = s.id
                WHERE
                    ss.name like '" . $letra . "%';";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function findByNome($name)
    {
        $sql = "SELECT 
                    ss.id,
                    ss.name,
                    ss.resume,
                    ss.imageweb,
                    ss.file,
                    s.id AS specialty_id,
                    s.specialty,
                    p.id AS product_id,
                    p.name AS product,
                    p.image AS product_image
                FROM
                    scientific_studies ss
                        INNER JOIN
                    products p ON ss.product_id = p.id
                        INNER JOIN
                    specialty s ON ss.specialty_id = s.id
                WHERE
                    ss.name like '%" . $name . "%';";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
    // /**
    //  * @return ScientificStudies[] Returns an array of ScientificStudies objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ScientificStudies
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}