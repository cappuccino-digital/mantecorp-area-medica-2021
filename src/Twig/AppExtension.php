<?php

namespace App\Twig;

use Doctrine\ORM\EntityManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * GetProvinceExtension constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('permission', [$this, 'getPermission']),
            new TwigFunction('history', [$this, 'getHistory']),
            new TwigFunction('statistics', [$this, 'getStatistics']),
            new TwigFunction('percentage', [$this, 'getPercentage']),
            new TwigFunction('evaluation', [$this, 'evaluationContents']),
            new TwigFunction('yourPatient', [$this, 'yourPatients']),
            new TwigFunction('avaliationMedia', [$this, 'avaliationMedia'])
        ];
    }

    public function evaluationContents($user, $type, $type_id)
    {
        $user_favorite = $this->em->getRepository("App:UserFavorite")->findOneBy(['user' => $user, 'type' => $type, 'typeId' => $type_id]);
        
        return $user_favorite;
    }

    public function yourPatients($product, $material)
    {
        $your_patient = $this->em->getRepository('App:YourPatient')->findBy(['products' => $product, 'materialtype' => $material]);

        return $your_patient;
    }

    public function avaliationMedia($studies_id)
    {
        $media = $this->em->getRepository('App:UserAvaliation')->findMidiaAvaliation($studies_id);
        $media = $media < 3  ? 3 : $media;
        return $media;
    }

    public function getPermission($user)
    {
        // $user = $this->em->getRepository("App:User")->findOneById($user);
        //dump($user); die();
        $category = [];
        if ($user) {
            if ($user->getUserAcesses()) {

                foreach ($user->getUserAcesses() as $key => $acess) {
                    if ($acess->getMenus()->getMenuCategory()) { // com submenu
                        $c = $acess->getMenus()->getMenuCategory();

                        $menus = $this->em->getRepository("App:MenuCategory")->findMenu($user, $c->getSlug());

                        $category[$c->getName()] = $menus;
                    }
                }
            }
        }
        //dump($category); die();
        return $category;
    }

    public function getHistory($file)
    {
        $history = $this->em->getRepository("App:Reproved")->findBy(['file' => $file]);
        return $history;
    }

    public function getStatistics($user)
    {
        $statistics = $this->em->getRepository("App:CatererFiles")->statistic($user);
        return $statistics;
    }

    public function getPercentage($parcial, $total)
    {
        if ($parcial == 0) $parcial = 1;
        if ($total == 0) $total = 1;

        $porc = ($parcial * 100) / $total;
        $porc = number_format($porc, 2, ".", "");

        return $porc;
    }
}
