<?php

namespace App\Form;

use App\Entity\ProductCategory;
use App\Entity\Products;
use App\Entity\Store;
use App\Entity\ScientificStudies;
use App\Entity\Specialty;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\File;

class ProductsType extends AbstractType
{
    // logo , apracour , onde encontrar , principio ativo apresentação n registro
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'required' => true,
                'label' => 'Nome'
            ])
            ->add('descriptive', TextareaType::class, [
                'attr' => ['class' => 'ckeditor'],
                'label' => 'Administração',
                'required' => true,
            ])
            ->add('bula')
            ->add('find', null, [
                'required' => false,
                'label' => 'Link onde encontrar',
                'required' => false,
            ])
            ->add('activeIngredient', TextareaType::class, [
                'attr' => ['class' => 'ckeditor'],
                'label' => 'Ingrediente Ativo',
            ])
            ->add('presentation', TextareaType::class, [
                'attr' => ['class' => 'ckeditor'],
                'label' => 'Apresentação',
                'required' => false,
            ])
            ->add('recommendation', TextareaType::class, [
                'attr' => ['class' => 'ckeditor'],
                'label' => 'Indicação',
                'required' => false,
            ])
            ->add('knowmore', null, [
                'required' => false,
                'label' => 'Link para saber mais',
                'required' => false,
                'empty_data' => '',
            ])
            ->add('image', FileType::class, [
                'label' => 'Imagem',
                'help' => 'São permitidos arquivos em PNG, JPG ou JPEG de até 1MB.',
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1000000',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Tipo de documento inválido',
                        'maxSizeMessage' => 'Tamanho de arquivo inválido.'
                    ])
                ],
                'required' => false
            ])
            ->add('image_title', null, [
                'label' => 'Título de imagem ',
                'required' => false
            ])
            ->add('specialty', EntityType::class, [
                'class' => Specialty::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.specialty', 'ASC');
                },
                'choice_label' => 'specialty',
                'required' => true,
                'label' => 'Especialidade',
                'multiple' => true,
                'expanded' => true
            ])
            // ->add('scientificStudies', EntityType::class, [
            //     'class' => ScientificStudies::class,
            //     'query_builder' => function (EntityRepository $er) {
            //         return $er->createQueryBuilder('n')
            //             ->orderBy('n.name', 'ASC');
            //     },
            //     'choice_label' => 'name',
            //     'required' => false,
            //     'label' => 'Estudos Científicos'
            // ])
            ->add('category', EntityType::class, [
                // looks for choices from this entity
                'class' => ProductCategory::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.name', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true,
                'required' => true,
                'label' => 'Categorias',
            ])->add('store', EntityType::class, [
                // looks for choices from this entity
                'class' => Store::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.name', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
                'required' => true,
                'label' => 'Loja',
            ])
            ->add('is_lancamento', CheckboxType::class, [
                'label' => 'Lançamento',
                'attr' => ['class' => 'user_role'],
                'required' => false
            ])
            ->add('is_active', CheckboxType::class, [
                'label' => 'Ativo',
                'attr' => ['class' => 'user_role'],
                'required' => false,
            ])->add('product_type', ChoiceType::class, [
                'choices'  => [
                    'Com Prescrição' => 'com-prescricao',
                    'Isento' => 'isento',
                ],
                "label" => "Tipo de Produto"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Products::class,
        ]);
    }
}