<?php

namespace App\Form;

use App\Entity\MenuCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MenuCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nome',
                'required' => true
            ])
            ->add('slug', null, [
                'label' => 'Slug',
                'required' => true
            ])
            ->add('icon', null, [
                'label' => 'Ícone',
                'required' => true
            ])
            ->add('is_active', CheckboxType::class, [
                'label' => 'Ativo',
                'attr' => ['class' => 'user_role']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MenuCategory::class,
        ]);
    }
}
