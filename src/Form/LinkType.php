<?php

namespace App\Form;

use App\Entity\Link;
use App\Entity\Products;
use App\Entity\Store;
use Symfony\Component\Form\AbstractType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class LinkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('store', EntityType::class, [
                // looks for choices from this entity
                'class' => Store::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.name', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
                'required' => true,
                'label' => 'Lojas',
            ])->add('product', EntityType::class, [
                // looks for choices from this entity
                'class' => Products::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.name', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
                'required' => true,
                'label' => 'Produto',
            ])
            ->add('link', null, [
                'required' => true,
                'label' => 'Link para a Loja que será utlizado na página compre aqui'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Link::class,
        ]);
    }
}