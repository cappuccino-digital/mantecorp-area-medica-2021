<?php

namespace App\Form;

use App\Entity\Specialty;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class SpecialtyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('specialty', null, [
                'label' => 'Especialidade',
                'required' => true
            ])
            ->add('icon', FileType::class, [
                'label' => 'Imagem',
                'help' => 'São permitidos arquivos em PNG, JPG ou JPEG de até 1MB.',
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1000000',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Tipo de documento inválido',
                        'maxSizeMessage' => 'Tamanho de arquivo inválido.'
                    ])
                ],
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Specialty::class,
        ]);
    }
}
