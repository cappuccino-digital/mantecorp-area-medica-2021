<?php

namespace App\Form;

use App\Entity\Products;
use App\Entity\Recipe;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Doctrine\ORM\EntityRepository;

class RecipeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('product', EntityType::class, [
                'label' => 'Produtos',
                'class' => Products::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.name', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
                'required' => true
            ])
            ->add('name', null, [
                'label' => 'Nome',
                'required' => true
            ])
            ->add('setup_time', null, [
                'label' => 'Tempo de preparo',
                'required' => true
            ])
            ->add('portions', null, [
                'label' => 'Porções',
                'required' => true
            ])
            ->add('image', FileType::class, [
                'label' => 'Imagem',
                'help' => 'São permitidos arquivos em PNG, JPG ou JPEG de até 1MB.',
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1000000',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Tipo de documento inválido',
                        'maxSizeMessage' => 'Tamanho de arquivo inválido.'
                    ])
                ],
                'required' => true
            ])
            ->add('image_title', null, [
                'label' => 'Título de imagem',
                'required' => true
            ])
            ->add('image_alt', null, [
                'label' => 'Alt imagem',
                'required' => true
            ])
            ->add('url', null, [
                'label' => 'URL',
                'required' => true
            ])
            ->add('is_active', CheckboxType::class, [
                'label' => 'Ativo',
                'attr' => ['class' => 'user_role']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Recipe::class,
        ]);
    }
}
