<?php

namespace App\Form;

use App\Entity\Products;
use App\Entity\YourPatient;
use App\Entity\Specialty;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class YourPatientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('materialType', ChoiceType::class, [
            'choices'  => [
                'Selecione' => '',
                'Video' => 'Video',
                'File' => 'File',
            ],
            "label" => "Tipo de Material"
        ])
            ->add('title_v', null, [
                'label' => 'Titulo do Vídeo',
            ])->add('resume_v', TextareaType::class, [
                'attr' => ['class' => 'ckeditor'],
                'label' => 'Resumo do Video',
            ])->add('video', TextareaType::class, [
                'attr' => ['class' => 'ckeditor'],
                'label' => 'Video Iframe',
            ])->add('title_m', null, [
                'label' => 'Titulo do Material',
                'required' => false
            ])->add('resume_m', TextareaType::class, [
                'attr' => ['class' => 'ckeditor'],
                'label' => 'Resumo do Material',
            ])->add('specialty', EntityType::class, [
                'class' => Specialty::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.specialty', 'ASC');
                },
                'choice_label' => 'specialty',
                'required' => true,
                'label' => 'Especialidade',
                'multiple' => true,
                'expanded' => true
            ])->add('products', EntityType::class, [
                // looks for choices from this entity
                'class' => Products::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.name', 'ASC');
                },
                'choice_label' => 'name',
                'required' => true,
                'multiple' => false,
                'label' => 'Produto',
            ])->add('file', FileType::class, array('data_class' => null), [

                'constraints' => [
                    new File([
                        'maxSize' => '1000000',
                        'mimeTypes' => [
                            'application/pdf'
                        ],
                        'mimeTypesMessage' => 'Tipo de documento inválido',
                        'maxSizeMessage' => 'Tamanho de arquivo inválido.'
                    ])
                ],
                'label' => 'File',
                "required" => false,

            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => YourPatient::class,
        ]);
    }
}