<?php

namespace App\Form;

use App\Entity\MedicalUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MedicalUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'required' => true,
                'label' => 'Nome'
            ])
            ->add('crm', null, [
                'required' => true,
                'label' => 'CRM'
            ])
            ->add('crm_state', null, [
                'label' => 'Estado do CRM'
            ])
            ->add('email', null, [
                'label' => 'Email'
            ])
            ->add('cpf', null, [
                'label' => 'CPF'
            ])
            ->add('cellphone', null, [
                'label' => 'Celular'
            ])
            ->add('specialty', null, [
                'label' => 'Especialidade'
            ])
            ->add('resident_doctor', null, [
                'label' => 'É Residente'
            ])
            ->add('medical_center', null, [
                'label' => 'Centro Médico'
            ])
            ->add('cep', null, [
                'label' => 'CEP'
            ])
            ->add('street', null, [
                'label' => 'Rua'
            ])
            ->add('number', null, [
                'label' => 'Número'
            ])
            ->add('complement', null, [
                'label' => 'Complemento'
            ])
            ->add('district', null, [
                'label' => 'Bairro'
            ])
            ->add('city', null, [
                'label' => 'Cidade'
            ])
            ->add('state', null, [
                'label' => 'Estado'
            ])
            ->add('password', null, [
                'label' => 'Senha'
            ])
            ->add('receive_news', null, [
                'label' => 'Desejo receber notícias'
            ])
            ->add('terms_of_use', null, [
                'label' => 'Aceito os termos de uso'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MedicalUser::class,
        ]);
    }
}
