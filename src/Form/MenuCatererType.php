<?php

namespace App\Form;

use App\Entity\Menu;
use App\Entity\MenuCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MenuCatererType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('menu_category', EntityType::class, [
                // looks for choices from this entity
                'class' => MenuCategory::class,
                // uses the User.username property as the visible option string
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where('c.is_caterer = true')
                        ->andWhere('c.is_active = true')
                        ->orderBy('c.name', 'ASC');
                },
                'choice_label' => 'name',
                'required' => true,
                'label' => 'Menu Categoria',
            ])
            ->add('name', null, [
                'label' => 'Nome',
                'required' => true
            ])
            ->add('slug', null, [
                'label' => 'Slug',
                'required' => true
            ])
            ->add('url', null, [
                'label' => 'URL',
                'required' => true
            ])
            ->add('is_active', CheckboxType::class, [
                'label' => 'Ativo',
                'attr' => ['class' => 'user_role']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Menu::class,
        ]);
    }
}
