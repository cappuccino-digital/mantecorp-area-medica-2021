<?php

namespace App\Form;

use App\Entity\Mementos;
use App\Entity\Products;
use App\Entity\Specialty;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MementosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nome',
                'required' => true
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['class' => 'ckeditor'],
                'label' => 'Descrição',
            ])->add('specialty', EntityType::class, [
                // looks for choices from this entity
                'class' => Specialty::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.specialty', 'ASC');
                },
                'choice_label' => 'specialty',
                'required' => true,
                'label' => 'Especialidade',
                'multiple' => true,
                'expanded' => true
            ])
            ->add('products', EntityType::class, [
                // looks for choices from this entity
                'class' => Products::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.name', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
                'required' => true,
                'label' => 'Produtos',
            ])
            ->add('file', FileType::class, array('data_class' => null), [
                'label' => 'File',
                'constraints' => [
                    new File([
                        #'maxSize' => '1000000',
                        'mimeTypes' => [
                            'application/pdf'
                        ],
                        'mimeTypesMessage' => 'Tipo de documento inválido'
                        #'maxSizeMessage' => 'Tamanho de arquivo inválido.'
                    ])
                ],
                'empty_data' => ''
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Mementos::class,
        ]);
    }
}