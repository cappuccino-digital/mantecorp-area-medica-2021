<?php

namespace App\Form;

use App\Entity\Profile;
use App\Entity\Role;
use App\Entity\Specialty;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('profile', EntityType::class, [
                'label' => 'Perfil',
                'class' => Profile::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->where('p.is_caterer = false')
                        ->andWhere('p.is_active = true')
                        ->orderBy('p.name', 'ASC');
                },
                'choice_label' => 'name',
                'multiple' => false,
                'attr' => ['class' => 'user_role']
            ])
            ->add('name', null, [
                'label' => 'Nome',
                'required' => true
            ])
            ->add('email', EmailType::class, [
                'invalid_message' => 'Preencha o campo corretamente',
                'label' => 'E-mail',
                'required' => true
            ])
            ->add('username', null, [
                'help' => 'campo para entrar na plataforma.'               
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Senha',
                // 'constraints' => [
                //     new NotBlank([
                //         'message' => 'Choose a password!']),
                //     ],
                'attr' => ['class' => 'p'],
                'required' => false,
                'empty_data' => ''
            ])
            ->add('avatar', FileType::class, [
                'label' => 'Imagem',
                // 'label_attr' => ['class' => 'custom-file-label'],
                'help' => 'São permitidos arquivos em PNG, JPG ou JPEG de até 1MB.',
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1000000',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Tipo de documento inválido.',
                        'maxSizeMessage' => 'Tamanho de arquivo inválido.'
                    ])
                ],
                'attr' => ['class' => 'file'],
            ])
            ->add('crm', null, [
                'label' => 'CRM',
                'help' => 'Atenção! Antes de alterar verificar se o CRM é válido',
                'required' => false,            
            ])
            ->add('uf_crm', null, [
                'label' => 'Estado do CRM',
                'attr' => ['class' => 'uf'],
                'required' => false,            
            ])
            ->add('cellphone', null, [
                'label' => 'Celular',
                'attr' => ['class' => 'cellphone'],
                'required' => false,            
            ])
            ->add('specialty', EntityType::class, [
                'label' => 'Especialidade',
                'class' => Specialty::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.specialty', 'ASC');
                },
                'required' => false,  
                'choice_label' => 'specialty',
                'multiple' => true,    
                'attr' => ['class' => 'specialty']      
            ])
            ->add('medical_center', null, [
                'label' => 'Centro médico',
                'required' => false,            
            ])
            ->add('is_resident', CheckboxType::class, [
                'label' => 'Residente?'
            ])
            ->add('zip_code', null, [
                'label' => 'CEP',
                'required' => false,  
                'attr' => ['class' => 'zip-code']          
            ])
            ->add('address', null, [
                'label' => 'Endereço',
                'required' => false,
                'attr' => ['class' => 'address']             
            ])
            ->add('neighborhood', null, [
                'label' => 'Bairro',
                'required' => false,    
                'attr' => ['class' => 'neighborhood']        
            ])
            ->add('city', null, [
                'label' => 'Cidade',
                'required' => false,  
                'attr' => ['class' => 'city']          
            ])
            ->add('number', null, [
                'label' => 'N°',
                'required' => false,   
                'attr' => ['class' => 'number']             
            ])
            ->add('complement', null, [
                'label' => 'Complemento',
                'required' => false,  
                'attr' => ['class' => 'complement']             
            ])
            ->add('uf', ChoiceType::class, [
                'choices'  => [
                    'SP' => 'SP',
                    'RJ' => 'RJ',
                ],
                'label' => 'UF',
                'required' => false,   
                'attr' => ['class' => 'uf']           
            ])
            ->add('is_active', CheckboxType::class, [
                'label' => 'Ativo',
                'attr' => ['class' => 'user_role']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }
}
