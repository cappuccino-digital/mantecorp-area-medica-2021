<?php

namespace App\Form;

use App\Entity\Events;
use App\Entity\Specialty;
use App\Entity\Speakers;
use Doctrine\DBAL\Types\DateTimeType as TypesDateTimeType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\DateType;
#use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class EventsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nome',
                'required' => true
            ])
            ->add('datetimeend', null, [
                'label' => 'Data e Hora para Fim',
                'required' => true
            ])
            // ->add('datetimeend', TypesDateTimeType::class, [
            //     'label' => 'Data de cadastro',
            //     'required' => true,
            //     'placeholder' => [
            //         'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
            //         'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',
            //     ],
            //     'widget' => 'single_text',
            //     'html5' => false,
            //     'attr' => [
            //         'class' => 'combinedPickerInput',
            //         'placeholder' => date('d/m/y H:i'),
            //     ],
            //     'widget' => 'single_text',
            //     'format' => 'dd/mm/yyyy',
            // ])
            ->add('local', null, [
                'label' => 'Local',
                'required' => true
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['class' => 'ckeditor'],
                'label' => 'Descrição',
            ])
           ->add('speakers', EntityType::class, [
                // looks for choices from this entity
                'class' => Speakers::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.speakers', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'speakers',
                'label' => 'Palestrantes',
                'multiple' => true, 
                'attr' => ['class' => 'mult-option']
            ])->add('specialty', EntityType::class, [
                // looks for choices from this entity
                'class' => Specialty::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.specialty', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'specialty',
                'multiple' => true, 
                'required' => true,
                'label' => 'Especialidade',
                'attr' => ['class' => 'mult-option']
            ])
            ->add('imageweb', FileType::class, [
                'label' => 'Imagem',
                'help' => 'São permitidos arquivos em PNG, JPG ou JPEG de até 1MB.',
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1000000',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Tipo de documento inválido',
                        'maxSizeMessage' => 'Tamanho de arquivo inválido.'
                    ])
                ],
                'required' => false
            ])->add('registration_type', ChoiceType::class, [
                'choices'  => [
                    'E-mail' => 'email',
                    'Link' => 'link',
                ],
                "label" => "Tipo de Inscrição"
            ])->add('email', null, [
                'label' => 'E-mail',
                'attr' => ['class' => 'email']
            ])
            ->add('link', null, [
                'label' => 'Link',
                'attr' => ['class' => 'link']
            ])
            ->add('is_active', CheckboxType::class, [
                'label' => 'Ativo',
                'attr' => ['class' => 'user_role']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Events::class,
        ]);
    }
}
