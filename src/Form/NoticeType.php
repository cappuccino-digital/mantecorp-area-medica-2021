<?php

namespace App\Form;

use App\Entity\NewsCategories;
use App\Entity\NewsType;
use App\Entity\Notice;
use App\Entity\Specialty;
use App\Entity\Products;
use Doctrine\ORM\EntityRepository;
use PhpParser\Parser\Multiple;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\DateType;
#use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class NoticeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*  ->add('category', EntityType::class, [
                // looks for choices from this entity
                'class' => NewsCategories::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.name', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
                'required' => true,
                'label' => 'Categoria',
                'multiple' => true,
            ]) */
            ->add('type', EntityType::class, [
                // looks for choices from this entity
                'class' => NewsType::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.name', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true,
                'required' => true,
                'label' => 'Tipo',
            ])->add('specialty', EntityType::class, [
                // looks for choices from this entity
                'class' => Specialty::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.specialty', 'ASC');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'specialty',
                'required' => true,
                'label' => 'Especialidade',
            ])
            ->add('products', EntityType::class, [
                'class' => Products::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.name', 'ASC');
                },
                'choice_label' => 'name',
                'required' => true,
                'label' => 'Produto',
            ])
            ->add('title', null, [
                'label' => 'Título',
                'required' => true
            ])
            ->add('image', FileType::class, [
                'label' => 'Imagem',
                'help' => 'São permitidos arquivos em PNG, JPG ou JPEG de até 1MB.',
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1000000',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Tipo de documento inválido',
                        'maxSizeMessage' => 'Tamanho de arquivo inválido.'
                    ])
                ],
                'required' => true
            ])
            ->add('image_title', null, [
                'label' => 'Título de imagem ',
                'required' => true
            ])
            ->add('image_alt', null, [
                'label' => 'Alt imagem',
                'required' => true
            ])
            ->add('link', null, [
                'label' => 'Link',
                'required' => false,
                'empty_data' => ' '
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['class' => 'ckeditor'],
                'label' => 'Descrição',
            ])
            ->add('created_at', DateTimeType::class, [
                'label' => 'Data de cadastro',
                'required' => true,
                // 'placeholder' => [
                //     'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                //     'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',
                // ]
                //'widget' => 'single_text',
                // 'html5' => false,
                // 'attr' => [
                //     'class' => 'combinedPickerInput',
                //     'placeholder' => date('d/m/y H:i'),
                // ],
                // 'widget' => 'single_text',
                //'format' => 'dd/mm/yyyy',
            ])
            // ->add('year', null, [
            //     'label' => 'Ano',
            //     'required' => true
            // ])

            ->add('is_active', CheckboxType::class, [
                'label' => 'Ativo',
                'attr' => ['class' => 'user_role']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Notice::class,
        ]);
    }
}
